# Abp_vNext中的仓储模式实现

## 仓储API使用
<!-- TOC -->

- [AbpvNext中的仓储模式实现](#abpvnext中的仓储模式实现)
    - [仓储API使用](#仓储api使用)
        - [仓储中的查询API](#仓储中的查询api)
        - [仓储中的增删改API](#仓储中的增删改api)

<!-- /TOC -->

### 仓储中的查询API

Abp vNext本身也是一套DDD应用开发框架，因此在设计查询API时，也充分考虑了DDD中聚合根的加载问题（聚合根中包含的子实体集合只能通过聚合根访问），因此可以看到其中的查询接口的定义都是带有`bool includeDetails = true`参数，用于指定是否加载子实体，而且默认是需要加载子实体的。下面看两个基础的只读仓储接口的定义:

```java
public interface IReadOnlyBasicRepository<TEntity> : IRepository
    where TEntity : class, IEntity
{
    /// <summary>
    /// Gets a list of all the entities.
    /// </summary>
    /// <returns>Entity</returns>
    Task<List<TEntity>> GetListAsync(bool includeDetails = false, CancellationToken cancellationToken = default);

    /// <summary>
    /// Gets total count of all entities.
    /// </summary>
    Task<long> GetCountAsync(CancellationToken cancellationToken = default);
}

public interface IReadOnlyBasicRepository<TEntity, TKey> : IReadOnlyBasicRepository<TEntity>
    where TEntity : class, IEntity<TKey>
{
    /// <summary>
    /// Gets an entity with given primary key.
    /// Throws <see cref="EntityNotFoundException"/> if can not find an entity with given id.
    /// 根据ID查询，没查到抛异常
    /// </summary>
    [NotNull]
    Task<TEntity> GetAsync(TKey id, bool includeDetails = true, CancellationToken cancellationToken = default);

    /// <summary>
    /// Gets an entity with given primary key or null if not found.
    /// 根据ID查询，没查到发挥NULL
    /// </summary>
    Task<TEntity> FindAsync(TKey id, bool includeDetails = true, CancellationToken cancellationToken = default);
}
```

接着来看一个具体的查询接口的实现，涉及到具体的查询实现了就会依赖具体的数据访问技术，比如`.net core`中常用的`Entity Framework Core`数据访问技术。下面以`Entity Framework Core`的仓储实现为例：

```java
public class EfCoreRepository<TDbContext, TEntity, TKey> : EfCoreRepository<TDbContext, TEntity>, 
    IEfCoreRepository<TEntity, TKey>,
    ISupportsExplicitLoading<TEntity, TKey>

    where TDbContext : IEfCoreDbContext
    where TEntity : class, IEntity<TKey>
{
    public EfCoreRepository(IDbContextProvider<TDbContext> dbContextProvider) 
        : base(dbContextProvider)
    {
    }

    // 省去无关代码...

    public virtual async Task<TEntity> FindAsync(TKey id, bool includeDetails = true, CancellationToken cancellationToken = default)
    {
        // 参数includeDetails为true时，则会通过 WithDetails方法来执行查询
        return includeDetails
            ? await WithDetails().FirstOrDefaultAsync(e => e.Id.Equals(id), GetCancellationToken(cancellationToken))
            : await DbSet.FindAsync(new object[] {id}, GetCancellationToken(cancellationToken));
    }

    // 省去无关代码...
}

public class EfCoreRepository<TDbContext, TEntity> : RepositoryBase<TEntity>, IEfCoreRepository<TEntity>
    where TDbContext : IEfCoreDbContext
    where TEntity : class, IEntity
{
    public virtual DbSet<TEntity> DbSet => DbContext.Set<TEntity>();

    DbContext IEfCoreRepository<TEntity>.DbContext => DbContext.As<DbContext>();

    protected virtual TDbContext DbContext => _dbContextProvider.GetDbContext();

    protected virtual AbpEntityOptions<TEntity> AbpEntityOptions => _entityOptionsLazy.Value;

    private readonly IDbContextProvider<TDbContext> _dbContextProvider;
    private readonly Lazy<AbpEntityOptions<TEntity>> _entityOptionsLazy;

    public EfCoreRepository(IDbContextProvider<TDbContext> dbContextProvider)
    {
        _dbContextProvider = dbContextProvider;

        _entityOptionsLazy = new Lazy<AbpEntityOptions<TEntity>>(
            () => ServiceProvider
                        .GetRequiredService<IOptions<AbpEntityOptions>>()
                        .Value
                        .GetOrNull<TEntity>() ?? AbpEntityOptions<TEntity>.Empty
        );
    }

  // 省去无关代码...

    protected override IQueryable<TEntity> GetQueryable()
    {
        return DbSet.AsQueryable();
    }

    public override IQueryable<TEntity> WithDetails()
    {
        // 没有默认的DefaultWithDetailsFunc代理，就直接调用Queryable接口去查询了
        // 这个DefaultWithDetailsFunc代理就是用于设置 EfCore的包含加载的，通过Include加载子实体
        if (AbpEntityOptions.DefaultWithDetailsFunc == null)
        {
            return base.WithDetails();
        }
        // 如果有默认的明细加载配置，则取执行配置。
        return AbpEntityOptions.DefaultWithDetailsFunc(GetQueryable());
    }

    public override IQueryable<TEntity> WithDetails(params Expression<Func<TEntity, object>>[] propertySelectors)
    {
        // 除了采用默认的配置，也提供了一个显示指定加载哪些子实体的方法。可以看到里面就是调用Ef Core的 Include
        var query = GetQueryable();
        
        if (!propertySelectors.IsNullOrEmpty())
        {
            foreach (var propertySelector in propertySelectors)
            {
                query = query.Include(propertySelector);
            }
        }

        return query;
    }

      // 省去无关代码...
}
```

看完了仓储的查询接口实现，再来看看具体如何使用，上面的源码显示了2种方式，先来看看第一种方式，预先设置聚合根实体和子实体加载关系的方式：

```java
// 1. 配置加载关系
public class AbpEntityFrameworkCoreTestModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.Services.AddAbpDbContext<TestAppDbContext>(options =>
        {
            options.AddDefaultRepositories(true);
            // 需要在Abp Module的服务配置方法中，指定加载Person聚合根时，默认加载Phone子实体
            options.Entity<Person>(opt =>
            {
                opt.DefaultWithDetailsFunc = q => q.Include(p => p.Phones);
            });
        });
    }
}

// 2. 调用查询方法
var personInDb = await _personRepository.FindAsync(personToAddNewPhone.Id);
personInDb.ShouldNotBeNull();
// 由于前面已经配置了加载子实体，这里Person的子实体Phones会被查询出来，如果没有前面的配置是不会加载出来的
personInDb.Phones.Any(p => p.Number == phoneNumberToAdd).ShouldBeTrue();

// 直接使用WithDetails也是一样的效果
var person = PersonRepository.WithDetails().Single(p => p.Id == TestDataBuilder.UserDouglasId);
person.Name.ShouldBe("Douglas");
person.Phones.Count.ShouldBe(2);
```

再来看看第二种方式，使用带参数的`WithDetails`方法，动态配置加载关系。

```java
// 具体调用时指定加载哪些子实体，这里表达式参数可以有多个
var person = PersonRepository.WithDetails(p => p.Phones).Single(p => p.Id == TestDataBuilder.UserDouglasId);
person.Name.ShouldBe("Douglas");
person.Phones.Count.ShouldBe(2);

```

其实如果纯粹是使用EfCore，而又启用了懒加载模式，这里的includeDetails参数其实是没有任何实际的用处的，不用设置包含关系，EfCore也会在访问导航属性时，自动的去加载子实体的数据。这里的由于API设计考虑，更多的可能是出于抽象的考虑，为了支持更为广泛的数据访问技术。


### 仓储中的增删改API

先来看看增删改的接口定义，增、删、改每个方法的参数都有一个`bool autoSave = false`，为什么会有这么一个参数了，方法注释上也给出了解释，在使用一些ORM框架或者数据库API时，如果需要立即保存，就设置为true。那具体什么场景才需要立即保存了？还是先找一个具体实现看一下。

```java
public interface IBasicRepository<TEntity> : IReadOnlyBasicRepository<TEntity>
    where TEntity : class, IEntity
{
    /// <summary>
    /// Inserts a new entity.
    /// </summary>
    /// <param name="autoSave">
    /// Set true to automatically save changes to database.
    /// This is useful for ORMs / database APIs those only save changes with an explicit method call, but you need to immediately save changes to the database.
    /// </param>
    /// <param name="cancellationToken">A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <param name="entity">Inserted entity</param>
    [NotNull]
    Task<TEntity> InsertAsync([NotNull] TEntity entity, bool autoSave = false, CancellationToken cancellationToken = default);

    /// <summary>
    /// Updates an existing entity. 
    /// </summary>
    /// <param name="autoSave">
    /// Set true to automatically save changes to database.
    /// This is useful for ORMs / database APIs those only save changes with an explicit method call, but you need to immediately save changes to the database.
    /// </param>
    /// <param name="cancellationToken">A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <param name="entity">Entity</param>
    [NotNull]
    Task<TEntity> UpdateAsync([NotNull] TEntity entity, bool autoSave = false, CancellationToken cancellationToken = default);

    /// <summary>
    /// Deletes an entity.
    /// </summary>
    /// <param name="entity">Entity to be deleted</param>
    /// <param name="autoSave">
    /// Set true to automatically save changes to database.
    /// This is useful for ORMs / database APIs those only save changes with an explicit method call, but you need to immediately save changes to the database.
    /// </param>
    /// <param name="cancellationToken">A <see cref="T:System.Threading.CancellationToken" /> to observe while waiting for the task to complete.</param>
    Task DeleteAsync([NotNull] TEntity entity, bool autoSave = false, CancellationToken cancellationToken = default);
}
```

基于`Entity Framework Core`的仓储实现中的增、删、改方法实现。

```java

public class EfCoreRepository<TDbContext, TEntity> : RepositoryBase<TEntity>, IEfCoreRepository<TEntity>
    where TDbContext : IEfCoreDbContext
    where TEntity : class, IEntity
{
    public virtual DbSet<TEntity> DbSet => DbContext.Set<TEntity>();

    DbContext IEfCoreRepository<TEntity>.DbContext => DbContext.As<DbContext>();

    protected virtual TDbContext DbContext => _dbContextProvider.GetDbContext();

    protected virtual AbpEntityOptions<TEntity> AbpEntityOptions => _entityOptionsLazy.Value;

    private readonly IDbContextProvider<TDbContext> _dbContextProvider;
    private readonly Lazy<AbpEntityOptions<TEntity>> _entityOptionsLazy;

    public EfCoreRepository(IDbContextProvider<TDbContext> dbContextProvider)
    {
        _dbContextProvider = dbContextProvider;

        _entityOptionsLazy = new Lazy<AbpEntityOptions<TEntity>>(
            () => ServiceProvider
                        .GetRequiredService<IOptions<AbpEntityOptions>>()
                        .Value
                        .GetOrNull<TEntity>() ?? AbpEntityOptions<TEntity>.Empty
        );
    }

    // 省去无关代码...

    public override async Task<TEntity> InsertAsync(TEntity entity, bool autoSave = false, CancellationToken cancellationToken = default)
    {
        var savedEntity = DbSet.Add(entity).Entity;

        if (autoSave)
        {
            await DbContext.SaveChangesAsync(GetCancellationToken(cancellationToken));
        }

        return savedEntity;
    }

    public override async Task<TEntity> UpdateAsync(TEntity entity, bool autoSave = false, CancellationToken cancellationToken = default)
    {
        DbContext.Attach(entity);

        var updatedEntity = DbContext.Update(entity).Entity;

        if (autoSave)
        {
            await DbContext.SaveChangesAsync(GetCancellationToken(cancellationToken));
        }

        return updatedEntity;
    }
    
    public override async Task DeleteAsync(TEntity entity, bool autoSave = false, CancellationToken cancellationToken = default)
    {
        DbSet.Remove(entity);

        if (autoSave)
        {
            await DbContext.SaveChangesAsync(GetCancellationToken(cancellationToken));
        }
    }

    public override async Task DeleteAsync(Expression<Func<TEntity, bool>> predicate, bool autoSave = false, CancellationToken cancellationToken = default)
    {
        var entities = await GetQueryable()
            .Where(predicate)
            .ToListAsync(GetCancellationToken(cancellationToken));

        foreach (var entity in entities)
        {
            DbSet.Remove(entity);
        }

        if (autoSave)
        {
            await DbContext.SaveChangesAsync(GetCancellationToken(cancellationToken));
        }
    }

    // 省去无关代码...
}
```

从上面的实现实现可以看到，当`autoSave=true`时，会立即调用EfCore的`DbContext.SaveChangesAsync`方法，该方会立即提交数据到数据库，提交完数据，我们的查询API是可以读取到数据库中的数据的，因此在做一些先删后插的动作时，它是管用的。

而这里的`autoSave=false`，那何时数据才会真正提交了，并不会因为这里设置了False数据就不提交，只是在调用这里的增、删、改方法时，不会立即提交，而是在Abp的工作单元提交的时候，才会真正进行提交。

> **注意**
如果在一个不开事务的工作单元里设置了`autoSave=true`，那后面的代码出现了异常，就不会自动回滚了。因此Abp vNext中的正确实践是：
> 1. 关闭全局自动开启事务、根据业务场景手动设置开启事务
> 2. 利用工作单元管理提交，禁止设置`autoSave=true`，由Uow控制提交，这时根本不用事务控制，有异常不会调用Ef的SaveChange，内存中的数据会被丢弃
> 
> 最后需要注意一点，也就是`EfCoreRepository.UpdateAsync`方法的实现，它将实体状态标记成了更新，因此EfCore会将实体上的所有字段进行更新，这往往也是造成性能问题的一大原因，在出现批量更新时尤为明显。而如果只是需要部分更新，如果是使用EfCore的仓储实现是不用显示的调用Update方法的。只用查出实体，修改属性即可，在最终调用SaveChange方法，会只对修改的部分字段执行更新。


> **其他资料** 
> Abp官方文档中的仓储说明 https://docs.abp.io/zh-Hans/abp/latest/Repositories