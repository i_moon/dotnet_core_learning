# Abp_vNext中的工作单元模式实现
<!-- TOC -->

- [Abp_vNext中的工作单元模式实现](#abpvnext%e4%b8%ad%e7%9a%84%e5%b7%a5%e4%bd%9c%e5%8d%95%e5%85%83%e6%a8%a1%e5%bc%8f%e5%ae%9e%e7%8e%b0)
  - [Abp中的工作单元执行流程](#abp%e4%b8%ad%e7%9a%84%e5%b7%a5%e4%bd%9c%e5%8d%95%e5%85%83%e6%89%a7%e8%a1%8c%e6%b5%81%e7%a8%8b)
  - [工作单元如何管理数据库资源的创建、释放](#%e5%b7%a5%e4%bd%9c%e5%8d%95%e5%85%83%e5%a6%82%e4%bd%95%e7%ae%a1%e7%90%86%e6%95%b0%e6%8d%ae%e5%ba%93%e8%b5%84%e6%ba%90%e7%9a%84%e5%88%9b%e5%bb%ba%e9%87%8a%e6%94%be)
  - [不使用工作单元调用仓储方法为什么会抛异常](#%e4%b8%8d%e4%bd%bf%e7%94%a8%e5%b7%a5%e4%bd%9c%e5%8d%95%e5%85%83%e8%b0%83%e7%94%a8%e4%bb%93%e5%82%a8%e6%96%b9%e6%b3%95%e4%b8%ba%e4%bb%80%e4%b9%88%e4%bc%9a%e6%8a%9b%e5%bc%82%e5%b8%b8)
  - [工作单元的基本使用](#%e5%b7%a5%e4%bd%9c%e5%8d%95%e5%85%83%e7%9a%84%e5%9f%ba%e6%9c%ac%e4%bd%bf%e7%94%a8)
    - [如何应用工作单元](#%e5%a6%82%e4%bd%95%e5%ba%94%e7%94%a8%e5%b7%a5%e4%bd%9c%e5%8d%95%e5%85%83)
    - [Abp默认工作单元应用规则](#abp%e9%bb%98%e8%ae%a4%e5%b7%a5%e4%bd%9c%e5%8d%95%e5%85%83%e5%ba%94%e7%94%a8%e8%a7%84%e5%88%99)
    - [工作单元配置](#%e5%b7%a5%e4%bd%9c%e5%8d%95%e5%85%83%e9%85%8d%e7%bd%ae)
  - [通过Abp中的工作单元测试代码认识工作单元嵌套，保留工作单元](#%e9%80%9a%e8%bf%87abp%e4%b8%ad%e7%9a%84%e5%b7%a5%e4%bd%9c%e5%8d%95%e5%85%83%e6%b5%8b%e8%af%95%e4%bb%a3%e7%a0%81%e8%ae%a4%e8%af%86%e5%b7%a5%e4%bd%9c%e5%8d%95%e5%85%83%e5%b5%8c%e5%a5%97%e4%bf%9d%e7%95%99%e5%b7%a5%e4%bd%9c%e5%8d%95%e5%85%83)
    - [嵌套工作单元](#%e5%b5%8c%e5%a5%97%e5%b7%a5%e4%bd%9c%e5%8d%95%e5%85%83)
    - [保留工作单元](#%e4%bf%9d%e7%95%99%e5%b7%a5%e4%bd%9c%e5%8d%95%e5%85%83)
  - [参考](#%e5%8f%82%e8%80%83)
  - [仓储、工作单元中的坑](#%e4%bb%93%e5%82%a8%e5%b7%a5%e4%bd%9c%e5%8d%95%e5%85%83%e4%b8%ad%e7%9a%84%e5%9d%91)

<!-- /TOC -->
在领域驱动中的工作单元模式通常用来管理数据库连接和事务。一方面简化事物的控制，另一方面通过集中进行事物提交，减小事务开启范围，减少锁表时间，来提升数据库操作性能。

## Abp中的工作单元执行流程

Abp对工作单元模式进行了抽象，`IUnitOfWork`抽象了事务的提交动作，回滚动作，`UnitOfWorkManger`则负责具体工作单元的开启，以及对当前工作单元上下文引用，`IAmbientUnitOfWork`则可以看做是一个工作单元的上下文对象，用于维持线程中的当前工作单元信息。另外由于工作单元模式并不依赖于具体的数据库访问技术，因此分别抽象了事务操作`ITransactionApi`，和数据库对象`IDatabaseApi`。

下面来看看一个Abp中的工作单元从开启，到提交、到销毁的整个流程图：
 ![UnitOfWorkExecFlow.png](resource/abp_action/UnitOfWorkExecFlow.png)

## 工作单元如何管理数据库资源的创建、释放

这里还是以具体的EfCore实现为例，来看看Abp中的工作单元是如何管理数据库资源的DbContext的创建和销毁的。在我们使用原生的EfCore的DbContext时，我们通常自己控制该对象的创建和销毁。

```java
using(var dbContext = new XXDbContext()){
    // 自己创建，使用完后释放
}
```

在引入IOC容器后，我们通常将DbContext的对象的创建和销毁托管给IOC容器，通常将生命周期设置为Transient。

而引入工作单元后，数据库资源的创建和销毁动作就交给了工作单元来完成。整个过程如下：

![DbContextLifeTime.png](resource/abp_action/DbContextLifeTime.png)

通过上面的流程可以看到，工作单元的实例控制着`DbContext`这个数据库资源的创建，而工作单元管理器又控制者工作单元`UnitOfWork`实例的创建，`UnitOfWork`和`DbContext`实际上在同一个`ServiceScopoe`下，因此，在工作单元实例销毁的时候，同时DbContext也会被销毁，也就实现了通过工作单元来控制`DbContext`的生命周期。


> **备注：** 
> 这里引申出一个问题，不开启工作单元，为什么调用仓储的`System.Linq.IQueryable`接口的扩展方法，比如`FirstOrDefault`会抛`DbContext`实例已释放的异常？

## 不使用工作单元调用仓储方法为什么会抛异常

其实这个问题提的并不十分准确，并不是调用所有的仓储方法都抛异常，而是调用了IQueryable接口的扩展方法会抛异常。这个问题十分让人困扰，可以看到Abp中的单元测试方法，都是自己在外面包一个UnitOfWork才能正常执行，比如这样：

```java
 protected virtual async Task WithUnitOfWorkAsync(AbpUnitOfWorkOptions options, Func<Task> action)
{
    using (var scope = ServiceProvider.CreateScope())
    {
        var uowManager = scope.ServiceProvider.GetRequiredService<IUnitOfWorkManager>();

        using (var uow = uowManager.Begin(options))
        {
            await action();

            await uow.CompleteAsync();
        }
    }
}
// 如果不用 WithUnitOfWorkAsync( async() =>{ })进行包裹，查询会报DbContext已释放异常。
```

Abp以AOP的方式已经对仓储方法调用进行了拦截。其中所有的方法都会被自动包一个`UnitOfWork`，按理说根据前面的生命周期分析，DbContext已经被创建了，哪为何又被释放了？又在一个被释放的DbContext上执行了数据库操作？顺着`EfCoreRepository`找到它的基类：

``` java
public abstract class RepositoryBase<TEntity> : BasicRepositoryBase<TEntity>, IRepository<TEntity>, IUnitOfWorkManagerAccessor
        where TEntity : class, IEntity
{
    public IDataFilter DataFilter { get; set; }

    public ICurrentTenant CurrentTenant { get; set; }

    public IUnitOfWorkManager UnitOfWorkManager { get; set; }

    // 继承了 IQueryable 接口，IQueryable接口的3个成员
    public virtual Type ElementType => GetQueryable().ElementType;

    public virtual Expression Expression => GetQueryable().Expression;

    public virtual IQueryProvider Provider => GetQueryable().Provider;

    // 省略非关键代码...
 
    protected abstract IQueryable<TEntity> GetQueryable();

    public abstract Task<TEntity> FindAsync(
        Expression<Func<TEntity, bool>> predicate, 
        bool includeDetails = true,
        CancellationToken cancellationToken = default);

    // 省略非关键代码...
}
```

结合前面的`UnitOfWorkInterceptor`这个拦截器，它会对仓储的公共方法属性都进行拦截，通过调试可以发现，在调用`System.Linq.Queryable`中的扩展方法比如`FirstOrDefault`时，由于`RepositoryBase`的`Provider`被拦截，已经对其开启了工作单元，但是在属性调用完，又会对对工作单元实例进行释放，因此DbContext也会被释放。因此在调用扩展方法`FirstOrDefault`时就出现了 数据库对象已被释放的异常。

另外如果是直接调用仓储中的其他非`System.Linq.Queryable`扩展方法，比如`FindAsync`是不会有这个异常的，调试发现调用它时，并不会先调用`RepositoryBase`的`Provider`，因为它并不是直接调用Queryable扩展方法，而是调用的DbSet来实现查询，因此也不存在DbContext提前被释放的问题。这也是目前发现Abp中设计的比较蹩脚的地方。

解决办法就是在外面再包一层`UnitOfWork`，让外层的UnitOfWork来维护DbContext的生命周期。也就是Abp中单元测试基类中的那种做法。


## 工作单元的基本使用

### 如何应用工作单元

1. 通过UnitOfWorkManger.Begin手动开启工作单元
2. 继承IUnitOfWorkEnabled接口
3. 通过UnitOfWorkAttribute标记的类或者方法

具体规则参见：`UnitOfWorkHelper.IsUnitOfWorkMethod`实现

### Abp默认工作单元应用规则

 1. 通过AbpUowActionFilter应用的控制器Action
 2. 继承自ApplicationService的应用服务, 和继承自BasicRepositoryBase的仓储 


### 工作单元配置

工作单元配置信息，可以通过全局配置类`AbpUnitOfWorkDefaultOptions`进行配置，也可以在创建工作单元时通过`AbpUnitOfWorkOptions`类型的参数进行指定。

``` java
public class AbpUnitOfWorkOptions : IAbpUnitOfWorkOptions
{
    // 是否开启事务
    public bool IsTransactional { get; set; }
    // 设置事务隔离级别
    public IsolationLevel? IsolationLevel { get; set; }
    // 设置超时时间
    public TimeSpan? Timeout { get; set; }

    public AbpUnitOfWorkOptions()
    {
    }

    public AbpUnitOfWorkOptions(bool isTransactional = false, IsolationLevel? isolationLevel = null, TimeSpan? timeout = null)
    {
        IsTransactional = isTransactional;
        IsolationLevel = isolationLevel;
        Timeout = timeout;
    }

    public AbpUnitOfWorkOptions Clone()
    {
        return new AbpUnitOfWorkOptions
        {
            IsTransactional = IsTransactional,
            IsolationLevel = IsolationLevel,
            Timeout = Timeout
        };
    }
}

// 全局配置，可以设置默认的工作单元事务行为，隔离级别，超时时间
public class AbpUnitOfWorkDefaultOptions
{
    /// <summary>
    /// Default value: <see cref="UnitOfWorkTransactionBehavior.Auto"/>.
    /// </summary>
    public UnitOfWorkTransactionBehavior TransactionBehavior { get; set; } = UnitOfWorkTransactionBehavior.Auto;

    public IsolationLevel? IsolationLevel { get; set; }

    public TimeSpan? Timeout { get; set; }

    internal AbpUnitOfWorkOptions Normalize(AbpUnitOfWorkOptions options)
    {
        if (options.IsolationLevel == null)
        {
            options.IsolationLevel = IsolationLevel;
        }

        if (options.Timeout == null)
        {
            options.Timeout = Timeout;
        }

        return options;
    }

    public bool CalculateIsTransactional(bool autoValue)
    {
        switch (TransactionBehavior)
        {
            case UnitOfWorkTransactionBehavior.Enabled:
                return true;
            case UnitOfWorkTransactionBehavior.Disabled:
                return false;
            case UnitOfWorkTransactionBehavior.Auto:
                return autoValue;
            default:
                throw new AbpException("Not implemented TransactionBehavior value: " + TransactionBehavior);
        }
    }
}

// 全局配置可以通过AbpModule.ConfigureServices方法中进行配置
Configure<AbpUnitOfWorkDefaultOptions>(options =>
{
    // 关闭自动事务，开发人员自己判断哪些地方需要事务，显示的开启
    options.TransactionBehavior = UnitOfWorkTransactionBehavior.Disabled;
});
```

## 通过Abp中的工作单元测试代码认识工作单元嵌套，保留工作单元

### 嵌套工作单元

```java

[Fact]
public async Task UnitOfWorkManager_Current_Should_Set_Correctly()
{
    _unitOfWorkManager.Current.ShouldBeNull();

    using (var uow1 = _unitOfWorkManager.Begin())
    {
        // 当前工作单元指向最外层uow1
        _unitOfWorkManager.Current.ShouldNotBeNull();
        _unitOfWorkManager.Current.ShouldBe(uow1);

        // 创建子工作单元
        using (var uow2 = _unitOfWorkManager.Begin())
        {
             // 当前工作单元指向最外层uow1
            _unitOfWorkManager.Current.ShouldNotBeNull();
            _unitOfWorkManager.Current.Id.ShouldBe(uow1.Id);

            await uow2.CompleteAsync();
        }

        _unitOfWorkManager.Current.ShouldNotBeNull();
        _unitOfWorkManager.Current.ShouldBe(uow1);

        await uow1.CompleteAsync();
    }

    _unitOfWorkManager.Current.ShouldBeNull();
}

[Fact]
public async Task Should_Create_Nested_UnitOfWorks()
{
    _unitOfWorkManager.Current.ShouldBeNull();

    using (var uow1 = _unitOfWorkManager.Begin())
    {
         // 当前工作单元指向最外层uow1
        _unitOfWorkManager.Current.ShouldNotBeNull();
        _unitOfWorkManager.Current.ShouldBe(uow1);

        // 创建新工作单元
        using (var uow2 = _unitOfWorkManager.Begin(requiresNew: true))
        {
            // 当前工作单元指向uow2
            _unitOfWorkManager.Current.ShouldNotBeNull();
            _unitOfWorkManager.Current.Id.ShouldNotBe(uow1.Id);
            _unitOfWorkManager.Current.Id.ShouldBe(uow2.Id);
            await uow2.CompleteAsync();
        }
        // Dispose后，会将Current重新指向外层uow1
        _unitOfWorkManager.Current.ShouldNotBeNull();
        _unitOfWorkManager.Current.ShouldBe(uow1);

        await uow1.CompleteAsync();
    }

    _unitOfWorkManager.Current.ShouldBeNull();
}
```

### 保留工作单元

```java
[Fact]
public async Task UnitOfWorkManager_Reservation_Test()
{
    _unitOfWorkManager.Current.ShouldBeNull();

    // 保留工作单元，保留即隐藏的意思
    using (var uow1 = _unitOfWorkManager.Reserve("Reservation1"))
    {
        _unitOfWorkManager.Current.ShouldBeNull();
        // 保留工作单元，中开启子工作单元，是看不到隐藏的工作单元
        using (var uow2 = _unitOfWorkManager.Begin())
        {
            // 因此当前工作单元指向的是uow2
            _unitOfWorkManager.Current.ShouldNotBeNull();
            _unitOfWorkManager.Current.Id.ShouldNotBe(uow1.Id);
            _unitOfWorkManager.Current.Id.ShouldBe(uow2.Id);
            await uow2.CompleteAsync();
        }

        _unitOfWorkManager.Current.ShouldBeNull();
        // 将保留工作单元恢复出来
        _unitOfWorkManager.BeginReserved("Reservation1");
        // 恢复后，当前工作单元可见
        _unitOfWorkManager.Current.ShouldNotBeNull();
        _unitOfWorkManager.Current.Id.ShouldBe(uow1.Id);

        await uow1.CompleteAsync();
    }

    _unitOfWorkManager.Current.ShouldBeNull();
}

```

## 参考

1. https://www.cnblogs.com/myzony/p/11112288.html
2. https://github.com/ABPFrameWorkGroup/AbpDocument2Chinese/blob/master/Markdown/Abp/3.5ABP%E9%A2%86%E5%9F%9F%E5%B1%82-%E5%B7%A5%E4%BD%9C%E5%8D%95%E5%85%83.md


## 仓储、工作单元中的坑

1. https://www.cnblogs.com/myzony/p/11863489.html
2. https://www.cnblogs.com/myzony/p/11647030.html
