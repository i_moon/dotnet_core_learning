# 1. Abp_vNext中JWT传递用户身份、自动审计及扩展
<!-- TOC -->

- [AbpvNext中JWT传递用户身份、自动审计及扩展](#abpvnext中jwt传递用户身份自动审计及扩展)
    - [通过JWT传递自定义用户身份信息](#通过jwt传递自定义用户身份信息)
        - [模拟颁发一个JWT Token](#模拟颁发一个jwt-token)
        - [通过扩展Abp中的用户上下文获取自定义的JWT Token信息](#通过扩展abp中的用户上下文获取自定义的jwt-token信息)
    - [实体保存时自动给审计字段赋值](#实体保存时自动给审计字段赋值)

<!-- /TOC -->

## 1.1. 通过JWT传递自定义用户身份信息

JWT Token在API认证场景中本身可以携带一些身份信息，.net core也提供了一套对JWT的实现，很方便的实现对API接口进行JWT认证，以及对JWT Token中自定义的身份信息进行解析，最终将身份信息放入HttpContext.User属性上，User对象为一个ClaimsPrincipal类型的对象，用于专门处理身份信息。

除了约定的基础的用户ID，用户名，通常我们还可以在JWT Token中携带一些自定义的用户信息，比如在流程引擎的审批业务中，可以携带当前审批用户的审批组织信息，签章等信息。

### 1.1.1. 模拟颁发一个JWT Token
我们先模拟颁发一个JWT Token，可以通过.net提供的生成JWT Token相关的API快速生成一个JWT Token。API除了可以设置JWT Token的加密方式，过期时间，还可以自定义JWT payload的内容，以携带额外的用户信息。在Abp中定义一个接口用来模拟颁发JWT Token。

``` java 
// 1. 首先定义一个模拟颁发Token时所需的信息的Dto
public class AuthInput
{
    /// <summary>
    /// 加密KEY
    /// </summary>
    public string EncryptKey { get; set; }

    /// <summary>
    /// 用户GUID
    /// </summary>
    public string UserGUID { set; get; }

    /// <summary>
    /// 用户ID
    /// </summary>
    public string UserId { get; set; }

    /// <summary>
    /// 用户名称
    /// </summary>
    public string UserName { get; set; }

    /// <summary>
    /// 用户编码
    /// </summary>
    public string UserCode { get; set; }

    /// <summary>
    /// 签章ID
    /// </summary>
    public string SignId { set; get; }

    /// <summary>
    /// 审批岗位
    /// </summary>
    public string StationId { set; get; }

    /// <summary>
    /// 审批岗位名称
    /// </summary>
    public string StationName { set; get; }
}

// 2. 在Abp vnext中定义一个用于生成模拟的JWT Token的API接口
public class AuthAppService : ApplicationService
{

    public ApiResult GenAuthenticate(AuthInput authInput)
    {
        var jwtTokenHandler = new JwtSecurityTokenHandler();
        var authTime = DateTime.Now; //授权时间
        var expiresAt = authTime.AddSeconds(300); //过期时间 ，实际场景放在配置文件中

        var keyString = Base64UrlEncoder.Decode(authInput.EncryptKey); //设置加密KEY
        
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new Claim[]
            {
                new Claim(JwtRegisteredClaimNames.Aud, "mysoft_open_api"), // 接收者标识，实际场景放在配置文件中
                new Claim(JwtRegisteredClaimNames.Iss, "mysoft_bpm"),  // 颁发机构标识，实际场景放在配置文件中
                new Claim(MyClaimType.UserId, authInput.UserId), 
                new Claim(AbpClaimTypes.UserId, authInput.UserGUID), 
                new Claim(AbpClaimTypes.UserName, authInput.UserName), 
                new Claim(MyClaimType.UserCode, authInput.UserCode), // 扩展携带用户编码
                new Claim(MyClaimType.StationId, authInput.StationId), // 扩展携带岗位ID
                new Claim(MyClaimType.StationName, authInput.StationName), // 扩展携带岗位名
                new Claim(MyClaimType.SignId, authInput.SignId), // 扩展携带签章信息
                //new Claim(JwtClaimTypes.Email, user.Email),
                //new Claim(JwtClaimTypes.PhoneNumber, user.PhoneNumber),
            }),
            Expires = expiresAt, //过期时间
            NotBefore = authTime, //授权生效时间
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(keyString)), SecurityAlgorithms.HmacSha256Signature)
        };
        var token = jwtTokenHandler.CreateToken(tokenDescriptor);
        var tokenString = jwtTokenHandler.WriteToken(token);
        return ApiResult.Success(new {access_token = tokenString, token_type = "Bearer",});
    }
}

```

定义好模拟的API接口以后，就可以通过Swagger UI模拟调用该接口获取一个JWT token信息,调用参数示例：
 ![JwtTokenRequest.png](resource/abp_action/JwtTokenRequest.png)
接口返回值：

 ![JwtTokenResponse.png](resource/abp_action/JwtTokenResponse.png)

拿到返回结果以后，我们可以到Jwt.io上去测试一下这个JWT Token，可以看到Token的payload上已经携带上了我们传入的审批岗位等信息。
 ![JwtTokenDetail.png](resource/abp_action/JwtTokenDetail.png)

下一步我们可以看看如何在Abp vnext中去取到这些自定义的信息。

### 1.1.2. 通过扩展Abp中的用户上下文获取自定义的JWT Token信息

1. 首先扩展一个用户上下文接口：

``` java
public interface IMyCurrentUser : ICurrentUser
{
    /// <summary>
    /// 流程中心用户ID
    /// </summary>
    string UserId { get; }
    
    /// <summary>
    /// 用户编码
    /// </summary>
    string UserCode { get; }

    /// <summary>
    /// 岗位ID
    /// </summary>
    string StationId { get; }

    /// <summary>
    /// 岗位名称
    /// </summary>
    string StationName { get; }

    /// <summary>
    /// 签章ID
    /// </summary>
    string SingId { get; }
}
```

2. 定义一个用户上下文接口实现:

``` java
public class MyCurrentUser : CurrentUser, IMyCurrentUser
{
    public MyCurrentUser(ICurrentPrincipalAccessor principalAccessor) : base(principalAccessor)
    {
    }

    /// <summary>
    /// 流程中心用户ID
    /// </summary>
    public string UserId
    {
        get
        {
            return this.FindClaimValue(MyClaimType.UserId);
        }
    }


    public string UserCode
    {
        get
        {
            return this.FindClaimValue(MyClaimType.UserCode);
        }
    }

    public string StationId
    {
        get
        {
            return this.FindClaimValue(MyClaimType.StationId);
        }
    }

    public string StationName
    {
        get
        {
            return this.FindClaimValue(MyClaimType.StationName);
        }
    }

    public string SingId
    {
        get
        {
            return this.FindClaimValue(MyClaimType.SignId);
        }
    }
}

```
其中的FindClaimValue方法实现，就是从HttpContext.User中去获取ClaimsPrincipal上的信息，前面说到 .net core 会将JWT中的扩展Payload最终转换到ClaimsPrincipal中，因此我们可以通过访问该对象获取JWT Token上的Payload信息。
``` java


//CurrentUser.cs中的获取Claim方法 ，Principal即为 ClaimsPrincipal，外部传入，会从HttpContext.User设置到_principalAccessor.Principal
public virtual Claim FindClaim(string claimType)
{
    return _principalAccessor.Principal?.Claims.FirstOrDefault(c => c.Type == claimType);
}

```

3.通过注入IMyCurrentUser接口获取用户信息

```java
// 在需要获取上下文中用户信息的地方，通过属性注入IMyCurrentUser
public IMyCurrentUser MyCurrentUser { get; set; }

public IMyCurrentUser GetUser()
{
    return this.MyCurrentUser;
}

```
比如这里做一个测试接口返回用户信息，通过Swagger UI调用测试，可以看到返回结果中包含了我们在生成JWT Token时所传递的用户信息，岗位信息。

![GetUserTest.png](resource/abp_action/GetUserTest.png)

回顾前面的过程：
> 1. 调用接口传递包含用户身份信息的JWT Token
> 2. .net core内部解析JWT Token，构造HttpContext.User，将用户信息转换到该属性上。
> 3. 定义一个上下文用户信息IMyCurrentUser，通过读取HttpContext.User里的数据，即可获取Jwt Token中的用户信息，扩展信息,将值赋到IMyCurrentUser的实现类MyCurrentUser中。
> 4. 在服务中通过依赖注入，注入MyCurrentUser，即可通过MyCurrentUser获取当前用户信息

## 1.2. 实体保存时自动给审计字段赋值

前面我们通过JWT Token传递了一些用户身份信息，也通过依赖注入全局管理这个用户信息，因此我们在做实体保存的时候同样可以拿到这些用户信息。然后可以通过重写DbContext的SaveChanges方法（也就是实体保存方法），可以对实体上的一些公共属性，比如创建人、用户岗位信息，签章信息等字段进行赋值，赋值完成后在调用实际的实体保存方法即可实现审计字段自动赋值。在abp vnext中将这个审计字段赋值过程抽象出了一个接口IAuditPropertySetter，我们可以通过实现这个接口，增加自定义扩展字段的赋值逻辑。

具体过程如下：
> 1. 首先需要将公共的需要自动赋值的审计字段定义到一个审计接口中，然后让实体集成这些审计接口
> 2. 通过继承AuditPropertySetter，重写其中的SetCreationProperties，SetModificationProperties方法。在这些方法检查实体是否实现了相应的审计接口，如果实现了这些审计接口，就从注入的IMyCurrentUser中获取值然后赋值给相应的审计字段上。
> 3. 依赖注入容器中注入AuditPropertySetter的重写类

完成以上3步，皆可实现保存时自动给审计字符赋值。


1. 定义审计接口
```java 

// 审计接口
public interface IOperatorStation
{
    string StationGUID { get; set; }

    string StationName { get; set; }
}
// 业务实体
public class TaskInstance : IOperatorStation {}

```
2. 重写审计字段赋值实现
``` java 
public class MyAuditPropertySetter : AuditPropertySetter, IAuditPropertySetter
{
    // 注入我们的IMyCurrentUser接口，IMyCurrentUser实现中包含了用户信息，以及我们扩展的审批岗位、签章信息
    private IMyCurrentUser _currentUser;

    public MyAuditPropertySetter(IMyCurrentUser currentUser, ICurrentTenant currentTenant, IClock clock) : base(currentUser, currentTenant, clock)
    {
        this._currentUser = currentUser;
    }


    public void SetCreationProperties(object targetObject)
    {
        base.SetCreationProperties(targetObject);
        this.SetCreatorId(targetObject);
    }

    public void SetModificationProperties(object targetObject)
    {
        base.SetModificationProperties(targetObject);
        this.SetLastModifierId(targetObject);
    }

    public void SetDeletionProperties(object targetObject)
    {
        base.SetDeletionProperties(targetObject);
    }

    private void SetCreatorId(object targetObject)
    {
        if (targetObject is IMultiTenant multiTenantEntity)
        {
            if (multiTenantEntity.TenantId != CurrentUser.TenantId)
            {
                return;
            }
        }

        // 这里的IOperatorStation接口实际包含了2个审批岗位属性，如果实体实现了该接口，这里就可以取出当前用户上的岗位信息，对实体进行赋值
        if (targetObject is IOperatorStation operatorStation)
        {
            operatorStation.StationGUID = this._currentUser.StationId;
            operatorStation.StationName = this._currentUser.StationName;
        }

        if (targetObject is IMyCreator mustHaveCreatorObject)
        {
            if (mustHaveCreatorObject.CreatorId != default)
            {
                return;
            }

            mustHaveCreatorObject.CreatorId = _currentUser.UserId;
            mustHaveCreatorObject.CreatorName = _currentUser.UserName;
        }
    }

    private void SetLastModifierId(object targetObject)
    {
        if (targetObject is IMultiTenant multiTenantEntity)
        {
            if (multiTenantEntity.TenantId != CurrentUser.TenantId)
            {
                return;
            }
        }

        if (targetObject is IOperatorStation operatorStation)
        {
            operatorStation.StationGUID = this._currentUser.StationId;
            operatorStation.StationName = this._currentUser.StationName;
        }

        // 设置修改者
        if (targetObject is IMyModifier mustHaveModifierObject)
        {
            mustHaveModifierObject.LastModifierId = _currentUser.UserId;
            mustHaveModifierObject.LastModifierName = _currentUser.UserName;
        }
    }
}
```

3. 依赖注入容器中注入AuditPropertySetter的重写类

``` java 
// 在实现了AbpModule的Module类的ConfigureServices(ServiceConfigurationContext context)方法中调用如下方法，对默认的实现进行替换
private void ConfigureCustomAuditing(ServiceConfigurationContext context)
{
    // 替换一个默认实现
    context.Services.Replace(ServiceDescriptor.Transient(typeof(IAuditPropertySetter), typeof(MyAuditPropertySetter)));
}

```


具体的实现过程，核心逻辑还在AbpDbContext的中：
```java 

  public abstract class AbpDbContext<TDbContext> : DbContext, IEfCoreDbContext, ITransientDependency
        where TDbContext : DbContext
    {
        // 删除了不相关代码逻辑...    

        // 通过属性注入审计字段赋值实现
        public IAuditPropertySetter AuditPropertySetter { get; set; }

        // 删除了不相关代码逻辑...    

        public override async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            try
            {
                // 删除了不相关代码逻辑...    

                // 该方法实际会对审计字段进行赋值
                var changeReport = ApplyAbpConcepts();
                // 审计字段赋值完成后，调用实体的保存
                var result = await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        
                // 删除了不相关代码逻辑...    
                return result;
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new AbpDbConcurrencyException(ex.Message, ex);
            }
            finally
            {
                ChangeTracker.AutoDetectChangesEnabled = true;
            }
        }

        protected virtual EntityChangeReport ApplyAbpConcepts()
        {
            var changeReport = new EntityChangeReport();

            foreach (var entry in ChangeTracker.Entries().ToList())
            {
                ApplyAbpConcepts(entry, changeReport);
            }

            return changeReport;
        }

        protected virtual void ApplyAbpConcepts(EntityEntry entry, EntityChangeReport changeReport)
        {
            switch (entry.State)
            {
                case EntityState.Added:
                    // 创建时调用 创建相关的字段属性的处理，比如设置主键ID，赋值时间戳，审计字段 创建时间、创建人等信息
                    ApplyAbpConceptsForAddedEntity(entry, changeReport);
                    break;
                case EntityState.Modified:
                    ApplyAbpConceptsForModifiedEntity(entry, changeReport);
                    break;
                case EntityState.Deleted:
                    ApplyAbpConceptsForDeletedEntity(entry, changeReport);
                    break;
            }

            AddDomainEvents(changeReport, entry.Entity);
        }

        protected virtual void ApplyAbpConceptsForAddedEntity(EntityEntry entry, EntityChangeReport changeReport)
        {
            CheckAndSetId(entry);
            SetConcurrencyStampIfNull(entry);

            // 添加实体时，会对调用创建相关的审计字段赋值方法
            SetCreationAuditProperties(entry);
            changeReport.ChangedEntities.Add(new EntityChangeEntry(entry.Entity, EntityChangeType.Created));
        }

        protected virtual void SetCreationAuditProperties(EntityEntry entry)
        {
            //  最终会调用我们前面注入的 审计字段赋值实现类的方法，完成审计字段的赋值
            AuditPropertySetter?.SetCreationProperties(entry.Entity);
        }

       // 删除了不相关代码逻辑...    
      
    }

```