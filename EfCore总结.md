# 1. EfCore总结
<!-- TOC -->

- [EfCore总结](#efcore总结)
    - [DbContext上下文](#dbcontext上下文)
        - [通过重写DbContext的OnConfiguring方法](#通过重写dbcontext的onconfiguring方法)
        - [通过DbContextOptions构造](#通过dbcontextoptions构造)
        - [依赖注入容器托管创建](#依赖注入容器托管创建)
        - [abp vnext中的数据库连接配置](#abp-vnext中的数据库连接配置)
        - [abp vnext中连接字符串、DbContext配置原理](#abp-vnext中连接字符串dbcontext配置原理)
    - [数据迁移](#数据迁移)
        - [配置Migration迁移](#配置migration迁移)
        - [生成Migration文件](#生成migration文件)
        - [执行miration更新](#执行miration更新)
    - [数据延迟加载](#数据延迟加载)
    - [实体关系、及映射](#实体关系及映射)
        - [一对一关系](#一对一关系)
        - [一对多关系](#一对多关系)
        - [多对多关系](#多对多关系)
    - [数据操作](#数据操作)
        - [实体跟踪](#实体跟踪)
        - [级联删除](#级联删除)
        - [事务支持](#事务支持)
        - [ABP EfCore仓储源码分析](#abp-efcore仓储源码分析)
        - [ABP工作单元、事务控制](#abp工作单元事务控制)
        - [ABP中针对EF的扩展：审计字段实现、软删除、多租户](#abp中针对ef的扩展审计字段实现软删除多租户)
    - [性能优化](#性能优化)

<!-- /TOC -->

## 1.1. DbContext上下文

DBContext的构造方式

### 1.1.1. 通过重写DbContext的OnConfiguring方法
```java
public class MyDbContext : DbContext
{
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        // 连接字符串可通过配置文件获取
        optionsBuilder.UseSqlServer("Data Source=localhost,51599;Initial Catalog=bpm_engine_new;Integrated Security=False;User ID=SA;Password=Mysoft95938;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False");
        base.OnConfiguring(optionsBuilder);
    }
}

```

### 1.1.2. 通过DbContextOptions构造

DbContext本身有带参数的构造函数，因此只需构造DbContextOptions对象传入即可。
```java

public class MyDbContext : DbContext
{
    public MyDbContext(DbContextOptions options):base( options){

    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        // 连接字符串可通过配置文件获取
        optionsBuilder.UseSqlServer("Data Source=localhost,51599;Initial Catalog=bpm_engine_new;Integrated Security=False;User ID=SA;Password=Mysoft95938;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False");
        base.OnConfiguring(optionsBuilder);
    }
}

```
DbContextOptions这个对象虽然可以直接new，但是efcore提供了一个Builder生成器，可以简化我们构造该对象的过程

```java

// 构建DbContext配置生成器对象
var builder = new DbContextOptionsBuilder<MyDbContext>();
builder.UseMySql("ConnectionString");
// Options属性就是DbContextOptions<TContext>泛型版的配置对象
using(var context= new MyDbContext(builder.Options))
{

}

```

### 1.1.3. 依赖注入容器托管创建

DI容器托管创建时需要注意，DbContext本身是非线程安全的，跨线程共用同一个DbContext实例是不被允许的。因此一般不建议将实例的生命周期设置为Singleton（进程内单例）、Scoped（请求级），建议设置成Transient(线程级)

容器托管创建，同样需要设置连接信息等等，方式也一样，如果是通过OnConfiguring设置，就可以采用无参构造函数，如果是采用带DbContextOptions参数的构造函数，则需要提前构造好该参数进程注入。

> 1. IServiceCollection API配置的方式
```java
// 构造DbContextOptions对象
var builder = new DbContextOptionsBuilder<MyDbContext>();
builder.UseMySql("ConnectionString");
var dbOptions = builder.Options;

// 注册两个对象，Options是一个无状态对象，因此可以设置为单例
var services =new ServiceCollection()
.AddSingleton(dbOptions)
.AddTransient<MyDbContext>();

// 创建DI容器对象
IServiceProvider serviceProvider = services.BuildServiceProvider();

// 通过容器创建DbContext
using(var dbContext = (MyDbContext)serviceProvider.GetService(typeof(MyDbContext))){

}

```
> 2. IServiceCollection扩展API配置

abp vnext中很常见的一种配置方式，通过委托将配置对象的创建行为暴露出来，在委托方法中进行配置。使用Microsoft.Extensions.DependencyInjection.dll中的扩展方法OptionsServiceCollectionExtensions.Configure可实现配置的创建和注入过程。

```java
/// <summary>
/// Registers an action used to configure a particular type of options.
/// Note: These are run before all <seealso cref="M:Microsoft.Extensions.DependencyInjection.OptionsServiceCollectionExtensions.PostConfigure``1(Microsoft.Extensions.DependencyInjection.IServiceCollection,System.Action{``0})" />.
/// </summary>
/// <typeparam name="TOptions">The options type to be configured.</typeparam>
/// <param name="services">The <see cref="T:Microsoft.Extensions.DependencyInjection.IServiceCollection" /> to add the services to.</param>
/// <param name="name">The name of the options instance.</param>
/// <param name="configureOptions">The action used to configure the options.</param>
/// <returns>The <see cref="T:Microsoft.Extensions.DependencyInjection.IServiceCollection" /> so that additional calls can be chained.</returns>
public static IServiceCollection Configure<TOptions>(
      this IServiceCollection services,
      string name,
      Action<TOptions> configureOptions)
      where TOptions : class
    {
      if (services == null)
        throw new ArgumentNullException(nameof (services));
      if (configureOptions == null)
        throw new ArgumentNullException(nameof (configureOptions));
      services.AddOptions();
      services.AddSingleton<IConfigureOptions<TOptions>>((IConfigureOptions<TOptions>) new ConfigureNamedOptions<TOptions>(name, configureOptions));
      return services;
    }

```

配置示例

``` java
// 需要获取到 IServiceCollection
IServiceCollection services;
// 配置Option，如果有多个DbContext，需配置多个，DI容器在创建对应的DbContext时，会自动找到相应的Builder
services.Configure<DbContextOptionsBuilder<ProcessEngineDbContext>>(dbOptionBuilder =>
{
    dbOptionBuilder.UseSqlServer("");
});

```

### 1.1.4. abp vnext中的数据库连接配置

abp vnext中将数据库连接放到一个全局的DbConnectionOptions中，该对象持有一个字典对象ConnectionStrings，可将所有的连接对象名称和数据库连接放入该字典。

``` java
// 配置DbConnectionOptions，集中配置所有连接字符串
Configure<DbConnectionOptions>(options =>
{
    options.ConnectionStrings.Default = configuration.GetConnectionString(ProcessEngineDbProperties.ConnectionStringName);
    options.ConnectionStrings["ProcessEngineReport"] = configuration.GetConnectionString("ProcessEngineReport");
    options.ConnectionStrings["Bpm"] = configuration.GetConnectionString("Bpm");
});

```
每个DbContext如何找到这些DbConnectionOptions中的连接字符串了？abp vnext提供了一个ConnectionStringNameAttribute，在DbContext上打Attribute标记，指定连接字符串的名称即可。
``` java
// 打上ConnectionStringName标记，传入连接字符串名称
[ConnectionStringName(ProcessEngineDbProperties.MigrateDefaultStringName)]
public class ProcessEngineMigrationsDbContext : AbpDbContext<ProcessEngineMigrationsDbContext>{
    
}

```

### 1.1.5. abp vnext中连接字符串、DbContext配置原理
下面看下内部如何实现连接字符串的解析过程。具体步骤：首先需要知道DbContext上的连接字符串名称，然后到DbConnectionOptions对象中查找具体的连接字符串。

```java
// 如下代码位于DbContextOptionsFactory类中
// 1.解析DbContext上的ConnectionStringNameAttribute标记上的连接字符串名称
var connectionStringName = ConnectionStringNameAttribute.GetConnStringName<TDbContext>();

// 2. 通过IConnectionStringResolver.Resolve接口实现到DbConnectionOptions对象中查找具体的连接字符串
var connectionString = serviceProvider.GetRequiredService<IConnectionStringResolver>().Resolve(connectionStringName);

```
具体的默认解析类如下，另外还有一个多租户的连接字符串解析类

``` java
// 该解析类通过传入的连接字符串名称，到DbConnectionOptions对象中查找具体的连接字符串
public class DefaultConnectionStringResolver : IConnectionStringResolver, ITransientDependency
{
    protected DbConnectionOptions Options { get; }

    public DefaultConnectionStringResolver(IOptionsSnapshot<DbConnectionOptions> options)
    {
        Options = options.Value;
    }

    public virtual string Resolve(string connectionStringName = null)
    {
        //Get module specific value if provided
        if (!connectionStringName.IsNullOrEmpty())
        {
            // 通过连接字符串名称查找具体的连接字符串信息
            var moduleConnString = Options.ConnectionStrings.GetOrDefault(connectionStringName);
            if (!moduleConnString.IsNullOrEmpty())
            {
                return moduleConnString;
            }
        }
        
        //Get default value
        return Options.ConnectionStrings.Default;
    }
}
```
前面找到了连接字符串，接下来看看，如何将解析好的连接字符串信息注入到DbContext对象上。
首先解析出来的连接字符串信息会被放到AbpDbContextConfigurationContext对象上，该对象同时还持有一个DbContextOptionsBuilder类型的属性DbContextOptions，可用于生成DbContext的构造函数参数DbContextOptions。AbpDbContextConfigurationContext相当于又做了一层包装，多持有了一些连接相关的属性，方便操作，其职责最终还是为了生成这个DbContextOptions。

AbpDbContextConfigurationContext这个对象会以前面提到的以代理Action的方式向外暴露给具体的配置方法（具体Action<AbpDbContextConfigurationContext<TDbContext>>），配置方法按该Action的约定，取到AbpDbContextConfigurationContext，就可以做一些针对DbContextOptions的设置，比如设置EF的懒加载，跟踪SQL执行等等。

同时abp venxt对这些代理Action又做了一层包装，让AbpDbContextOptions这个对象持有具体的Action设置的集合,可以通过多个Action每个包含不同的职责，设置不同的选项。以上过程都包含在DbContextOptionsFactory.Create方法中。

```java
internal static class DbContextOptionsFactory
{
    public static DbContextOptions<TDbContext> Create<TDbContext>(IServiceProvider serviceProvider)
        where TDbContext : AbpDbContext<TDbContext>
    {
        // 该方法会解析出具体的连接字符串
        var creationContext = GetCreationContext<TDbContext>(serviceProvider);
        // 创建一个AbpDbContextConfigurationContext持有连接信息
        var context = new AbpDbContextConfigurationContext<TDbContext>(
            creationContext.ConnectionString,
            serviceProvider,
            creationContext.ConnectionStringName,
            creationContext.ExistingConnection
        );
        // 获取一个AbpDbContextOptions，该对象持有设置连接对象的Action代理集合
        var options = GetDbContextOptions<TDbContext>(serviceProvider);

        // 循环Action集合，执行具体的Action代理，设置连接对象相关信息
        PreConfigure(options, context);
        Configure(options, context);

        return context.DbContextOptions.Options;
    }
}
```

可以看到在abp vnext中，最终都是可以通过配置AbpDbContextOptions来配置连接对象。
```java

// DbContext的配置示例
Configure<AbpDbContextOptions>(options =>
{
    // abp vnext提供了一些扩展方法，可以快速的进行一些连接对象的配置
    options.UseSqlServer(sqlSrvOption => { sqlSrvOption.UseRowNumberForPaging(); });

    // 但是由于Action代理是没有返回值的，因此上面的方式是没法拿到内部的DbContextOptionsBuilder对象的，因此无法做一些基于DbContextOptions对象的配置。
    // 根据前面的源码分析可知，AbpDbContextOptions其内部是持有了Action<AbpDbContextConfigurationContext<TDbContext>>集合的，因此可以通过 Configure方法进行配置，该方法通过参数传入AbpDbContextConfigurationContext实例，该参数实例又持有DbContextOptionsBuilder，因此可以在代理实现方法中获取DbContextOptionsBuilder做进一步的配置，比如懒加载，日志跟踪等配置
  
  // 针对具体的DbContext配置延迟加载，SQL跟踪
  options.Configure<ProcessEngineDbContext>(context =>
  {
        // 开启懒加载
        var contextBuilder = context.UseSqlServer(sqlSrvOption => sqlSrvOption.UseRowNumberForPaging()).UseLazyLoadingProxies();

        // 开启SQL跟踪，可在配置文件里引入一个开关来开启。
        contextBuilder.UseLoggerFactory(context.ServiceProvider.GetService<ILoggerFactory>());

   });
});
```


## 1.2. 数据迁移

利用EfCore的migration功能，我们能够轻松的实现数据表的创建以及增量的更新。更新的方式可以通过命令生成增量更新SQL和建立专门的可执行程序两种方式来进行更新。

efcore会根据migration文件，以及当前配置的实体信息进行对比，能够识别到具体的表、字段的变更信息。而数据库的更新动作，则通过__EFMigrationsHistory来维护，某一个Migration执行完后就会插入到该表，重复执行时会跳过已执行过的Migration。

### 1.2.1. 配置Migration迁移

首先配置一个数据迁移使用的DbContext，考虑到职责分离，以及后续独立建表账户和运行账户，因此建议建立一个独立的DbContext，以及为迁移的DbContext设置独立的连接字符串。示例如下：

> 1. 首先需要建立一个Migration所需要的DbContext
``` java
// migration的DbContext可使用独立的连接字符串
[ConnectionStringName(ProcessEngineDbProperties.MigrateDefaultStringName)]
public class ProcessEngineMigrationsDbContext : AbpDbContext<ProcessEngineMigrationsDbContext>
{
    public ProcessEngineMigrationsDbContext(DbContextOptions<ProcessEngineMigrationsDbContext> options) 
        : base(options)
    {

    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

// abp自身不同的模块有自己的实体配置，也是一个很好的实践参考

//            builder.ConfigurePermissionManagement();
//            builder.ConfigureSettingManagement();
//            builder.ConfigureBackgroundJobs();
//            builder.ConfigureAuditLogging();
//            builder.ConfigureIdentity();
//            builder.ConfigureIdentityServer();
//            builder.ConfigureFeatureManagement();
//            builder.ConfigureTenantManagement();

        // 例如这里的流程引擎的配置，包含流程引擎的实体配置
        builder.ConfigureProcessEngine(null, false);
    }
}
// 配置自己的实体
public static class ProcessEngineDbContextModelCreatingExtensions
{
    /// <summary>
    /// 配置实体信息
    /// </summary>
    /// <param name="builder"></param>
    public static void ConfigureProcessEngine(this ModelBuilder builder)
    {
        Check.NotNull(builder, nameof(builder));

        /* Configure your own tables/entities inside here */

        builder.Entity<ProcessDefinition>();

        builder.Entity<ProcessInstance>(b =>
        {
            b.ConfigureConcurrencyStamp();
            b.ConfigureExtraProperties();
        });

        // 配置实体关系、以及级联删除
        builder.Entity<ActivityInstance>()
            .HasOne(b => b.ProcessInstance)
            .WithMany(b => b.ActivityInstances)
            .HasForeignKey(b => b.ProcessInstanceID)
            .OnDelete(DeleteBehavior.Cascade);

        builder.Entity<TransitionInstance>()
            .HasOne(b => b.ProcessInstance)
            .WithMany(b => b.TransitionInstances)
            .HasForeignKey(b => b.ProcessInstanceID)
            .OnDelete(DeleteBehavior.Cascade);

        builder.Entity<TaskInstance>()
            .HasOne(b => b.ActivityInstance)
            .WithMany(b => b.Tasks)
            .HasForeignKey(b => new { b.ActivityInstanceID, b.ProcessInstanceID })
            .HasPrincipalKey(c => new { c.Id, c.ProcessInstanceID })
            .OnDelete(DeleteBehavior.Cascade);
    }
        
}

```
> 2. 建立IDesignTimeDbContextFactory实现类

```java
// 该实现类主要告诉EfCore的Migration功能在设计时如何创建DbContext
public class ProcessEngineMigrationsDbContextFactory : IDesignTimeDbContextFactory<ProcessEngineMigrationsDbContext>
    {
        public ProcessEngineMigrationsDbContext CreateDbContext(string[] args)
        {
            var configuration = BuildConfiguration(args);

            var dbType = configuration.GetSection("DbType")?.Value;
            var builder = new DbContextOptionsBuilder<ProcessEngineMigrationsDbContext>();
            if (!string.IsNullOrEmpty(dbType) && dbType == DbTypeConst.MYSQL)
            {
                builder.UseMySql(configuration.GetConnectionString(ProcessEngineDbProperties.ConnectionStringName));
            }
            else
            {
                builder.UseSqlServer(configuration.GetConnectionString(ProcessEngineDbProperties.ConnectionStringName));
            }
            return new ProcessEngineMigrationsDbContext(builder.Options);
        }

        private static IConfigurationRoot BuildConfiguration(string[] args)
        {
            var builder = new ConfigurationBuilder()
              .SetBasePath(Directory.GetCurrentDirectory())
              .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
              .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json", optional: true)
              .AddCommandLine(args)
              .AddEnvironmentVariables();

            return builder.Build();
        }
    }
```

### 1.2.2. 生成Migration文件

前面配置好了Migration所需的DbContext，就可以采用dotnet ef命令生成migration执行文件
> 1. 生成migration文件
```sh
# 进入上面建立的Migration工程目录。
# 查看ef支持的命令
dotnet ef -h
# 查看migration子命令
dotnet ef migrations -h
# 可以看到如下支持的命令
# Commands:
#  add     Adds a new migration. （添加新的Migration文件）
#  list    Lists available migrations.（查看现有的Migration文件）
#  remove  Removes the last migration.（移除指定的Migration文件）
#  script  Generates a SQL script from migrations.（根据Migration文件生成SQL）

# 如下实例：比如根据冲刺创建migration文件，会在当前目录下创建一个Migrations目录，记录数据表的创建、以及更新操作
dotnet ef migrations add 201912sp2
```
>2. 生成migration-sql

同时ef命令还支持将Migration文件生成为一个SQL文件，该SQL文件也是一个可重复执行的增量SQL文件，可以直观的查看变动的SQL，以及方便一些生产环境的更新。

```sh
# 如下命令将生成一个包含所有Migration的SQL更新脚本，可重复执行
dotnet ef migrations script -i -o Migrations/migrations-ProcessEngine.sql
```


### 1.2.3. 执行miration更新

> 1. Migration脚本更新

前面提到可以将Migration生成为SQL脚本，自然可直接把生成好的SQL拿到库里去执行更新。

> 2. 通过Migration命令更新

```sh
# ef命令同时也提供了更新数据库的命令，可通过如下命令查看具体如何使用
dotnet ef database -h 

# 将migration更新到最新
dotnet ef database update

```
**注意**：该命令需要在Migration工程的源文件目录下才能执行，因此一般仅适合开发环境使用，到成产环境就不合适了，生成环境可以采用前面的生成SQL脚本的方式更新，以及采用后面要介绍的方式来执行更为合适。


> 3. 封装可执行程序调用migration api的方式执行更新

该方式是借助DbContext.Database.MigrateAsync()方法来执行，可以建立一个独立的Console应用，在Console应用中构造好DbContext，然后去调用该方法即可执行更新。abp vnext的模板工程就是采用该种方式进行更新。

```sh
# 比如，建立一个Mysoft.ProcessEngine.DbMigrator的Console类型的工程，通过如下命令更新
dotnet  Mysoft.ProcessEngine.DbMigrator.dll
```


## 1.3. 数据延迟加载

延迟加载需要引入一个EFCore的扩展程序集：Microsoft.EntityFrameworkCore.Proxies，该程序集提供了DbContextBuilder的扩展方法UseLazyLoadingProxies()来开启DbContext的延迟加载。开启延迟加载后，对实体的导航属性有了一个强制的要求，必须是虚方法。具体配置参见前面的 [abp vnext中连接字符串、DbContext配置原理](#abp-vnext中连接字符串dbcontext配置原理)

开启延迟加载后，不实际访问导航属性，就不会从数据库加载对应的导航属性数据，某些场景下可以作为一个优化项，加快数据访问速度。

如下示例：ProcessInstance实体包含两个virtual修饰符的导航属性集合ActivityInstances、TransitionInstances，否则运行时会报错。
```java

[Table("WfProcessInstance")]
public class ProcessInstance : AggregateRoot<Guid>, IMyAuditedObject<string>, IRuntimeCommonProperties
{
    public Guid ProcessDefineID { set; get; }

    #region 业务属性信息
    ......
    #endregion

    /// <summary>
    /// 流程下的活动实例信息 集合导航属性
    /// </summary>
    public virtual IList<ActivityInstance> ActivityInstances { get; set; }

    /// <summary>
    /// 实例之间的关系 集合导航属性
    /// </summary>
    public virtual IList<TransitionInstance> TransitionInstances { get; set; }

}

```

如下延迟加载示例：
```java

// abp vnext中的Find，FindAsync方法，参数includeDetail = false, 最终会调用DbSet的Find方法，如果设置为true，会调用DbSet.AsQueryable()，从数据库中查询，第一次查询后结果会被跟踪缓存
// 执行查询
var processInstance = await ProcessInstanceRepository.FindAsync(processInstanceId, false);
// Find,FindAsync方法二次查询，由于结果被缓存，不会执行查询
processInstance = await ProcessInstanceRepository.FindAsync(processInstanceId, false);
// 由于使用了lazyload，查询主实体时，只会生成查主实体的SQL，只有实际通过导航属性访问子实体时，才会生成加载子实体的SQL
var lazyLoadActivity = processInstance.ActivityInstances.FirstOrDefault();

```
开启SQL跟踪后，输出的SQL脚本可以看到上面的执行过程，两次调用FindAsync，但是仅生成了一条查询WfProcessInstance表的SQL，另外由于配置了延迟加载，并没有生成关联查询的语句一起查出关联的实体，而是在访问子实体的时候，生成一条子实体的外键查询语句。
```txt
2019-11-27 10:26:22.671 +08:00 [INF] Executing DbCommand [Parameters=[@__p_0='?' (DbType = Guid)], CommandType='"Text"', CommandTimeout='30']
SELECT TOP(1) [w].[Id], [w].[AutoPassRules], [w].[BeginTime], [w].[BusinessKey], [w].[ConcurrencyStamp], [w].[CreationTime], [w].[CreatorId], [w].[CreatorName], [w].[EndedById], [w].[EndedByName], [w].[EndedTime], [w].[ExtraProperties], [w].[FormId], [w].[InitiaterId], [w].[InitiaterName], [w].[InitiaterOrgId], [w].[IsConfirmStepPathsAndAuditors], [w].[LastModificationTime], [w].[LastModifierId], [w].[LastModifierName], [w].[LineStyle], [w].[OverdueDateTime], [w].[OverdueTreatedDateTime], [w].[PackageId], [w].[ProcessDefineID], [w].[ProcessGUID], [w].[ProcessLevel], [w].[ProcessName], [w].[ProcessState], [w].[SerialNum], [w].[Version]
FROM [WfProcessInstance] AS [w]
WHERE ([w].[Id] = @__p_0) AND @__p_0 IS NOT NULL

2019-11-27 10:26:37.031 +08:00 [INF] Executing DbCommand [Parameters=[@__p_0='?' (DbType = Guid)], CommandType='"Text"', CommandTimeout='30']
SELECT [w].[Id], [w].[ActivityGUID], [w].[ActivityLogicOrder], [w].[ActivityName], [w].[ActivityOrder], [w].[ActivityState], [w].[ActivityType], [w].[ApprovalRights], [w].[ArchivedDirectoryId], [w].[AttachmentControl], [w].[BeginById], [w].[BeginByName], [w].[BeginDateTime], [w].[BusinessKey], [w].[CreationTime], [w].[CreatorId], [w].[CreatorName], [w].[DataControl], [w].[Description], [w].[EndedById], [w].[EndedByName], [w].[EndedTime], [w].[GatewayDirectionTypeID], [w].[IsCurTransaction], [w].[LastModificationTime], [w].[LastModifierId], [w].[LastModifierName], [w].[OpinionBackfillDomain], [w].[OpinionBackfillingMode], [w].[ProcessGUID], [w].[ProcessInstanceID], [w].[RejectFromActivityGUID], [w].[RollBackType], [w].[StepAttrs], [w].[WorkItemType]
FROM [WfActivityInstance] AS [w]
WHERE ([w].[ProcessInstanceID] = @__p_0) AND @__p_0 IS NOT NULL
```


## 1.4. 实体关系、及映射

### 1.4.1. 一对一关系

一对一的关系的常见的场景，比如拆表场景，对应一个实体记录常用数据，一个实体记录一些扩展的属性，不经常用到的属性，大字段等，配置延迟加载特性，也是一种性能优化的手段。

一对多关系在两侧都有一个引用导航属性。借用EFCore官方示例如下：

```java
public class Blog
{
    //主键，默认的主键约定 实体名+Id
    public int BlogId { get; set; }
    public string Url { get; set; }
    //导航属性
    public BlogImage BlogImage { get; set; }
}

public class BlogImage
{
     //主键，默认的主键约定 实体名+Id
    public int BlogImageId { get; set; }
    public byte[] Image { get; set; }
    public string Caption { get; set; }

    // 外键约定 主实体主键ID
    public int BlogId { get; set; }
    // 导航属性，反向引用主实体
    public Blog Blog { get; set; }
}
```
除了约定配置外，还可以通过Fluent API的方式进行配置
```java
class MyContext : DbContext
{
    public DbSet<Blog> Blogs { get; set; }
    public DbSet<BlogImage> BlogImages { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // Blog实体有一个BlogImage，同时BlogImage也有一个Blog实体
        modelBuilder.Entity<Blog>()
            .HasOne(p => p.BlogImage)
            .WithOne(i => i.Blog)
            // .HasForeignKey<BlogImage>(b => b.BlogForeignKey); //如果外键不是约定的规则，也可以直接指定
            .HasForeignKey<BlogImage>(b => b.BlogId);
    }
}
```

### 1.4.2. 一对多关系

一对多的关系也比较常见，比如主表和明细表这种关系。以前面的 [数据延迟加载](#数据延迟加载) 小节中介绍的示例为例,其中ActivityInstances，TransitionInstances是作为流程实例的一部分的，是一个典型的一对多的关系，通过定义两个集合导航属性。就默认建立了两个一对多的关系。

```java

[Table("WfProcessInstance")]
public class ProcessInstance : AggregateRoot<Guid>, IMyAuditedObject<string>, IRuntimeCommonProperties
{
    public Guid ProcessDefineID { set; get; }

    #region 业务属性信息
    ......
    #endregion

    /// <summary>
    /// 流程下的活动实例信息 集合导航属性
    /// </summary>
    public virtual IList<ActivityInstance> ActivityInstances { get; set; }

    /// <summary>
    /// 实例之间的关系 集合导航属性
    /// </summary>
    public virtual IList<TransitionInstance> TransitionInstances { get; set; }

}

```
通过Fluent API设置一对多的关系
```java

// 配置自己的实体
public static class ProcessEngineDbContextModelCreatingExtensions
{
    /// <summary>
    /// 配置实体信息
    /// </summary>
    /// <param name="builder"></param>
    public static void ConfigureProcessEngine(this ModelBuilder builder)
    {
        Check.NotNull(builder, nameof(builder));

        /* Configure your own tables/entities inside here */

        builder.Entity<ProcessDefinition>();

        builder.Entity<ProcessInstance>(b =>
        {
            b.ConfigureConcurrencyStamp();
            b.ConfigureExtraProperties();
        });

        // ProcessInstance包含ActivityInstance、TransitionInstance两个导航属性集合，可以反向的在ActivityInstance、TransitionInstance实体上配置一对多
        builder.Entity<ActivityInstance>()
            .HasOne(b => b.ProcessInstance)
            .WithMany(b => b.ActivityInstances)
            .HasForeignKey(b => b.ProcessInstanceID)
            .OnDelete(DeleteBehavior.Cascade);

        builder.Entity<TransitionInstance>()
            .HasOne(b => b.ProcessInstance)
            .WithMany(b => b.TransitionInstances)
            .HasForeignKey(b => b.ProcessInstanceID)
            .OnDelete(DeleteBehavior.Cascade);

        // ActivityInstance实体包含TaskInstance实体导航属性集合
        builder.Entity<TaskInstance>()
            .HasOne(b => b.ActivityInstance)
            .WithMany(b => b.Tasks)
            .HasForeignKey(b => new { b.ActivityInstanceID, b.ProcessInstanceID })
            .HasPrincipalKey(c => new { c.Id, c.ProcessInstanceID })
            .OnDelete(DeleteBehavior.Cascade);
    }
        
}

```

### 1.4.3. 多对多关系

多对多关系也比较常见，比如常见的角色和成员的模型，一个角色可以包含多个成员，一个成员也可以有多个角色，在数据库中需要建立关系表，在EFCore中也可以通过类似建立关联实体的方式来表达多对多的关系。相当两个关联实体对关联关系实体的一对多。

```java
class MyContext : DbContext
{
    public DbSet<User> Users { get; set; }
    public DbSet<Role> Roles { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // 联合键
        modelBuilder.Entity<User2Role>()
            .HasKey(t => new { t.UserId, t.RoleId });

        // 关系实体User2Role有一个Role实体，一个Role实体又关联了多个User2Role关系实体
        modelBuilder.Entity<User2Role>()
            .HasOne(ur => ur.Role)
            .WithMany(r => r.User2Roles)
            .HasForeignKey(ur => ur.RoleId);
        
        // 关系实体User2Role有一个User实体，一个User实体又关联了多个User2Role关系实体
        modelBuilder.Entity<User2Role>()
            .HasOne(ur => ur.User)
            .WithMany(u => u.User2Roles)
            .HasForeignKey(ur => ur.UserId);
    }
}

public class User
{
    public Guid UserId { get; set; }
    // 包含多个关系实体
    public virtual List<User2Role> User2Roles { get; set; }
}

public class Role
{
    public Guid RoleId { get; set; }
    // 包含多个关系实体
    public virtual List<User2Role> User2Roles { get; set; }
}

// 关系实体，包含两个相关实体
public class User2Role
{
    public Guid UserId { get; set; }
    public virtual User User { get; set; }

    public Guid RoleId { get; set; }
    public virtual Role Role { get; set; }
}

```

## 1.5. 数据操作

### 1.5.1. 实体跟踪

EfCore中的查询会自动对返回的实体进行跟踪，另外对DbSet的Add，Remove的行为也会进行跟踪。实体跟踪采用主键进行跟踪。实体有4种状态。

 1. Added状态：实体在数据库中不存在，比如新添加到DbSet中的实体，在SaveChange时，会产生插入语句
 2. UnChanged： 实体在数据中存在，但是未做修改，在SaveChange时，会忽略。
 3. Modified：实体在数据中存在，做了修改，在SaveChange时，仅对修改的属性产生Update更新语句。
 4. Deleted：实体在数据库中，从DbSet中移除后，实体被标记删除，在SaveChange时，产生删除语句。

如何标记上面的状态了？efcore内部其实是为每个实体维护了3个值，通过比较这3个值，可以知道哪些是新增的，哪些是修改的，哪些是删除的：

1. Current Value：当前值（删除的实体没有该值）
2. Original Value：原始值，就是从数据库中刚提取出来的值
3. Database Value：数据库中对应记录的对应字段的值（新增的实体没有该值）

实体被跟踪以后会对查询、更新有一些影响需要注意：

1. Find()、FindAsync()方法会对查询结果缓存，多次调用查询相同主键的结果，只会在第一次查询时生成SQL。
2. DbSet.AsQueryable()后的操作，实体值也会被跟踪，但是需要注意每次查询都会生成SQL查询语句，虽然不会用新查询出的结果去覆盖已被跟踪的实体值。
3. 调用DbContext.Update()方法和设置实体状态为EntityState.Modified，会更新实体的所有字段，如果仅需更新部分字段，直接获取实体后，修改字段值，调用DbContext.SaveChange即可。

```java
// 在abp中这里的ProcessInstanceRepository的查询操作实际是调用的DbSet.AsQueryable()后的操作。
// 执行第一次查询，产生SQL查询
var processInstance = ProcessInstanceRepository.FirstOrDefault(item=>item.Id==processInstanceId);
// 修改了值
processInstance.ProcessName = processInstance.ProcessName + "-1";
// 二次查询，再次产生SQL查询，但是不会用第二次查询结果覆盖第一次已被跟踪的查询结果。
var processInstance2 = ProcessInstanceRepository.FirstOrDefault(item=>item.Id==processInstanceId);       
```
跟踪SQL结果如下：产生两次查询，最后产生一次更新
```txt
2019-11-28 01:22:16.550 +08:00 [INF] Executing DbCommand [Parameters=[@__processInstanceId_0='?' (DbType = Guid)], CommandType='"Text"', CommandTimeout='30']
SELECT TOP(1) [w].[Id], [w].[AutoPassRules], [w].[BeginTime], [w].[BusinessKey], [w].[ConcurrencyStamp], [w].[CreationTime], [w].[CreatorId], [w].[CreatorName], [w].[EndedById], [w].[EndedByName], [w].[EndedTime], [w].[ExtraProperties], [w].[FormId], [w].[InitiaterId], [w].[InitiaterName], [w].[InitiaterOrgId], [w].[IsConfirmStepPathsAndAuditors], [w].[LastModificationTime], [w].[LastModifierId], [w].[LastModifierName], [w].[LineStyle], [w].[OverdueDateTime], [w].[OverdueTreatedDateTime], [w].[PackageId], [w].[ProcessDefineID], [w].[ProcessGUID], [w].[ProcessLevel], [w].[ProcessName], [w].[ProcessState], [w].[SerialNum], [w].[Version]
FROM [WfProcessInstance] AS [w]
WHERE ([w].[Id] = @__processInstanceId_0) AND @__processInstanceId_0 IS NOT NULL

2019-11-28 01:22:19.775 +08:00 [INF] Executing DbCommand [Parameters=[@__processInstanceId_0='?' (DbType = Guid)], CommandType='"Text"', CommandTimeout='30']
SELECT TOP(1) [w].[Id], [w].[AutoPassRules], [w].[BeginTime], [w].[BusinessKey], [w].[ConcurrencyStamp], [w].[CreationTime], [w].[CreatorId], [w].[CreatorName], [w].[EndedById], [w].[EndedByName], [w].[EndedTime], [w].[ExtraProperties], [w].[FormId], [w].[InitiaterId], [w].[InitiaterName], [w].[InitiaterOrgId], [w].[IsConfirmStepPathsAndAuditors], [w].[LastModificationTime], [w].[LastModifierId], [w].[LastModifierName], [w].[LineStyle], [w].[OverdueDateTime], [w].[OverdueTreatedDateTime], [w].[PackageId], [w].[ProcessDefineID], [w].[ProcessGUID], [w].[ProcessLevel], [w].[ProcessName], [w].[ProcessState], [w].[SerialNum], [w].[Version]
FROM [WfProcessInstance] AS [w]
WHERE ([w].[Id] = @__processInstanceId_0) AND @__processInstanceId_0 IS NOT NULL

2019-11-28 01:22:45.510 +08:00 [INF] Executing DbCommand [Parameters=[@p3='?' (DbType = Guid), @p0='?' (Size = 4000), @p4='?' (Size = 4000), @p1='?' (DbType = DateTime2), @p2='?' (Size = 200)], CommandType='"Text"', CommandTimeout='30']
SET NOCOUNT ON;
UPDATE [WfProcessInstance] SET [ConcurrencyStamp] = @p0, [LastModificationTime] = @p1, [ProcessName] = @p2
WHERE [Id] = @p3 AND [ConcurrencyStamp] = @p4;
SELECT @@ROWCOUNT;
```


### 1.5.2. 级联删除

仅需在配置实体关系时，配置级联删除，如下配置，删除ProcessInstance，会级联删除ActivityInstance、TransitionInstance。实际上是在生成数据表时，配置了数据库表的级联删除
```java
 builder.Entity<ProcessInstance>(b =>
{
    b.ConfigureConcurrencyStamp();
    b.ConfigureExtraProperties();
});

builder.Entity<ActivityInstance>()
    .HasOne(b => b.ProcessInstance)
    .WithMany(b => b.ActivityInstances)
    .HasForeignKey(b => b.ProcessInstanceID)
    .OnDelete(DeleteBehavior.Cascade);

builder.Entity<TransitionInstance>()
    .HasOne(b => b.ProcessInstance)
    .WithMany(b => b.TransitionInstances)
    .HasForeignKey(b => b.ProcessInstanceID)
    .OnDelete(DeleteBehavior.Cascade);
```

当我们通过ef的migration功能生成数据库脚本时，可以看到表上建立了级联删除的配置,示例如下：

```sql
CREATE TABLE [WfActivityInstance] (
        [Id] uniqueidentifier NOT NULL,
         // 删除了不相关字段...
        [ProcessInstanceID] uniqueidentifier NOT NULL,
        CONSTRAINT [PK_WfActivityInstance] PRIMARY KEY ([Id]),
        CONSTRAINT [AK_WfActivityInstance_Id_ProcessInstanceID] UNIQUE ([Id], [ProcessInstanceID]),
        CONSTRAINT [FK_WfActivityInstance_WfProcessInstance_ProcessInstanceID] FOREIGN KEY ([ProcessInstanceID]) REFERENCES [WfProcessInstance] ([Id]) ON DELETE CASCADE
    );
```

### 1.5.3. 事务支持

### 1.5.4. ABP EfCore仓储源码分析

```java

 public class EfCoreRepository<TDbContext, TEntity> : RepositoryBase<TEntity>, IEfCoreRepository<TEntity>
        where TDbContext : IEfCoreDbContext
        where TEntity : class, IEntity
    {
        public virtual DbSet<TEntity> DbSet => DbContext.Set<TEntity>();

        DbContext IEfCoreRepository<TEntity>.DbContext => DbContext.As<DbContext>();

        protected virtual TDbContext DbContext => _dbContextProvider.GetDbContext();

        protected virtual EntityOptions<TEntity> EntityOptions => _entityOptionsLazy.Value;

        private readonly IDbContextProvider<TDbContext> _dbContextProvider;
        private readonly Lazy<EntityOptions<TEntity>> _entityOptionsLazy;

        public EfCoreRepository(IDbContextProvider<TDbContext> dbContextProvider)
        {
            _dbContextProvider = dbContextProvider;

            _entityOptionsLazy = new Lazy<EntityOptions<TEntity>>(
                () => ServiceProvider
                          .GetRequiredService<IOptions<EntityOptions>>()
                          .Value
                          .GetOrNull<TEntity>() ?? EntityOptions<TEntity>.Empty
            );
        }

        public override async Task<TEntity> InsertAsync(TEntity entity, bool autoSave = false, CancellationToken cancellationToken = default)
        {
            var savedEntity = DbSet.Add(entity).Entity;

            if (autoSave)
            {
                await DbContext.SaveChangesAsync(GetCancellationToken(cancellationToken));
            }

            return savedEntity;
        }
        
        // 同时调用Attach、Update，会产生更新所有字段的SQL
        public override async Task<TEntity> UpdateAsync(TEntity entity, bool autoSave = false, CancellationToken cancellationToken = default)
        {
            DbContext.Attach(entity);

            var updatedEntity = DbContext.Update(entity).Entity;

            if (autoSave)
            {
                await DbContext.SaveChangesAsync(GetCancellationToken(cancellationToken));
            }

            return updatedEntity;
        }

        public override async Task DeleteAsync(TEntity entity, bool autoSave = false, CancellationToken cancellationToken = default)
        {
            DbSet.Remove(entity);

            if (autoSave)
            {
                await DbContext.SaveChangesAsync(GetCancellationToken(cancellationToken));
            }
        }

        // 查询方法都带有includeDetails，如果为true，会根据实体上的EntityOptions.DefaultWithDetailsFunc配置预先加载依赖实体数据。
        public override async Task<List<TEntity>> GetListAsync(bool includeDetails = false, CancellationToken cancellationToken = default)
        {
            return includeDetails
                ? await WithDetails().ToListAsync(GetCancellationToken(cancellationToken))
                : await DbSet.ToListAsync(GetCancellationToken(cancellationToken));
        }

        protected override IQueryable<TEntity> GetQueryable()
        {
            return DbSet.AsQueryable();
        }

        // 表达式删除，先查询，后删除
        public override async Task DeleteAsync(Expression<Func<TEntity, bool>> predicate, bool autoSave = false, CancellationToken cancellationToken = default)
        {
            var entities = await GetQueryable()
                .Where(predicate)
                .ToListAsync(GetCancellationToken(cancellationToken));

            foreach (var entity in entities)
            {
                DbSet.Remove(entity);
            }

            if (autoSave)
            {
                await DbContext.SaveChangesAsync(GetCancellationToken(cancellationToken));
            }
        }

        // base.WithDetails(),实际是直接调用GetQueryable()方法
        public override IQueryable<TEntity> WithDetails()
        {
            if (EntityOptions.DefaultWithDetailsFunc == null)
            {
                return base.WithDetails();
            }

            return EntityOptions.DefaultWithDetailsFunc(GetQueryable());
        }

        public override IQueryable<TEntity> WithDetails(params Expression<Func<TEntity, object>>[] propertySelectors)
        {
            var query = GetQueryable();

            if (!propertySelectors.IsNullOrEmpty())
            {
                foreach (var propertySelector in propertySelectors)
                {
                    query = query.Include(propertySelector);
                }
            }

            return query;
        }
    }

    public class EfCoreRepository<TDbContext, TEntity, TKey> : EfCoreRepository<TDbContext, TEntity>, 
        IEfCoreRepository<TEntity, TKey>,
        ISupportsExplicitLoading<TEntity, TKey>

        where TDbContext : IEfCoreDbContext
        where TEntity : class, IEntity<TKey>
    {
        public EfCoreRepository(IDbContextProvider<TDbContext> dbContextProvider) 
            : base(dbContextProvider)
        {

        }

        public virtual async Task<TEntity> GetAsync(TKey id, bool includeDetails = true, CancellationToken cancellationToken = default)
        {
            var entity = await FindAsync(id, includeDetails, GetCancellationToken(cancellationToken));

            if (entity == null)
            {
                throw new EntityNotFoundException(typeof(TEntity), id);
            }

            return entity;
        }

        // Find方法默认根据WithDetails，实际是调用DbSet.AsQueryable()后进行查询，该操作不会对实体缓存
        // 这点含义和DbSet.Find含义不一致，此设计值得商榷。
        public virtual async Task<TEntity> FindAsync(TKey id, bool includeDetails = true, CancellationToken cancellationToken = default)
        {
            return includeDetails
                ? await WithDetails().FirstOrDefaultAsync(EntityHelper.CreateEqualityExpressionForId<TEntity, TKey>(id), GetCancellationToken(cancellationToken))
                : await DbSet.FindAsync(new object[] { id }, GetCancellationToken(cancellationToken));
        }

        public virtual async Task DeleteAsync(TKey id, bool autoSave = false, CancellationToken cancellationToken = default)
        {
            var entity = await FindAsync(id, includeDetails: false, cancellationToken: cancellationToken);
            if (entity == null)
            {
                return;
            }

            await DeleteAsync(entity, autoSave, cancellationToken);
        }
    }


```


### 1.5.5. ABP工作单元、事务控制

工作单元默认的应用范围

UnitOfWorkInterceptorRegistrar注册AOP拦截所有满足条件的对象加入工作单元控制。注册规则包括：
1. 实现了IUnitOfWorkEnabled的类型
2. 方法上有UnitOfWorkAttribute属性的
3. 通过AbpUowActionFilter添加控制器方法上的工作单元控制

可通过全局设置AbpUnitOfWorkDefaultOptions的配置类上的IsTransactional来控制工作单元的全局事务的开启和关闭，默认工单单元开启事务。

```java
// UnitOfWorkInterceptor 中的事务控制
private AbpUnitOfWorkOptions CreateOptions(IAbpMethodInvocation invocation, [CanBeNull] UnitOfWorkAttribute unitOfWorkAttribute)
{
    var options = new AbpUnitOfWorkOptions();

    unitOfWorkAttribute?.SetOptions(options);

    if (unitOfWorkAttribute?.IsTransactional == null)
    {
        // 以显示的UnitOfWork属性上的值为优先，如果没有设置，则以采用全局配置
        // 类中的Get开头的方法，不加事务控制， _defaultOptions为AbpUnitOfWorkDefaultOptions类型，可通过DI全局注入配置
        options.IsTransactional = _defaultOptions.CalculateIsTransactional(
            autoValue: !invocation.Method.Name.StartsWith("Get", StringComparison.InvariantCultureIgnoreCase)
        );
    }

    return options;
}

// AbpUowActionFilter 上的事务控制
private AbpUnitOfWorkOptions CreateOptions(ActionExecutingContext context, UnitOfWorkAttribute unitOfWorkAttribute)
{
    var options = new AbpUnitOfWorkOptions();

    unitOfWorkAttribute?.SetOptions(options);

    if (unitOfWorkAttribute?.IsTransactional == null)
    {
        // HttpGet请求的，不加事务控制， _defaultOptions为AbpUnitOfWorkDefaultOptions类型，可通过DI全局注入配置
        options.IsTransactional = _defaultOptions.CalculateIsTransactional(
            autoValue: !string.Equals(context.HttpContext.Request.Method, HttpMethod.Get.Method, StringComparison.OrdinalIgnoreCase)
        );
    }

    return options;
}

// UnitOfWorkDbContextProvider类中控制事务开启


private TDbContext CreateDbContext(IUnitOfWork unitOfWork)
{
    // 不开启事务，则直接返回DBContext
    return unitOfWork.Options.IsTransactional
        ? CreateDbContextWithTransaction(unitOfWork)
        : unitOfWork.ServiceProvider.GetRequiredService<TDbContext>();
}

public TDbContext CreateDbContextWithTransaction(IUnitOfWork unitOfWork) 
{
    var transactionApiKey = $"EntityFrameworkCore_{DbContextCreationContext.Current.ConnectionString}";
    var activeTransaction = unitOfWork.FindTransactionApi(transactionApiKey) as EfCoreTransactionApi;

    if (activeTransaction == null)
    {
        var dbContext = unitOfWork.ServiceProvider.GetRequiredService<TDbContext>();

        var dbtransaction = unitOfWork.Options.IsolationLevel.HasValue
            ? dbContext.Database.BeginTransaction(unitOfWork.Options.IsolationLevel.Value)
            : dbContext.Database.BeginTransaction();

        unitOfWork.AddTransactionApi(
            transactionApiKey,
            new EfCoreTransactionApi(
                dbtransaction,
                dbContext
            )
        );

        return dbContext;
    }
    else
    {
        DbContextCreationContext.Current.ExistingConnection = activeTransaction.DbContextTransaction.GetDbTransaction().Connection;

        var dbContext = unitOfWork.ServiceProvider.GetRequiredService<TDbContext>();

        if (dbContext.As<DbContext>().HasRelationalTransactionManager())
        {
            dbContext.Database.UseTransaction(activeTransaction.DbContextTransaction.GetDbTransaction());
        }
        else
        {
            dbContext.Database.BeginTransaction(); //TODO: Why not using the new created transaction?
        }

        activeTransaction.AttendedDbContexts.Add(dbContext);

        return dbContext;
    }
}


```


### 1.5.6. ABP中针对EF的扩展：审计字段实现、软删除、多租户

abp提供了一个集成DbContext的类AbpDbContext，在该类中重写了SaveChanges方法，该方法中在调用基类的SaveChange之前，会做一些审计字段的赋值、以及软删除的处理动作。相关方法如下：
```java
public override async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
{
    try
    {
        var auditLog = AuditingManager?.Current?.Log;

        List<EntityChangeInfo> entityChangeList = null;
        if (auditLog != null)
        {
            entityChangeList = EntityHistoryHelper.CreateChangeList(ChangeTracker.Entries().ToList());
        }
        // 先处理审计字段的赋值等等操作
        var changeReport = ApplyAbpConcepts();
        // 再调用基类DbContext的SaveChange
        var result = await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);

        await EntityChangeEventHelper.TriggerEventsAsync(changeReport);

        if (auditLog != null)
        {
            EntityHistoryHelper.UpdateChangeList(entityChangeList);
            auditLog.EntityChanges.AddRange(entityChangeList);
            Logger.LogDebug($"Added {entityChangeList.Count} entity changes to the current audit log");
        }

        return result;
    }
    catch (DbUpdateConcurrencyException ex)
    {
        throw new AbpDbConcurrencyException(ex.Message, ex);
    }
    finally
    {
        ChangeTracker.AutoDetectChangesEnabled = true;
    }
}

protected virtual EntityChangeReport ApplyAbpConcepts()
{
    var changeReport = new EntityChangeReport();

    foreach (var entry in ChangeTracker.Entries().ToList())
    {
        ApplyAbpConcepts(entry, changeReport);
    }

    return changeReport;
}

protected virtual void ApplyAbpConcepts(EntityEntry entry, EntityChangeReport changeReport)
{
    // 根据实体的状态，做不同的保存前的处理
    switch (entry.State)
    {
        case EntityState.Added:
            ApplyAbpConceptsForAddedEntity(entry, changeReport);
            break;
        case EntityState.Modified:
            ApplyAbpConceptsForModifiedEntity(entry, changeReport);
            break;
        case EntityState.Deleted:
            ApplyAbpConceptsForDeletedEntity(entry, changeReport);
            break;
    }

    AddDomainEvents(changeReport, entry.Entity);
}

protected virtual void ApplyAbpConceptsForAddedEntity(EntityEntry entry, EntityChangeReport changeReport)
{
    CheckAndSetId(entry);
    SetConcurrencyStampIfNull(entry);
    // 新建实体时，处理新增的审计字段赋值
    SetCreationAuditProperties(entry);
    changeReport.ChangedEntities.Add(new EntityChangeEntry(entry.Entity, EntityChangeType.Created));
}

protected virtual void ApplyAbpConceptsForModifiedEntity(EntityEntry entry, EntityChangeReport changeReport)
{
    UpdateConcurrencyStamp(entry);
    // 修改时，处理修改的审计字段赋值
    SetModificationAuditProperties(entry);
    // 修改时处理软删
    if (entry.Entity is ISoftDelete && entry.Entity.As<ISoftDelete>().IsDeleted)
    {
        SetDeletionAuditProperties(entry);
        changeReport.ChangedEntities.Add(new EntityChangeEntry(entry.Entity, EntityChangeType.Deleted));
    }
    else
    {
        changeReport.ChangedEntities.Add(new EntityChangeEntry(entry.Entity, EntityChangeType.Updated));
    }
}

protected virtual void ApplyAbpConceptsForDeletedEntity(EntityEntry entry, EntityChangeReport changeReport)
{
    CancelDeletionForSoftDelete(entry);
    UpdateConcurrencyStamp(entry);
    SetDeletionAuditProperties(entry);
    changeReport.ChangedEntities.Add(new EntityChangeEntry(entry.Entity, EntityChangeType.Deleted));
}

// 具体的处理审计字段赋值，则是检查实体是否继承了审计字段接口，比如继承IHasCreationTime，就会对创建时间赋值
// 另外对于实现了IMustHaveCreator接口的会设置创建人字段，创建人的取值则会通过注入ICurrentUser对象，从中获取，该对象Abp框架在接收到请求构造好后，会注入到DI容器中，后续直接通过构造函数注入取得该对象
```





软删和单库的多组模式都是采用HasQueryFilter进行配置

## 1.6. 性能优化

1. EfCore所执行的SQL跟踪配置
2. EF实体缓存相关
3. 触发执行SQL查询的API与内存操作的API
