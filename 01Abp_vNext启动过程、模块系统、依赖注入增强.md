# Abp vNext启动过程、模块系统、依赖注入增强
<!-- TOC -->

- [Abp vNext启动过程、模块系统、依赖注入增强](#abp-vnext%E5%90%AF%E5%8A%A8%E8%BF%87%E7%A8%8B%E6%A8%A1%E5%9D%97%E7%B3%BB%E7%BB%9F%E4%BE%9D%E8%B5%96%E6%B3%A8%E5%85%A5%E5%A2%9E%E5%BC%BA)
    - [服务注册](#%E6%9C%8D%E5%8A%A1%E6%B3%A8%E5%86%8C)
    - [配置应用请求管道](#%E9%85%8D%E7%BD%AE%E5%BA%94%E7%94%A8%E8%AF%B7%E6%B1%82%E7%AE%A1%E9%81%93)
    - [Abp中的模块化系统](#abp%E4%B8%AD%E7%9A%84%E6%A8%A1%E5%9D%97%E5%8C%96%E7%B3%BB%E7%BB%9F)
        - [模块系统的中的依赖加载机制](#%E6%A8%A1%E5%9D%97%E7%B3%BB%E7%BB%9F%E7%9A%84%E4%B8%AD%E7%9A%84%E4%BE%9D%E8%B5%96%E5%8A%A0%E8%BD%BD%E6%9C%BA%E5%88%B6)
        - [模块系统中的服务注册](#%E6%A8%A1%E5%9D%97%E7%B3%BB%E7%BB%9F%E4%B8%AD%E7%9A%84%E6%9C%8D%E5%8A%A1%E6%B3%A8%E5%86%8C)
        - [模块系统中的模块生命周期](#%E6%A8%A1%E5%9D%97%E7%B3%BB%E7%BB%9F%E4%B8%AD%E7%9A%84%E6%A8%A1%E5%9D%97%E7%94%9F%E5%91%BD%E5%91%A8%E6%9C%9F)
    - [依赖注入增强](#%E4%BE%9D%E8%B5%96%E6%B3%A8%E5%85%A5%E5%A2%9E%E5%BC%BA)
        - [服务自动注册](#%E6%9C%8D%E5%8A%A1%E8%87%AA%E5%8A%A8%E6%B3%A8%E5%86%8C)
    - [AOP增强](#aop%E5%A2%9E%E5%BC%BA)
        - [AOP拦截示例](#aop%E6%8B%A6%E6%88%AA%E7%A4%BA%E4%BE%8B)
        - [Abp中的方法拦截实现](#abp%E4%B8%AD%E7%9A%84%E6%96%B9%E6%B3%95%E6%8B%A6%E6%88%AA%E5%AE%9E%E7%8E%B0)
    - [其他](#%E5%85%B6%E4%BB%96)

<!-- /TOC -->
`Abp vNext`作为一个基于dotnet core的应用框架，整体启动流程和微软官方给出的示例中的启动配置流程并无差异。主要包括两个过程：

1. 配置应用服务（或者说服务注册）
2. 配置应用请求管道。

而这两个过程分别对应`Startup.cs`文件中`ConfigureServices`和`Configure`方法。参考：[ASP.NET CORE中的启动过程](https://docs.microsoft.com/zh-cn/aspnet/core/fundamentals/startup?view=aspnetcore-3.1)

`Abp vNext`的模板项目中的`Startup.cs`入口文件非常简单，因为服务注册和配置应用请求管道的过程被放到了`AbpModule`中的对应的方法里进行配置。这样做的好处是，可以将每个模块的自身服务注册过程，以及应用管道配置放到各个模块中完成。对于那些同时有WebHost和Cli控制台的应用，可以公用一些配置。

```java
public class Startup
{
    public void ConfigureServices(IServiceCollection services)
    {
        // 进行服务配置
        services.AddApplication<ProcessEngineHttpApiHostModule>();
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
    {
        // 进行应用请求管道配置
        app.InitializeApplication();
    }
}
```

## 服务注册

`dotnet core`中引入`IServiceCollection`作为IOC容器的抽象，用于注册服务接口与实现，服务注册过程通常直接放在`Startup.ConfigureServices`方法中完成。在Abp中，服务注册分为2个部分：

1. Abp本身的服务注册动作是在构造Abp应用的过程中注册 
2. 需要自定义注册的服务，则是放到了各个模块`AbpModule`的`ConfigureServices`方法中完成注册。

 服务注册的过程从前面的`IServiceCollection.AddApplication`开始，这个方法构造了一个Abp应用（继承自`IAbpApplication`），从Abp应用的构造开始，默认会注册dotnet core提供的一些基础的服务，比如日志、多语言本地化，还会注册Abp自身核心的模块化相关的服务。同时结合模块的加载，同时还对服务注册部分做了增强，提供了一些自动注册服务的机制，简化服务注册过程。

`IServiceCollection.AddApplication`方法定义如下，该方法最终会通过`AbpApplicationFactory`工厂来创建一个`IAbpApplication`应用接口的实现`AbpApplicationWithExternalServiceProvider`。

> **提示：** 
> Abp抽象出了一个应用(IAbpApplication)的概念，而应用下又包含了若干个模块(AbpModule)
 
```java

public static class ServiceCollectionApplicationExtensions
{
    public static IAbpApplicationWithExternalServiceProvider AddApplication<TStartupModule>(
        [NotNull] this IServiceCollection services, 
        [CanBeNull] Action<AbpApplicationCreationOptions> optionsAction = null)
        where TStartupModule : IAbpModule
    {
        // 应用工厂方法，创建应用实例AbpApplicationWithExternalServiceProvider
        return AbpApplicationFactory.Create<TStartupModule>(services, optionsAction);
    }
    
}

public static class AbpApplicationFactory
{

    public static IAbpApplicationWithExternalServiceProvider Create<TStartupModule>(
        [NotNull] IServiceCollection services,
        [CanBeNull] Action<AbpApplicationCreationOptions> optionsAction = null)
        where TStartupModule : IAbpModule
    {
        return Create(typeof(TStartupModule), services, optionsAction);
    }

    public static IAbpApplicationWithExternalServiceProvider Create(
        [NotNull] Type startupModuleType,
        [NotNull] IServiceCollection services,
        [CanBeNull] Action<AbpApplicationCreationOptions> optionsAction = null)
    {
        // 创建应用
        return new AbpApplicationWithExternalServiceProvider(startupModuleType, services, optionsAction);
    }
}
```

下面看下Abp应用具体如何定义，可以看到应用包含模块描述集合，包含注册的服务，包含启动模块。
```java

public interface IModuleContainer
{
    [NotNull]
    IReadOnlyList<IAbpModuleDescriptor> Modules { get; }
}

public interface IAbpApplication : IModuleContainer, IDisposable
{
    /// <summary>
    /// Type of the startup (entrance) module of the application.
    /// 启动类型模块
    /// </summary>
    Type StartupModuleType { get; }

    /// <summary>
    /// List of services registered to this application.
    /// Can not add new services to this collection after application initialize.
    /// 服务集合（服务注册表）
    /// </summary>
    IServiceCollection Services { get; }

    /// <summary>
    /// Reference to the root service provider used by the application.
    /// This can not be used before initialize the application.
    /// 服务提供者
    /// </summary>
    IServiceProvider ServiceProvider { get; }

    /// <summary>
    /// Used to gracefully shutdown the application and all modules.
    /// 关闭应用及模块
    /// </summary>
    void Shutdown();
}
```
应用的一个实现`AbpApplicationWithExternalServiceProvider`定义如下，其主要是将自身注册到IOC容器中，并执行模块系统的初始化，服务注册的主要逻辑在基类`AbpApplicationBase`的构造函数中实现:

```java
internal class AbpApplicationWithExternalServiceProvider : AbpApplicationBase, IAbpApplicationWithExternalServiceProvider
{
    public AbpApplicationWithExternalServiceProvider(
        [NotNull] Type startupModuleType, 
        [NotNull] IServiceCollection services, 
        [CanBeNull] Action<AbpApplicationCreationOptions> optionsAction
        ) : base(
            startupModuleType, 
            services, 
            optionsAction)
    {
        // 将自己注册到IOC容器中，注册为单例
        services.AddSingleton<IAbpApplicationWithExternalServiceProvider>(this);
    }

    public void Initialize(IServiceProvider serviceProvider)
    {
        // 应用初始化，实际就是设置IServiceProvider实例，以及执行模块初始化，在Startup.Configure中调用该方法

        Check.NotNull(serviceProvider, nameof(serviceProvider));

        SetServiceProvider(serviceProvider);

        InitializeModules();
    }

    public override void Dispose()
    {
        base.Dispose();

        if (ServiceProvider is IDisposable disposableServiceProvider)
        {
            disposableServiceProvider.Dispose();
        }
    }
}

public abstract class AbpApplicationBase : IAbpApplication
{
    [NotNull]
    public Type StartupModuleType { get; }

    public IServiceProvider ServiceProvider { get; private set; }

    public IServiceCollection Services { get; }

    public IReadOnlyList<IAbpModuleDescriptor> Modules { get; }

    internal AbpApplicationBase(
        [NotNull] Type startupModuleType,
        [NotNull] IServiceCollection services,
        [CanBeNull] Action<AbpApplicationCreationOptions> optionsAction)
    {
        Check.NotNull(startupModuleType, nameof(startupModuleType));
        Check.NotNull(services, nameof(services));

        // 记录启动模块
        StartupModuleType = startupModuleType;
        Services = services;

        // 对象访问器模式，注册一个空IServiceProvider实现
        services.TryAddObjectAccessor<IServiceProvider>();
        
        // 外部注入委托代理配置一些应用相关的配置
        var options = new AbpApplicationCreationOptions(services);
        optionsAction?.Invoke(options);
        
        // 将自身注册为IAbpApplication实现
        services.AddSingleton<IAbpApplication>(this);
        // 将自身注册为IModuleContainer实现
        services.AddSingleton<IModuleContainer>(this);
        
        // 添加doet net core的一些基础服务比如日志、多语言等
        services.AddCoreServices();
        // 配置Abp的核心服务，比如模块加载，程序集扫描，类型查找等
        services.AddCoreAbpServices(this, options);

        // 加载Abp的模块化系统
        Modules = LoadModules(services, options);
    }

    // 省略不相关代码...

    protected virtual void SetServiceProvider(IServiceProvider serviceProvider)
    {
        ServiceProvider = serviceProvider;
        // 对象访问器模式，延迟设置IServiceProvider的实现
        ServiceProvider.GetRequiredService<ObjectAccessor<IServiceProvider>>().Value = ServiceProvider;
    }
 

    private IReadOnlyList<IAbpModuleDescriptor> LoadModules(IServiceCollection services, AbpApplicationCreationOptions options)
    {
        // 通过IModuleLoader加载所有的模块
        return services
            .GetSingletonInstance<IModuleLoader>()
            .LoadModules(
                services,
                StartupModuleType,
                options.PlugInSources
            );
    }
}
```

## 配置应用请求管道

`Startup.Configure`的职责通常是完成应用请求管道的配置，而在Abp中这些工作则是放到了`ApbApplication`应用中的模块生命周期方法中去完成。在Abp中`Startup.Configure`方法仅仅是完成Apb应用的初始化过程，通过调用`IApplicationBuilder`的扩展方法`InitializeApplication`完成应用初始化。而这个应用初始化过程实际上干了2件事情：

1. 设置IServiceProvider实例（前面的服务注册阶段，仅仅通过对象访问器模式设置了一个IServiceProvider的占位符，实际是在应用管道配置的时候，dotnet core内部才会通过ServiceProviderFactory来创建一个ServiceProvider的实例）
2. 执行模块初始化

应用初始化方法如下：
```java

public static class AbpApplicationBuilderExtensions
{
    public static void InitializeApplication([NotNull] this IApplicationBuilder app)
    {
        // 对象访问器，设置IApplicationBuilder的具体实例
        app.ApplicationServices.GetRequiredService<ObjectAccessor<IApplicationBuilder>>().Value = app;
        // 获取服务注册过程中注册的 应用实例
        var application = app.ApplicationServices.GetRequiredService<IAbpApplicationWithExternalServiceProvider>();
     
        // 执行前面的应用初始化，实际就是设置IServiceProvider实例，以及执行模块初始化
        application.Initialize(app.ApplicationServices);
    }
}

internal class AbpApplicationWithExternalServiceProvider : AbpApplicationBase, IAbpApplicationWithExternalServiceProvider
{
    // 省略不相关代码...

    public void Initialize(IServiceProvider serviceProvider)
    {
        // 应用初始化过程：
        //1、实际就是设置IServiceProvider实例
        SetServiceProvider(serviceProvider);
        //2、执行模块初始化
        InitializeModules();
    }
}

public abstract class AbpApplicationBase : IAbpApplication
{
    [NotNull]
    public Type StartupModuleType { get; }

    public IServiceProvider ServiceProvider { get; private set; }

    public IServiceCollection Services { get; }

    public IReadOnlyList<IAbpModuleDescriptor> Modules { get; }
 
    protected virtual void SetServiceProvider(IServiceProvider serviceProvider)
    {
        ServiceProvider = serviceProvider;
        // 对象访问器模式，延迟设置IServiceProvider的实现
        ServiceProvider.GetRequiredService<ObjectAccessor<IServiceProvider>>().Value = ServiceProvider;
    }

    protected virtual void InitializeModules()
    {
        // 通过IModuleManager执行所有加载的模块的初始化（在执行应用初始化时调用）
        using (var scope = ServiceProvider.CreateScope())
        {
            scope.ServiceProvider
                .GetRequiredService<IModuleManager>()
                .InitializeModules(new ApplicationInitializationContext(scope.ServiceProvider));
        }
    }

}

```

再来看一个实际在模块的应用初始化方法中配置应用请求管道的示例：

```java
[DependsOn(
    typeof(ProcessEngineHttpApiModule),
    typeof(AbpAutofacModule),
    typeof(AbpAspNetCoreMultiTenancyModule),
    typeof(AbpAspNetCoreMvcUiMultiTenancyModule),
    typeof(ProcessEngineApplicationModule),
    typeof(ProcessEngineEntityFrameworkCoreDbMigrationsModule),
    typeof(ProcessEngineInfrastructureModule)
)]
public class ProcessEngineHttpApiHostModule : AbpModule
{
    public override void OnApplicationInitialization(ApplicationInitializationContext context)
    {
        // 可以看到这里的应用请求管道配置就是.net core模板中放在Configure中对应的配置
        var app = context.GetApplicationBuilder();

        app.UseCorrelationId();
        app.UseVirtualFiles();
        app.UseRouting();
        app.UseCors(DefaultCorsPolicyName);
        app.UseAuthentication();
        app.UseAuthorization();
        
        if (MultiTenancyConsts.IsEnabled)
        {
            app.UseMultiTenancy();
        }

        app.UseAbpRequestLocalization();

        app.UseSwagger();
        app.UseSwaggerUI(options =>
        {
            options.SwaggerEndpoint("/swagger/v1/swagger.json", "ProcessEngine API");
        });

        app.UseAuditing();
        app.UseMvcWithDefaultRouteAndArea();
        
        app.UseHangfireDashboard(); //启动hangfire面板  
    }
}
```

可以看到无论是服务的注册（自动注册），还是原来的应用请求管道的配置（模块生命周期方法中完成）都是依赖于Abp的核心模块系统的设计，下面就来看看Abp中的核心模块系统的设计

## Abp中的模块化系统

前面在讲服务注册时，能看到加载模块，调用模块初始化，以及模块销毁的方法。那这里的模块设计具体有什么作用？

最主要的一个作用，引入模块依赖关系的概念，通过模块依赖关系，以及预定义的模块生命周期方法，可以保证一些有相互依赖的资源能够按预先的顺序进行初始化和释放。


### 模块系统的中的依赖加载机制

 
模块加载逻辑：从启动模块开始，按照模块类上的DependsOnAttribute特性标记的依赖关系，递归找到所有依赖的模块。

> 提示：
> **启动模块**：前面的`Startup.ConfigureServices`方法中调用`services.AddApplication<ProcessEngineHttpApiHostModule>()`，这里的泛型类型参数，即为启动模块
 
模块加载动作是在前面介绍的`AbpApplicationBase`的构造函数当中完成的，构造函数中调用的`LoadModules`方法就是模块加载的入口方法，其又调用`IModuleLoader.LoadModules`方法进行模块加载。

模块加载入口如下：

```java
public abstract class AbpApplicationBase : IAbpApplication
{
    // 启动模块类型
    [NotNull]
    public Type StartupModuleType { get; }

    public IServiceProvider ServiceProvider { get; private set; }

    public IServiceCollection Services { get; }

    // 所有模块的模块描述信息
    public IReadOnlyList<IAbpModuleDescriptor> Modules { get; }

    internal AbpApplicationBase(
        [NotNull] Type startupModuleType,
        [NotNull] IServiceCollection services,
        [CanBeNull] Action<AbpApplicationCreationOptions> optionsAction)
    {
        Check.NotNull(startupModuleType, nameof(startupModuleType));
        Check.NotNull(services, nameof(services));

        // 记录启动模块
        StartupModuleType = startupModuleType;
        // 省略不相关代码...

        // 加载Abp的模块化系统
        Modules = LoadModules(services, options);
    }

    // 省略不相关代码...
    private IReadOnlyList<IAbpModuleDescriptor> LoadModules(IServiceCollection services, AbpApplicationCreationOptions options)
    {
        // 通过IModuleLoader加载所有的模块
        return services
            .GetSingletonInstance<IModuleLoader>()
            .LoadModules(
                services,
                StartupModuleType,
                options.PlugInSources
            );
    }
}
```

其中`LoadModules`模块加载逻实现如下：

1. 从启动模块开始，根据DependsOnAttribute标记的依赖关系，获取模块构造模块描述信息
2. 按照模块依赖关系进行排序
3. 模块中的服务注册

```java
public class ModuleLoader : IModuleLoader
{
    public IAbpModuleDescriptor[] LoadModules(
        IServiceCollection services,
        Type startupModuleType,
        PlugInSourceList plugInSources)
    {
        Check.NotNull(services, nameof(services));
        Check.NotNull(startupModuleType, nameof(startupModuleType));
        Check.NotNull(plugInSources, nameof(plugInSources));

        // 获取模块描述信息
        var modules = GetDescriptors(services, startupModuleType, plugInSources);

        // 按照模块依赖关系进行排序
        modules = SortByDependency(modules, startupModuleType);

        // 模块中的服务注册
        ConfigureServices(modules, services);

        return modules.ToArray();
    }

    // 省略不相关代码...
}
```

我们先来看下第一步，模块加载过程，该步骤主要做了2件事情：

1. 从startupModuleType标记的启动模块开始，递归找到所有模块，为每个模块创建模块描述信息，由`FillModules`方法完成
2. 为每个模块描述信息的依赖模块集合添加所有依赖模块，由`SetDependencies`方法完成

代码如下：
```java
public class ModuleLoader : IModuleLoader
{

    private List<IAbpModuleDescriptor> GetDescriptors(
        IServiceCollection services, 
        Type startupModuleType,
        PlugInSourceList plugInSources)
    {
        var modules = new List<AbpModuleDescriptor>();

        // 1.从startupModuleType标记的启动模块开始，递归找到所有模块，为每个模块创建模块描述信息
        FillModules(modules, services, startupModuleType, plugInSources);
        // 2.为每个模块描述信息的依赖模块集合添加依赖模块
        SetDependencies(modules);

        return modules.Cast<IAbpModuleDescriptor>().ToList();
    }

    protected virtual void FillModules(
        List<AbpModuleDescriptor> modules,
        IServiceCollection services,
        Type startupModuleType,
        PlugInSourceList plugInSources)
    {
        // 这里的AbpModuleHelper.FindAllModuleTypes会根据依赖关系找出所有模块
        foreach (var moduleType in AbpModuleHelper.FindAllModuleTypes(startupModuleType))
        {
            // 创建模块描述信息，模块描述包含：模块类型，所在程序集，依赖的模块集合等信息
            modules.Add(CreateModuleDescriptor(services, moduleType));
        }

        foreach (var moduleType in plugInSources.GetAllModules())
        {
            if (modules.Any(m => m.Type == moduleType))
            {
                continue;
            }

            modules.Add(CreateModuleDescriptor(services, moduleType, isLoadedAsPlugIn: true));
        }
    }

    protected virtual AbpModuleDescriptor CreateModuleDescriptor(IServiceCollection services, Type moduleType, bool isLoadedAsPlugIn = false)
    {
        // 创建模块描述信息
        return new AbpModuleDescriptor(moduleType, CreateAndRegisterModule(services, moduleType), isLoadedAsPlugIn);
    }

    protected virtual IAbpModule CreateAndRegisterModule(IServiceCollection services, Type moduleType)
    {
        // 反射创建模块实例
        var module = (IAbpModule)Activator.CreateInstance(moduleType);
        services.AddSingleton(moduleType, module);
        return module;
    }

    protected virtual void SetDependencies(List<AbpModuleDescriptor> modules)
    {
        foreach (var module in modules)
        {
            // 将依赖模块添加到当前模块的依赖模块集合中
            SetDependencies(modules, module);
        }
    }
    
    protected virtual void SetDependencies(List<AbpModuleDescriptor> modules, AbpModuleDescriptor module)
    {
        foreach (var dependedModuleType in AbpModuleHelper.FindDependedModuleTypes(module.Type))
        {
            var dependedModule = modules.FirstOrDefault(m => m.Type == dependedModuleType);
            if (dependedModule == null)
            {
                throw new AbpException("Could not find a depended module " + dependedModuleType.AssemblyQualifiedName + " for " + module.Type.AssemblyQualifiedName);
            }

            module.AddDependency(dependedModule);
        }
    }

}
```

到这里模块已加载完成，接下来就是按照拓扑顺序，依次调用每个模块的服务配置方法，完成模块下的服务注册。

### 模块系统中的服务注册

前面提到`LoadModules`里加载完模块，就开始执行模块下的服务配置，这个过程会依次遍历所有模块，调用它们的3个生命周期方法（服务配置前，服务配置中，服务配置后），进行服务自定义的服务注册，同时还会进行服务的自动注册。

模块中的服务配置：
```java

protected virtual List<IAbpModuleDescriptor> SortByDependency(List<IAbpModuleDescriptor> modules, Type startupModuleType)
{
    var sortedModules = modules.SortByDependencies(m => m.Dependencies);
    sortedModules.MoveItem(m => m.Type == startupModuleType, modules.Count - 1);
    return sortedModules;
}

protected virtual void ConfigureServices(List<IAbpModuleDescriptor> modules, IServiceCollection services)
{
    var context = new ServiceConfigurationContext(services);
    services.AddSingleton(context);

    foreach (var module in modules)
    {
        if (module.Instance is AbpModule abpModule)
        {
            abpModule.ServiceConfigurationContext = context;
        }
    }

    //PreConfigureServices
    foreach (var module in modules.Where(m => m.Instance is IPreConfigureServices))
    {
        ((IPreConfigureServices)module.Instance).PreConfigureServices(context);
    }

    //ConfigureServices，SkipAutoServiceRegistration是否跳过自动注册
    foreach (var module in modules)
    {
        if (module.Instance is AbpModule abpModule)
        {
            if (!abpModule.SkipAutoServiceRegistration)
            {
                // 进行服务自动注册
                services.AddAssembly(module.Type.Assembly);
            }
        }

        module.Instance.ConfigureServices(context);
    }

    //PostConfigureServices
    foreach (var module in modules.Where(m => m.Instance is IPostConfigureServices))
    {
        ((IPostConfigureServices)module.Instance).PostConfigureServices(context);
    }

    foreach (var module in modules)
    {
        if (module.Instance is AbpModule abpModule)
        {
            abpModule.ServiceConfigurationContext = null;
        }
    }
}

```

### 模块系统中的模块生命周期

Abp目前模拟了4个模块生命周期对象，分别是`OnPreApplicationInitializationModuleLifecycleContributor`、`OnApplicationInitializationModuleLifecycleContributor`、`OnPostApplicationInitializationModuleLifecycleContributor`、`OnApplicationShutdownModuleLifecycleContributor`，可以理解为应用初始化之前，应用初始化之中，应用初始化之后，以及应用关闭4个生命周期对象，每个生命周期对象都有初始化和关闭2个方法，相应的`AbpModule`则有4个生命周期的方法与之对应， `IOnPreApplicationInitialization`、`IOnApplicationInitialization`、`IOnPostApplicationInitialization`、`IOnApplicationShutdown`。在注册完服务以后，开始初始化应用时，会按照模块依赖关系，依次对每个模块执行执行应用初始前方法，应用初始化之中方法，和应用初始化后方法。而在应用关闭的时候，则会按模块依赖关系的逆序，执行关闭方法。

4个生命周期对象的注册在时`AbpApplicationBase`的构造函数中调用`AddCoreAbpServices`时注册的，如下：
```java

internal static class InternalServiceCollectionExtensions
{
    internal static void AddCoreAbpServices(this IServiceCollection services,
        IAbpApplication abpApplication, 
        AbpApplicationCreationOptions applicationCreationOptions)
    {
        // 省略不相关代码...

        // 注册模块的4个生命周期对象
        services.Configure<AbpModuleLifecycleOptions>(options =>
        {
            options.Contributors.Add<OnPreApplicationInitializationModuleLifecycleContributor>();
            options.Contributors.Add<OnApplicationInitializationModuleLifecycleContributor>();
            options.Contributors.Add<OnPostApplicationInitializationModuleLifecycleContributor>();
            options.Contributors.Add<OnApplicationShutdownModuleLifecycleContributor>();
        });
    }
}

```

4个生命周期对象的实现：

```java
public class OnApplicationInitializationModuleLifecycleContributor : ModuleLifecycleContributorBase
{
    public override void Initialize(ApplicationInitializationContext context, IAbpModule module)
    {
        (module as IOnApplicationInitialization)?.OnApplicationInitialization(context);
    }
}

public class OnApplicationShutdownModuleLifecycleContributor : ModuleLifecycleContributorBase
{
    public override void Shutdown(ApplicationShutdownContext context, IAbpModule module)
    {
        (module as IOnApplicationShutdown)?.OnApplicationShutdown(context);
    }
}

public class OnPreApplicationInitializationModuleLifecycleContributor : ModuleLifecycleContributorBase
{
    public override void Initialize(ApplicationInitializationContext context, IAbpModule module)
    {
        (module as IOnPreApplicationInitialization)?.OnPreApplicationInitialization(context);
    }
}

public class OnPostApplicationInitializationModuleLifecycleContributor : ModuleLifecycleContributorBase
{
    public override void Initialize(ApplicationInitializationContext context, IAbpModule module)
    {
        (module as IOnPostApplicationInitialization)?.OnPostApplicationInitialization(context);
    }
}
```

而4个生命周期对象的实际执行则是在模块初始化方法`ModuleManager.InitializeModules()`方法当中，如下代码，可以看到模块初始化时，会按注册顺序依次对所有模块执行4个生命周期方对象的`Initialize`方法，只有3个应用初始化生命周期对象实现了`Initialize`，3个`Initialize`则分别对应的调用了`AbpModule`的`OnPreApplicationInitialization`、`OnApplicationInitialization`、`OnPostApplicationInitialization`生命周期方法。

```java
public class ModuleManager : IModuleManager, ISingletonDependency
{
    private readonly IModuleContainer _moduleContainer;
    private readonly IEnumerable<IModuleLifecycleContributor> _lifecycleContributors;
    private readonly ILogger<ModuleManager> _logger;

    public ModuleManager(
        IModuleContainer moduleContainer,
        ILogger<ModuleManager> logger,
        IOptions<AbpModuleLifecycleOptions> options,
        IServiceProvider serviceProvider)
    {
        // moduleContainer包含所有模块,在AbpApplicationBase构造函数中构造
        _moduleContainer = moduleContainer;
        _logger = logger;

        _lifecycleContributors = options.Value
            .Contributors
            .Select(serviceProvider.GetRequiredService)
            .Cast<IModuleLifecycleContributor>()
            .ToArray();
    }

    public void InitializeModules(ApplicationInitializationContext context)
    {
        // 省略不相关代码...
        // 按注册顺序依次对所有模块执行4个生命周期方法
        foreach (var Contributor in _lifecycleContributors)
        {
            foreach (var module in _moduleContainer.Modules)
            {
                Contributor.Initialize(context, module.Instance);
            }
        }

        _logger.LogInformation("Initialized all ABP modules.");
    }

    public void ShutdownModules(ApplicationShutdownContext context)
    {
        // 而在应用关闭的时候，则会按模块依赖关系的逆序，执行关闭方法
        var modules = _moduleContainer.Modules.Reverse().ToList();

        foreach (var Contributor in _lifecycleContributors)
        {
            foreach (var module in modules)
            {
                Contributor.Shutdown(context, module.Instance);
            }
        }
    }
}

```

查看Abp实现的所有模块，3个初始化的生命周期方法中只有`OnApplicationInitialization`用的最多，其他2个应用初始化的生命周期方法目前几乎没有地方用，目前感觉3个应用初始化的方法使用上没有体现出很大的差异，可能更多出于设计上的完整性的考虑。

## 依赖注入增强


### 服务自动注册

手动注册服务接口与实现是一件相当繁琐的事情，Abp也提供了一套机制帮忙进行服务的自动注册。前面提到的模块中的服务注册时就提到，会对每个模块调用`services.AddAssembly(module.Type.Assembly)`进行自动注册，下面来看看具体如何实现自动注册。

```java
public static class ServiceCollectionConventionalRegistrationExtensions
{

     public static IServiceCollection AddAssembly(this IServiceCollection services, Assembly assembly)
    {
        // 获取所有的注册规则，下面有一个默认的注册规则 DefaultConventionalRegistrar
        foreach (var registrar in services.GetConventionalRegistrars())
        {
            registrar.AddAssembly(services, assembly);
        }

        return services;
    }

    internal static List<IConventionalRegistrar> GetConventionalRegistrars(this IServiceCollection services)
    {
        return GetOrCreateRegistrarList(services);
    }

    private static ConventionalRegistrarList GetOrCreateRegistrarList(IServiceCollection services)
    {
        // 这里又采用对象访问器模式，这里设置了默认的注册策略实现
        var conventionalRegistrars = services.GetSingletonInstanceOrNull<IObjectAccessor<ConventionalRegistrarList>>()?.Value;
        if (conventionalRegistrars == null)
        {
            conventionalRegistrars = new ConventionalRegistrarList { new DefaultConventionalRegistrar() };
            services.AddObjectAccessor(conventionalRegistrars);
        }

        return conventionalRegistrars;
    }

    // 省略不相关代码...
}
```

默认的服务自动注册策略实现`DefaultConventionalRegistrar`，这里的默认服务注册实现就是Abp官方文档中的介绍的几种注册服务的方式：

1. 标记接口注册`ITransientDependency`,`IScopedDependency`,`ISingletonDependency`，标记接口就分别代表服务所具有的生命周期，瞬时、范围、单例
2. `ExposeServicesAttribute`特性标记注册，通过`ServiceLifetime`枚举属性标记服务生命周期。优先以该标记上的Lifetimes属性表示的生命周期为准，没有则检查有没有实现前面的3个标记接口

```java
public class DefaultConventionalRegistrar : ConventionalRegistrarBase
{
    public override void AddType(IServiceCollection services, Type type)
    {
        if (IsConventionalRegistrationDisabled(type))
        {
            // 检查类型是否可自动注册
            // 基类中的实现，type.IsDefined(typeof(DisableConventionalRegistrationAttribute), true);
            return;
        }

        // 获取DependencyAttribute特性标记，从其中取得服务的生命周期方法，可以看到先取DependencyAttribute，如果没有该标记，则看有没有实现标记接口
        var dependencyAttribute = GetDependencyAttributeOrNull(type);
        var lifeTime = GetLifeTimeOrNull(type, dependencyAttribute);

        if (lifeTime == null)
        {
            return;
        }
        // 获取暴露的类型，一个服务可能实现多个接口，可以暴露的那些服务通过ExposeServicesAttribute特性标记进行设置
        var serviceTypes = ExposedServiceExplorer.GetExposedServices(type);

        TriggerServiceExposing(services, type, serviceTypes);
        // 根据ExposeServicesAttribute的属性，转换为实际的服务注册动作
        foreach (var serviceType in serviceTypes)
        {
            // 获取服务描述
            var serviceDescriptor = ServiceDescriptor.Describe(serviceType, type, lifeTime.Value);

            // 如果设置了替换，则替换已有的服务注册
            if (dependencyAttribute?.ReplaceServices == true)
            {
                services.Replace(serviceDescriptor);
            }
            else if (dependencyAttribute?.TryRegister == true)
            {
                // 尝试注册服务，如果有则不注册
                services.TryAdd(serviceDescriptor);
            }
            else
            {
                // 不管有没有都进行注册
                services.Add(serviceDescriptor);
            }
        }
    }
    
    protected virtual DependencyAttribute GetDependencyAttributeOrNull(Type type)
    {
        return type.GetCustomAttribute<DependencyAttribute>(true);
    }

    protected virtual ServiceLifetime? GetLifeTimeOrNull(Type type, [CanBeNull] DependencyAttribute dependencyAttribute)
    {
        // 先检查类型上有没有DependencyAttribute，没有则检查有没有实现标记接口
        return dependencyAttribute?.Lifetime ?? GetServiceLifetimeFromClassHierarcy(type);
    }

    protected virtual ServiceLifetime? GetServiceLifetimeFromClassHierarcy(Type type)
    {
        if (typeof(ITransientDependency).GetTypeInfo().IsAssignableFrom(type))
        {
            return ServiceLifetime.Transient;
        }

        if (typeof(ISingletonDependency).GetTypeInfo().IsAssignableFrom(type))
        {
            return ServiceLifetime.Singleton;
        }

        if (typeof(IScopedDependency).GetTypeInfo().IsAssignableFrom(type))
        {
            return ServiceLifetime.Scoped;
        }

        return null;
    }
}
```

## AOP增强

Abp对AOP的方法拦截做了一层抽象，其主要目的除了对AOP做一层抽象，不用关注背后的实现，更主要的是让大部分AOP方法拦截的使用更加简单。

先看下Abp中实现AOP方法拦截的几个步骤：

1. 继承`AbpInterceptor`实现自己的拦截器
2. 通过`IServiceCollection.OnRegistred`扩展方法注册拦截器（拦截器拦截哪些类）

### AOP拦截示例

下面看Abp针对`AbpInterceptor`单测的一段示例，通过该示例可以有一个直观的使用印象：

首先是需要被拦截的类

```java
public class SimpleInterceptionTargetClass : ICanLogOnObject
{
    public List<string> Logs { get; } = new List<string>();

    public virtual void DoIt()
    {
        Logs.Add("ExecutingDoIt");
    }

    public virtual int GetValue()
    {
        Logs.Add("ExecutingGetValue");
        return 42;
    }

    public virtual async Task<int> GetValueAsync()
    {
        Logs.Add("EnterGetValueAsync");
        await Task.Delay(5).ConfigureAwait(false);
        Logs.Add("MiddleGetValueAsync");
        await Task.Delay(5).ConfigureAwait(false);
        Logs.Add("ExitGetValueAsync");
        return 42;
    }

    public virtual async Task DoItAsync()
    {
        Logs.Add("EnterDoItAsync");
        await Task.Delay(5).ConfigureAwait(false);
        Logs.Add("MiddleDoItAsync");
        await Task.Delay(5).ConfigureAwait(false);
        Logs.Add("ExitDoItAsync");
    }
}

```

拦截器定义：

```java

public class SimpleAsyncInterceptor : AbpInterceptor
{
    public override async Task InterceptAsync(IAbpMethodInvocation invocation)
    {
        // 被拦截方法执行前
        await Task.Delay(5).ConfigureAwait(false);
        (invocation.TargetObject as ICanLogOnObject)?.Logs?.Add($"{GetType().Name}_InterceptAsync_BeforeInvocation");

        // 执行被拦截方法
        await invocation.ProceedAsync().ConfigureAwait(false);
        
        // 被拦截方法执行后
        (invocation.TargetObject as ICanLogOnObject)?.Logs?.Add($"{GetType().Name}_InterceptAsync_AfterInvocation");
        await Task.Delay(5).ConfigureAwait(false);
    }
}

public class SimpleAsyncInterceptor2 : SimpleAsyncInterceptor
{
    
}

```

注册拦截器：

```java
protected override void BeforeAddApplication(IServiceCollection services)
{
    // 首先不管是被拦截的类，或是拦截器类，自身还都是需要注册到IOC容器中
    services.AddTransient<SimpleAsyncInterceptor>();
    services.AddTransient<SimpleAsyncInterceptor2>();
    services.AddTransient<SimpleInterceptionTargetClass>();

    services.AddTransient<SimpleResultCacheTestInterceptor>();
    services.AddTransient<CachedTestObject>();

    // 配置拦截器，具体的拦截器要应用到哪些被拦截的类上
    services.OnRegistred(registration =>
    {
        //比如这里的 实现类型为SimpleInterceptionTargetClass的，添加2个拦截器
        if (typeof(SimpleInterceptionTargetClass) == registration.ImplementationType)
        {
            registration.Interceptors.Add<SimpleAsyncInterceptor>();
            registration.Interceptors.Add<SimpleAsyncInterceptor2>();
        }
        // 实现类型为CachedTestObject，添加一个缓存结果的拦截器
        if (typeof(CachedTestObject) == registration.ImplementationType)
        {
            registration.Interceptors.Add<SimpleResultCacheTestInterceptor>();
        }
    });
}
```

实际的执行结果:

```java
[Fact]
public async Task Should_Intercept_Async_Method_Without_Return_Value()
{
    //Arrange

    var target = ServiceProvider.GetService<SimpleInterceptionTargetClass>();

    //Act

    await target.DoItAsync().ConfigureAwait(false);

    //Assert

    target.Logs.Count.ShouldBe(7);
    target.Logs[0].ShouldBe("SimpleAsyncInterceptor_InterceptAsync_BeforeInvocation");
    target.Logs[1].ShouldBe("SimpleAsyncInterceptor2_InterceptAsync_BeforeInvocation");
    target.Logs[2].ShouldBe("EnterDoItAsync");
    target.Logs[3].ShouldBe("MiddleDoItAsync");
    target.Logs[4].ShouldBe("ExitDoItAsync");
    target.Logs[5].ShouldBe("SimpleAsyncInterceptor2_InterceptAsync_AfterInvocation");
    target.Logs[6].ShouldBe("SimpleAsyncInterceptor_InterceptAsync_AfterInvocation");
}
```
可以看到上面的测试断言结果，在执行`DoItAsync`方法之前，依次按注册的拦截器的顺序执行了拦截器`InterceptAsync`方法。

这是一个最简单的方法拦截的使用场景，对被拦截类的所有方法执行了相同的拦截，如果仅仅是对某些类的某些方法执行拦截了，其实也是可以的，比如在注册拦截器时，可以通过判断是否实现了特定的接口来对特定的实现类拦截，判断方法上是否有自定义的特性标记来拦截特定的方法。也可以按一些类名，方法名约定进行拦截。

下面看Abp中的一个审计拦截器的注册示例：

```java
public static class AuditingInterceptorRegistrar
{
    public static void RegisterIfNeeded(IOnServiceRegistredContext context)
    {
        if (ShouldIntercept(context.ImplementationType))
        {
            context.Interceptors.TryAdd<AuditingInterceptor>();
        }
    }

    private static bool ShouldIntercept(Type type)
    {
        // 如果是在类型上打了AuditedAttribute标记，或者实现了IAuditingEnabled接口，类下的所有方法进行拦截
        if (ShouldAuditTypeByDefault(type))
        {
            return true;
        }
        // 如果仅在方法上打了AuditedAttribute标记，则仅对方法拦截
        if (type.GetMethods().Any(m => m.IsDefined(typeof(AuditedAttribute), true)))
        {
            return true;
        }

        return false;
    }

    //TODO: Move to a better place
    public static bool ShouldAuditTypeByDefault(Type type)
    {
        if (type.IsDefined(typeof(AuditedAttribute), true))
        {
            return true;
        }

        if (type.IsDefined(typeof(DisableAuditingAttribute), true))
        {
            return false;
        }

        if (typeof(IAuditingEnabled).IsAssignableFrom(type))
        {
            return true;
        }

        return false;
    }
}
```

### Abp中的方法拦截实现

Abp中的IOC容器和AOP的内部实现都是基于Autofac，Autofac中的AOP实现则是用的Castle DynamicProxy组件，但是由于在AOP的实现上，Abp做了一套自己的抽象，最后需要将Abp的抽象适配成Autofac的AOP实现，因此在Autofac的注册上，Abp其实是拿了Autofac的源码直接改了AOP注册的部分。虽然改动点不多，但是这种直接改用到的第三方库源码的方式还是值得商榷。

从注册Autofac的入口开始看起, Abp程序一起启动`Program.Main`中创建`IHostBuilder`时调用了Autofac的注册，如下：

```java
internal static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();
            })
            // 注册autofac组件
            .UseAutofac()
            .UseSerilog();
```

`IHostBuilder`的扩展方法`UseAutofac`定义如下：

```java
public static class AbpAutofacHostBuilderExtensions
{
    public static IHostBuilder UseAutofac(this IHostBuilder hostBuilder)
    {
        // 创建一个Autofac的容器
        var containerBuilder = new ContainerBuilder();
        // 1.向对象访问器注册这个containerBuilder实例并调用
        // 2.注册AbpAutofacServiceProviderFactory
        return hostBuilder.ConfigureServices((_, services) =>
            {
                services.AddObjectAccessor(containerBuilder);
            })
            .UseServiceProviderFactory(new AbpAutofacServiceProviderFactory(containerBuilder));
    }
}
```

这个`AbpAutofacServiceProviderFactory`和`Autofac`中的`AutofacServiceProviderFactory`代码几乎一致，区别是构造函数参数`ContainerBuilder`的构造方式和时机不一样，Abp中提前构造的，以及采用Abp自己的扩展方法`ContainerBuilder.Populate`。

```java
public class AbpAutofacServiceProviderFactory : IServiceProviderFactory<ContainerBuilder>
{
    private readonly ContainerBuilder _builder;
    private IServiceCollection _services;

    public AbpAutofacServiceProviderFactory(ContainerBuilder builder)
    {
        _builder = builder;
    }

    public ContainerBuilder CreateBuilder(IServiceCollection services)
    {
        _services = services;
        // 这里Populate使用的是Abp改写的扩展方法，而不是Autofac中原始的Populate方法
        _builder.Populate(services);

        return _builder;
    }

    public IServiceProvider CreateServiceProvider(ContainerBuilder containerBuilder)
    {
        Check.NotNull(containerBuilder, nameof(containerBuilder));

        return new AutofacServiceProvider(containerBuilder.Build());
    }
}
```

在来看下这个`ContainerBuilder.Populate`扩展方法实现，这个方法几乎照搬了Autofac中的实现。唯一的区别是除了将所有注册到`IServiceCollection`中的服务重新注册到Autofac容器中外，还增加了一个扩展方法`IRegistrationBuilder.ConfigureAbpConventions`来对服务增加一些额外的配置，比如开启Autofac的属性注入，还有将Abp抽象的拦截器配置，转换成Autofac的拦截器配置。

```java
/// <summary>
/// Extension methods for registering ASP.NET Core dependencies with Autofac.
/// </summary>
public static class AutofacRegistration
{
    public static void Populate(
            this ContainerBuilder builder,
            IServiceCollection services)
    {
        builder.RegisterType<AutofacServiceProvider>().As<IServiceProvider>();
        builder.RegisterType<AutofacServiceScopeFactory>().As<IServiceScopeFactory>();

        Register(builder, services);
    }

    private static void Register(
            ContainerBuilder builder,
            IServiceCollection services)
    {
        var moduleContainer = services.GetSingletonInstance<IModuleContainer>();
        var registrationActionList = services.GetRegistrationActionList();
        // 遍历所有注册的服务，注册到Autofac容器中
        foreach (var service in services)
        {
            if (service.ImplementationType != null)
            {
                // 重点是这里的 ConfigureAbpConventions，里面会开启属性注入和配置拦截器
                var serviceTypeInfo = service.ServiceType.GetTypeInfo();
                if (serviceTypeInfo.IsGenericTypeDefinition)
                {
                    builder
                        .RegisterGeneric(service.ImplementationType)
                        .As(service.ServiceType)
                        .ConfigureLifecycle(service.Lifetime)
                        .ConfigureAbpConventions(moduleContainer, registrationActionList);
                }
                else
                {
                    builder
                        .RegisterType(service.ImplementationType)
                        .As(service.ServiceType)
                        .ConfigureLifecycle(service.Lifetime)
                        .ConfigureAbpConventions(moduleContainer, registrationActionList);
                }
            }
            else if (service.ImplementationFactory != null)
            {
                var registration = RegistrationBuilder.ForDelegate(service.ServiceType, (context, parameters) =>
                {
                    var serviceProvider = context.Resolve<IServiceProvider>();
                    return service.ImplementationFactory(serviceProvider);
                })
                .ConfigureLifecycle(service.Lifetime)
                .CreateRegistration();
                //TODO: ConfigureAbpConventions ?

                builder.RegisterComponent(registration);
            }
            else
            {
                builder
                    .RegisterInstance(service.ImplementationInstance)
                    .As(service.ServiceType)
                    .ConfigureLifecycle(service.Lifetime);
            }
        }
    }

    // 将dotnet core的ServiceLifetime转换成Autofac中对应的生命周期
    private static IRegistrationBuilder<object, TActivatorData, TRegistrationStyle> ConfigureLifecycle<TActivatorData, TRegistrationStyle>(
            this IRegistrationBuilder<object, TActivatorData, TRegistrationStyle> registrationBuilder,
            ServiceLifetime lifecycleKind)
    {
        switch (lifecycleKind)
        {
            case ServiceLifetime.Singleton:
                registrationBuilder.SingleInstance();
                break;
            case ServiceLifetime.Scoped:
                registrationBuilder.InstancePerLifetimeScope();
                break;
            case ServiceLifetime.Transient:
                registrationBuilder.InstancePerDependency();
                break;
        }

        return registrationBuilder;
    }
}
```

`ConfigureAbpConventions`方法的实现如下，主要干了2件事情：

1. 开启Autofac的属性注入
2. 将Abp的拦截器配置转换成Autofac的拦截器配置

```java
public static class AbpRegistrationBuilderExtensions
{
    public static IRegistrationBuilder<TLimit, TActivatorData, TRegistrationStyle> ConfigureAbpConventions<TLimit, TActivatorData, TRegistrationStyle>(
            this IRegistrationBuilder<TLimit, TActivatorData, TRegistrationStyle> registrationBuilder, 
            IModuleContainer moduleContainer, 
            ServiceRegistrationActionList registrationActionList)
        where TActivatorData : ReflectionActivatorData
    {
        // 省略不相关代码...

        // 1. 开启Autofac的属性注入
        registrationBuilder = registrationBuilder.EnablePropertyInjection(moduleContainer, implementationType);

        // 2. 将Abp的拦截器配置转换成Autofac的拦截器配置
        registrationBuilder = registrationBuilder.InvokeRegistrationActions(registrationActionList, serviceType, implementationType);

        return registrationBuilder;
    }

    private static IRegistrationBuilder<TLimit, TActivatorData, TRegistrationStyle> InvokeRegistrationActions<TLimit, TActivatorData, TRegistrationStyle>(this IRegistrationBuilder<TLimit, TActivatorData, TRegistrationStyle> registrationBuilder, ServiceRegistrationActionList registrationActionList, Type serviceType, Type implementationType) 
        where TActivatorData : ReflectionActivatorData
    {
        var serviceRegistredArgs = new OnServiceRegistredContext(serviceType, implementationType);
        // 这里就是前面单测示例中services.OnRegistred中所有的用来注册的Action集合
        foreach (var registrationAction in registrationActionList)
        {
            // 执行委托Action后，会把拦截器的信息写到OnServiceRegistredContext.Interceptors
            registrationAction.Invoke(serviceRegistredArgs);
        }

        if (serviceRegistredArgs.Interceptors.Any())
        {
            // 如果有拦截器定义，就执行Autofac的拦截器注册
            registrationBuilder = registrationBuilder.AddInterceptors(
                serviceType,
                serviceRegistredArgs.Interceptors
            );
        }

        return registrationBuilder;
    }

    private static IRegistrationBuilder<TLimit, TActivatorData, TRegistrationStyle> EnablePropertyInjection<TLimit, TActivatorData, TRegistrationStyle>(
            this IRegistrationBuilder<TLimit, TActivatorData, TRegistrationStyle> registrationBuilder, 
            IModuleContainer moduleContainer,
            Type implementationType) 
        where TActivatorData : ReflectionActivatorData
    {
        //Enable Property Injection only for types in an assembly containing an AbpModule
        // 拦截器的所在的程序包含AbpModule，就开启属性注入
        if (moduleContainer.Modules.Any(m => m.Assembly == implementationType.Assembly))
        {
            registrationBuilder = registrationBuilder.PropertiesAutowired();
        }

        return registrationBuilder;
    }

    private static IRegistrationBuilder<TLimit, TActivatorData, TRegistrationStyle> AddInterceptors<TLimit, TActivatorData, TRegistrationStyle>(
        this IRegistrationBuilder<TLimit, TActivatorData, TRegistrationStyle> registrationBuilder, 
        Type serviceType,
        IEnumerable<Type> interceptors)
        where TActivatorData : ReflectionActivatorData
    {
        if (serviceType.IsInterface)
        {
            registrationBuilder = registrationBuilder.EnableInterfaceInterceptors();
        }
        else
        {
            (registrationBuilder as IRegistrationBuilder<TLimit, ConcreteReflectionActivatorData, TRegistrationStyle>)?.EnableClassInterceptors();
        }

        foreach (var interceptor in interceptors)
        {
            // 注册Autofac的拦截器
            registrationBuilder.InterceptedBy(typeof(AbpAsyncDeterminationInterceptor<>).MakeGenericType(interceptor));
        }

        return registrationBuilder;
    }
}
```

看到上面的注册Autofac拦截器的代码，可能还是会疑惑，这里注册的`AbpAsyncDeterminationInterceptor`是个什么东西？这玩意可以理解为一个通用的异步拦截器实现。可以理解成通过它去执行我们Abp的适配器，当然也不是直接执行，仍然需要将Abp的拦截器做一次适配。适配成`IAsyncInterceptor`。

```java

public class AbpAsyncDeterminationInterceptor<TInterceptor> : AsyncDeterminationInterceptor
    where TInterceptor : IAbpInterceptor
{
    public AbpAsyncDeterminationInterceptor(TInterceptor abpInterceptor)
        : base(new CastleAsyncAbpInterceptorAdapter<TInterceptor>(abpInterceptor))
    {

    }
}
```

将Abp的拦截器适配成Castle DynamicProxy的异步拦截器

```java
public class CastleAsyncAbpInterceptorAdapter<TInterceptor> : AsyncInterceptorBase
    where TInterceptor : IAbpInterceptor
{
    private readonly TInterceptor _abpInterceptor;

    public CastleAsyncAbpInterceptorAdapter(TInterceptor abpInterceptor)
    {
        _abpInterceptor = abpInterceptor;
    }

    protected override async Task InterceptAsync(IInvocation invocation, IInvocationProceedInfo proceedInfo, Func<IInvocation, IInvocationProceedInfo, Task> proceed)
    {
        await _abpInterceptor.InterceptAsync(
            new CastleAbpMethodInvocationAdapter(invocation, proceedInfo, proceed)
        ).ConfigureAwait(false);
    }

    protected override async Task<TResult> InterceptAsync<TResult>(IInvocation invocation, IInvocationProceedInfo proceedInfo, Func<IInvocation, IInvocationProceedInfo, Task<TResult>> proceed)
    {
        var adapter = new CastleAbpMethodInvocationAdapterWithReturnValue<TResult>(invocation, proceedInfo, proceed);

        await _abpInterceptor.InterceptAsync(
            adapter
        ).ConfigureAwait(false);

        return (TResult)adapter.ReturnValue;
    }
}
```

通过上面的代码分析，可以得出一个结论，Abp仅使用了Castle DynamicProxy的一个通用的异步拦截器，使用它的目的也仅仅只是为了在方法调用时做一层代理，最终会代理给Abp的拦截器实现，再由Abp的拦截器来处理方法执行前，执行后的扩展。

由于dotnet core仅对IOC定了一套抽象标准，并未对AOP做出一套抽象标准，因此像Abp这类应用框架在选用AOP组件时，为了避免依赖具体的AOP组件实现，可能倾向于自己又做一层抽象，来解耦屏蔽背后的AOP组件实现。另一方面这种抽象可能在各个应用框架下都有自己的一套，并不通用。


## 其他

模块系统，拦截器是构成Abp vNext框架的基础，其中它的工作单元应用`(UnitOfWorkInterceptor)`、自动审计功能`(AuditingInterceptor)`、验证模块`(ValidationInterceptor)`、认证模块`(AuthorizationInterceptor)`、动态WEB API，动态WEB API客户端`(DynamicHttpProxyInterceptor)`都是依赖于它的拦截器来实现的。

> **参考**
> 1. Castle 异步拦截器： https://github.com/JSkimming/Castle.Core.AsyncInterceptor
> 2. Autofac 类型拦截： https://autofaccn.readthedocs.io/zh/latest/advanced/interceptors.html
