# 1. Abp_vNext后台作业和后台工作者
<!-- TOC -->

- [AbpvNext后台作业和后台工作者](#abpvnext后台作业和后台工作者)
    - [后台作业（BackgroudJob）](#后台作业backgroudjob)
        - [后台作业运行示例](#后台作业运行示例)
        - [后台作业运行流程](#后台作业运行流程)
            - [发起异步调用过程实现](#发起异步调用过程实现)
            - [调度后台作业的执行过程](#调度后台作业的执行过程)
    - [后台工作者(BackgroudWorker)](#后台工作者backgroudworker)
    - [将Abp的后台作业适配成Hangfire的作业](#将abp的后台作业适配成hangfire的作业)
    - [Abp中Hangfire Server的启动配置](#abp中hangfire-server的启动配置)

<!-- /TOC -->
Abp提供的`后台作业（BackgroudJob）`和`后台工作者(BackgroudWorker)`这两个概念初次接触很容易傻傻分不清。类比一下大多数的作业调度系统都将`作业调度`和`作业执行`进行了区分。`作业调度`通常会指明作业的执行时间，是否周期性执行，分布式环境下在那台机器执行，并行度的问题等等。而`作业执行`则通常指接收调度请求，执行作业的过程，作业的执行通常是异步的，后台执行的。

`后台作业（BackgroudJob）`：实际就是对异步执行过程的统一封装，这一过程就是接受到请求，分配一个后台线程执行的业务。如果仅仅是单机环境，一个后台线程就搞定了，如果需要扩展到多机分布式环境，就可能是任意作业机器上的任意一个线程，同时要保证最基本的，一次后台作业任务请求，只能有唯一一个线程来执行。

`后台工作者(BackgroudWorker)`：在Abp中它其实扮演了对作业调度执行的角色。Abp中默认的后台工作者包含一个`AbpTimer`定时器，可以实现简单的定时调度策略，如果需要更负责的调度策略，例如周期性调度，则需要结合Hangfire这类专门的作业调度系统来完成。

## 1.1. 后台作业（BackgroudJob）

### 1.1.1. 后台作业运行示例

先来看看Abp中的一个后台作业的调用示例：

1. 定义后台作业

```java
// 1. 首先定义一个后台作业参数 
public class MyJobArgs
{
    public string Value { get; set; }

    public MyJobArgs()
    {
    }

    public MyJobArgs(string value)
    {
        Value = value;
    }
}
// 2. 定义一个后台作业跑的业务， 后台作业需要继承 BackgroundJob
public class MyJob : BackgroundJob<MyJobArgs>, ISingletonDependency
{
    public List<string> ExecutedValues { get; } = new List<string>();

    public override void Execute(MyJobArgs args)
    {
        // 这里实现后台运行的业务逻辑
        ExecutedValues.Add(args.Value);
    }
}

```

2. 发起异步调用

```java
  // 通过 IBackgroundJobManager.EnqueueAsync 发起后台异步调用
 _backgroundJobManager.EnqueueAsync(new MyJobArgs("42"))
```

到这里，发起异步调用就完了，可以看到整个后台作业的定义和执行后台作业的过程都非常简单，`IBackgroundJobManager.EnqueueAsync`的参数仅仅是一个普通的类型，又怎么将这次异步调用和`MyJob`这个需要后台执行的业务关联起来，又是如何最终执行到`MyJob.Execute`中的业务逻辑了？

### 1.1.2. 后台作业运行流程

#### 1.1.2.1. 发起异步调用过程实现

首先来看下这个发起异步调用的方法的实现，也就`IBackgroundJobManager.EnqueueAsync`的实现，Abp默认提供了一个`DefaultBackgroundJobManager`的实现，除了默认的实现还提供了基于hangfire的实现`HangfireBackgroundJobManager`。

先来看下这个基于`DefaultBackgroundJobManager`的实现：

```java
[Dependency(ReplaceServices = true)]
public class DefaultBackgroundJobManager : IBackgroundJobManager, ITransientDependency
{
    protected IClock Clock { get; }
    protected IBackgroundJobSerializer Serializer { get; }
    protected IGuidGenerator GuidGenerator { get; }
    protected IBackgroundJobStore Store { get; }
    
    public DefaultBackgroundJobManager(
        IClock clock,
        IBackgroundJobSerializer serializer,
        IBackgroundJobStore store,
        IGuidGenerator guidGenerator)
    {
        Clock = clock;
        Serializer = serializer;
        GuidGenerator = guidGenerator;
        Store = store;
    }

    public virtual async Task<string> EnqueueAsync<TArgs>(TArgs args, BackgroundJobPriority priority = BackgroundJobPriority.Normal, TimeSpan? delay = null)
    {
        // 以参数类名作为作业名称来对不同的作业进行区分
        var jobName = BackgroundJobNameAttribute.GetName<TArgs>();
        var jobId = await EnqueueAsync(jobName, args, priority, delay).ConfigureAwait(false);
        return jobId.ToString();
    }

    protected virtual async Task<Guid> EnqueueAsync(string jobName, object args, BackgroundJobPriority priority = BackgroundJobPriority.Normal, TimeSpan? delay = null)
    {
        // 创建一个后台作业执行信息，存起来，默认存在内存中
        var jobInfo = new BackgroundJobInfo
        {
            Id = GuidGenerator.Create(),
            JobName = jobName,
            JobArgs = Serializer.Serialize(args),
            Priority = priority,
            CreationTime = Clock.Now,
            NextTryTime = Clock.Now
        };

        if (delay.HasValue)
        {
            jobInfo.NextTryTime = Clock.Now.Add(delay.Value);
        }
        // IBackgroundJobStore 默认采用内存保存
        await Store.InsertAsync(jobInfo).ConfigureAwait(false);

        return jobInfo.Id;
    }
}
```

可以看到发起异步调用后，仅仅只是构造了一个后台作业执行信息`BackgroundJobInfo`，然后保存到`IBackgroundJobStore`表示的存储中，到这里发起异步调用就完成了，这里才算完成了异步任务的产生。后面一定还有一个地方在负责异步任务的执行。

#### 1.1.2.2. 调度后台作业的执行过程

可以猜测一下，异步任务的最终执行，一定有地方从JobStore中获取任务信息，然后才能执行异步任务。看看这个`IBackgroundJobStore`接口定义，有一个`GetWaitingJobsAsync`方法，搜索源码发现`BackgroundJobWorker`在使用它，这里正是获取注册的异步任务信息，然后执行异步任务逻辑所在。

```java
public class BackgroundJobWorker : AsyncPeriodicBackgroundWorkerBase, IBackgroundJobWorker
    {
        protected AbpBackgroundJobOptions JobOptions { get; }

        protected AbpBackgroundJobWorkerOptions WorkerOptions { get; }

        public BackgroundJobWorker(
            AbpTimer timer,
            IOptions<AbpBackgroundJobOptions> jobOptions,
            IOptions<AbpBackgroundJobWorkerOptions> workerOptions,
            IServiceScopeFactory serviceScopeFactory)
            : base(
                timer,
                serviceScopeFactory)
        {
            WorkerOptions = workerOptions.Value;
            JobOptions = jobOptions.Value;
            Timer.Period = WorkerOptions.JobPollPeriod;
        }

        protected override async Task DoWorkAsync(PeriodicBackgroundWorkerContext workerContext)
        {

            // 1、首先获取 IBackgroundJobStore 实例，从中获取所有等待执行的作业
            var store = workerContext.ServiceProvider.GetRequiredService<IBackgroundJobStore>();
            var waitingJobs = await store.GetWaitingJobsAsync(WorkerOptions.MaxJobFetchCount).ConfigureAwait(false);

            if (!waitingJobs.Any())
            {
                return;
            }
            // 2、获取后台任务执行器
            var jobExecuter = workerContext.ServiceProvider.GetRequiredService<IBackgroundJobExecuter>();
            var clock = workerContext.ServiceProvider.GetRequiredService<IClock>();
            var serializer = workerContext.ServiceProvider.GetRequiredService<IBackgroundJobSerializer>();

            foreach (var jobInfo in waitingJobs)
            {
                jobInfo.TryCount++;
                jobInfo.LastTryTime = clock.Now;

                try
                {
                    // 3、循环所有接收到的任务，构造一个执行上下文
                    var jobConfiguration = JobOptions.GetJob(jobInfo.JobName);
                    var jobArgs = serializer.Deserialize(jobInfo.JobArgs, jobConfiguration.ArgsType);
                    var context = new JobExecutionContext(workerContext.ServiceProvider, jobConfiguration.JobType, jobArgs);

                    try
                    {
                        // 使用执行器执行这个后台作业，成功则删除任务
                        await jobExecuter.ExecuteAsync(context).ConfigureAwait(false);

                        await store.DeleteAsync(jobInfo.Id).ConfigureAwait(false);
                    }
                    catch (BackgroundJobExecutionException)
                    {
                        // 如果执行失败了，就计算下一次的执行时间（重试时间会随着次数的增加越来越长，直到到达超时时间后标记丢弃），然后更新作业信息
                        var nextTryTime = CalculateNextTryTime(jobInfo, clock);

                        if (nextTryTime.HasValue)
                        {
                            jobInfo.NextTryTime = nextTryTime.Value;
                        }
                        else
                        {
                            jobInfo.IsAbandoned = true;
                        }

                        await TryUpdateAsync(store, jobInfo).ConfigureAwait(false);
                    }
                }
                catch (Exception ex)
                {
                    // 如果构造作业执行上下文都失败了，就把任务标记丢弃状态，记录日志
                    Logger.LogException(ex);
                    jobInfo.IsAbandoned = true;
                    await TryUpdateAsync(store, jobInfo).ConfigureAwait(false);
                }
            }
        }

        protected virtual async Task TryUpdateAsync(IBackgroundJobStore store, BackgroundJobInfo jobInfo)
        {
            try
            {
                await store.UpdateAsync(jobInfo).ConfigureAwait(false);
            }
            catch (Exception updateEx)
            {
                Logger.LogException(updateEx);
            }
        }

        protected virtual DateTime? CalculateNextTryTime(BackgroundJobInfo jobInfo, IClock clock)
        {
            var nextWaitDuration = WorkerOptions.DefaultFirstWaitDuration * (Math.Pow(WorkerOptions.DefaultWaitFactor, jobInfo.TryCount - 1));
            var nextTryDate = jobInfo.LastTryTime?.AddSeconds(nextWaitDuration) ??
                              clock.Now.AddSeconds(nextWaitDuration);

            if (nextTryDate.Subtract(jobInfo.CreationTime).TotalSeconds > WorkerOptions.DefaultTimeout)
            {
                return null;
            }

            return nextTryDate;
        }
    }
```

再来看看这个基类`AsyncPeriodicBackgroundWorkerBase`是个啥，可以看到这个基类又继承了`BackgroundWorkerBase`,这就引出了我们之前提到`后台工作者(BackgroudWorker)`的概念。一个简单调度程序，每隔上几秒就执行一次上面的`DoWorkAsync`方法，这样就不断的从`JobStore`中获取接收到的后台作业信息，不断执行前面接收到的后台作业。

## 1.2. 后台工作者(BackgroudWorker)

这里我们先来看看这个`AsyncPeriodicBackgroundWorkerBase`后台工作者的实现

```java
public abstract class AsyncPeriodicBackgroundWorkerBase : BackgroundWorkerBase
{
    protected IServiceScopeFactory ServiceScopeFactory { get; }
    protected AbpTimer Timer { get; }

    protected AsyncPeriodicBackgroundWorkerBase(
        AbpTimer timer,
        IServiceScopeFactory serviceScopeFactory)
    {
        ServiceScopeFactory = serviceScopeFactory;
        Timer = timer;
        // 注册定时器满足条件触发时的处理函数
        Timer.Elapsed += Timer_Elapsed;
    }

    public override async Task StartAsync(CancellationToken cancellationToken = default)
    {
        // 后台工作者启动时，同时启动了一个定时器
        await base.StartAsync(cancellationToken).ConfigureAwait(false);
        Timer.Start(cancellationToken);
    }

    public override async Task StopAsync(CancellationToken cancellationToken = default)
    {
        Timer.Stop(cancellationToken);
        // 后台工作者结束时，关闭定时器
        await base.StopAsync(cancellationToken).ConfigureAwait(false);
    }

    private void Timer_Elapsed(object sender, System.EventArgs e)
    {
        try
        {
            // 定时器每隔一段时间就会触发Elapsed事件，这里就会去不断调用 DoWorkAsync抽象方法，也就是前面提到的不断消费接收到后台作业
            using (var scope = ServiceScopeFactory.CreateScope())
            {
                AsyncHelper.RunSync(
                    () => DoWorkAsync(new PeriodicBackgroundWorkerContext(scope.ServiceProvider))
                );
            }
        }
        catch (Exception ex)
        {
            Logger.LogException(ex);
        }
    }

    protected abstract Task DoWorkAsync(PeriodicBackgroundWorkerContext workerContext);
}
```

最后我们继续顺藤摸瓜，再来看看这个后台工作者又是如何触发执行的了，又是在哪里注册的了？我们继续搜索Abp的源码，检查这个`IBackgroundJobWorker`有哪些地方引用了它。可以看到注册它的地方在`AbpBackgroundJobsModule`中,代码如下：

```java
[DependsOn(
    typeof(AbpBackgroundJobsAbstractionsModule),
    typeof(AbpBackgroundWorkersModule),
    typeof(AbpTimingModule),
    typeof(AbpGuidsModule)
    )]
public class AbpBackgroundJobsModule : AbpModule
{
    public override void OnApplicationInitialization(ApplicationInitializationContext context)
    {
        // 先获取完`IBackgroundJobWorker`的示例后，马上注册进了`IBackgroundWorkerManager`的实现当中
        var options = context.ServiceProvider.GetRequiredService<IOptions<AbpBackgroundJobOptions>>().Value;
        if (options.IsJobExecutionEnabled)
        {
            context.ServiceProvider
                .GetRequiredService<IBackgroundWorkerManager>()
                .Add(
                    context.ServiceProvider
                        .GetRequiredService<IBackgroundJobWorker>()
                );
        }
    }
}
```

可以看到，先获取完`IBackgroundJobWorker`的示例后，马上注册进了`IBackgroundWorkerManager`的实现当中，这个`BackgroundWorkerManager`除了注册所有的`IBackgroundJobWorker`,还负责了启动和关闭这些后台工作者。

```java
public class BackgroundWorkerManager : IBackgroundWorkerManager, ISingletonDependency, IDisposable
{
    protected bool IsRunning { get; private set; }

    private bool _isDisposed;

    private readonly List<IBackgroundWorker> _backgroundWorkers;
 
    public BackgroundWorkerManager()
    {
        _backgroundWorkers = new List<IBackgroundWorker>();
    }

    public virtual void Add(IBackgroundWorker worker)
    {
        // 注册工作者
        _backgroundWorkers.Add(worker);

        // 添加的时候如果后台工作者管理器是启动的，则立即触发一次当前注册的工作者的执行
        if (IsRunning)
        {
            AsyncHelper.RunSync(
                () => worker.StartAsync()
            );
        }
    }

    public virtual async Task StartAsync(CancellationToken cancellationToken = default)
    {
        IsRunning = true;
        // 循环启动所有的后台工作者
        foreach (var worker in _backgroundWorkers)
        {
            await worker.StartAsync(cancellationToken).ConfigureAwait(false);
        }
    }

    public virtual async Task StopAsync(CancellationToken cancellationToken = default)
    {
        IsRunning = false;
        // 循环关闭所有后台工作者
        foreach (var worker in _backgroundWorkers)
        {
            await worker.StopAsync(cancellationToken).ConfigureAwait(false);
        }
    }
}
```

最后在`AbpBackgroundWorkersModule`的应用初始化方法中会开启后台工作者，应用关闭时，则会关闭后台工作者。

```java
[DependsOn(
    typeof(AbpThreadingModule)
    )]
public class AbpBackgroundWorkersModule : AbpModule
{
    public override void OnApplicationInitialization(ApplicationInitializationContext context)
    {
        var options = context.ServiceProvider.GetRequiredService<IOptions<AbpBackgroundWorkerOptions>>().Value;
        if (options.IsEnabled)
        {
            AsyncHelper.RunSync(
                () => context.ServiceProvider
                    .GetRequiredService<IBackgroundWorkerManager>()
                    .StartAsync()
            );
        }
    }

    public override void OnApplicationShutdown(ApplicationShutdownContext context)
    {
        var options = context.ServiceProvider.GetRequiredService<IOptions<AbpBackgroundWorkerOptions>>().Value;
        if (options.IsEnabled)
        {
            AsyncHelper.RunSync(
                () => context.ServiceProvider
                    .GetRequiredService<IBackgroundWorkerManager>()
                    .StopAsync()
            );
        }
    }
}
```

至此可以看到在Abp中由后台工作者管理着后台作业的，可以实现简单的后台异步，以及周期性的后台调度。但是并没有提供更加常见的基于Crontab的调度方式，更专业的作业调度交给更专业的组件`hangfire`。那如何利用这个Hangfire的调度能哪里了？

## 1.3. 将Abp的后台作业适配成Hangfire的作业

前面提到在发起异步调用时，除了通过默认的实现`DefaultBackgroundJobManager`来发后台作业任务，还提供了基于hangfire的实现`HangfireBackgroundJobManager`，它会将Abp的后台作业适配成hangfire的后台作业，后台作业的调度执行则完全由hangfire来保障执行，代码如下：

```java
[Dependency(ReplaceServices = true)]
public class HangfireBackgroundJobManager : IBackgroundJobManager, ITransientDependency
{
    public Task<string> EnqueueAsync<TArgs>(TArgs args, BackgroundJobPriority priority = BackgroundJobPriority.Normal,
        TimeSpan? delay = null)
    {
        // 将接受的后台作业请求适配成hangfire的请求，下面的BackgroundJob是hangfire中的类
        if (!delay.HasValue)
        {
            // 立即执行的后台作业
            return Task.FromResult(
                BackgroundJob.Enqueue<HangfireJobExecutionAdapter<TArgs>>(
                    adapter => adapter.Execute(args)
                )
            );
        }
        else
        {
            // 延期执行的后台作业
            return Task.FromResult(
                BackgroundJob.Schedule<HangfireJobExecutionAdapter<TArgs>>(
                    adapter => adapter.Execute(args),
                    delay.Value
                )
            );
        }
    }
}
```

由于Abp对于作业调度的抽象本身就没有基于Crontab这样的调度策略，因此要实现基于Crontab来调度Abp的后台作业，除了前面的通过`HangfireJobExecutionAdapter`将后台作业适配成hangfire作业，还得利用Hangfire的基于Crontab的调度API来注册作业。

```java
[DependsOn(
    typeof(AbpBackgroundWorkersModule),
    typeof(AbpBackgroundJobsHangfireModule),
    typeof(ProcessEngineApplicationModule),
    typeof(ProcessEngineInfrastructureModule)
)]
public class ProcessEngineScheduleModule : AbpModule
{
    public override void PreConfigureServices(ServiceConfigurationContext context)
    {
        // 首先需要注册JOB，在适配器HangfireJobExecutionAdapter中会从该注册表中获取作业信息
        Configure<AbpBackgroundJobOptions>(option =>
        {
            option.IsJobExecutionEnabled = jobConf.IsJobExecutionEnabled;
            option.AddJob<EtlReportJob>();
        });
    }

    public override void OnPostApplicationInitialization(ApplicationInitializationContext context)
    {
        // 注册Crontab作业
        RecurringJob.AddOrUpdate<HangfireJobExecutionAdapter<EtlReportJobArgs>>(adapter => adapter.Execute(new EtlReportJobArgs() { }), Cron.Daily(1, 1));
    }
}

```

最后再来看看这个`HangfireJobExecutionAdapter`适配器的实现，通过TArgs参数，从前面的`AbpBackgroundJobOptions`注册表中获取到具体的作业，然后直接利用`IBackgroundJobExecuter`来执行作业。

```java
public class HangfireJobExecutionAdapter<TArgs>
{
    protected AbpBackgroundJobOptions Options { get; }
    protected IServiceScopeFactory ServiceScopeFactory { get; }
    protected IBackgroundJobExecuter JobExecuter { get; }

    public HangfireJobExecutionAdapter(
        IOptions<AbpBackgroundJobOptions> options, 
        IBackgroundJobExecuter jobExecuter, 
        IServiceScopeFactory serviceScopeFactory)
    {
        JobExecuter = jobExecuter;
        ServiceScopeFactory = serviceScopeFactory;
        Options = options.Value;
    }

    public void Execute(TArgs args)
    {
        using (var scope = ServiceScopeFactory.CreateScope())
        {
            // 从前面的AbpModule处注册的AbpBackgroundJobOptions中取得作业类型
            var jobType = Options.GetJob(typeof(TArgs)).JobType;
            
            // 构造作业执行上下文
            var context = new JobExecutionContext(scope.ServiceProvider, jobType, args);

            // 执行作业
            AsyncHelper.RunSync(() => JobExecuter.ExecuteAsync(context));
        }
    }
}
```

## 1.4. Abp中Hangfire Server的启动配置

要使用hangfire做调度，就需要完成与hangfire的集成，Abp提供专门的`AbpHangfireModule`来完成这个集成过程。只要自己的模块依赖了这个`AbpHangfireModule`就会自动启动hangfire服务。

```java
public class AbpHangfireModule : AbpModule
{
    private BackgroundJobServer _backgroundJobServer;

    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        // 配置hangfire的服务
        context.Services.AddHangfire(configuration =>
        {
            context.Services.ExecutePreConfiguredActions(configuration);
        });
    }

    public override void OnApplicationInitialization(ApplicationInitializationContext context)
    {
        // 启动hangfire Server
        var options = context.ServiceProvider.GetRequiredService<IOptions<AbpHangfireOptions>>().Value;
        _backgroundJobServer = options.BackgroundJobServerFactory.Invoke(context.ServiceProvider);
    }

    public override void OnApplicationShutdown(ApplicationShutdownContext context)
    {
        // 应用关闭时，关闭hangfire Server
        _backgroundJobServer.SendStop();
        _backgroundJobServer.Dispose();
    }
}

public class AbpHangfireOptions
{
    [CanBeNull]
    public BackgroundJobServerOptions ServerOptions { get; set; }

    [CanBeNull]
    public IEnumerable<IBackgroundProcess> AdditionalProcesses { get; set; }

    [CanBeNull]
    public JobStorage Storage { get; set; }

    [NotNull]
    public Func<IServiceProvider, BackgroundJobServer> BackgroundJobServerFactory
    {
        get => _backgroundJobServerFactory;
        set => _backgroundJobServerFactory = Check.NotNull(value, nameof(value));
    }
    private Func<IServiceProvider, BackgroundJobServer> _backgroundJobServerFactory;

    public AbpHangfireOptions()
    {
        _backgroundJobServerFactory = CreateJobServer;
    }

    private BackgroundJobServer CreateJobServer(IServiceProvider serviceProvider)
    {
        // 配置hangfire server
        Storage = Storage ?? serviceProvider.GetRequiredService<JobStorage>();
        ServerOptions = ServerOptions ?? serviceProvider.GetService<BackgroundJobServerOptions>() ?? new BackgroundJobServerOptions();
        AdditionalProcesses = AdditionalProcesses ?? serviceProvider.GetServices<IBackgroundProcess>();

        return new BackgroundJobServer(ServerOptions, Storage, AdditionalProcesses,
            ServerOptions.FilterProvider ?? serviceProvider.GetRequiredService<IJobFilterProvider>(),
            ServerOptions.Activator ?? serviceProvider.GetRequiredService<JobActivator>(),
            serviceProvider.GetService<IBackgroundJobFactory>(),
            serviceProvider.GetService<IBackgroundJobPerformer>(),
            serviceProvider.GetService<IBackgroundJobStateChanger>()
        );
    }
}    
```

> **提示**
> 关于hangfire Server根据配置开启问题，可以看到目前只要继承AbpHangfireModule，默认就启动hangfire server，并没有一个地方配置是否开启Server，因此只能重写AbpHangfireModule，增加配置逻辑，然后重写用到它的AbpBackgroundJobsHangfireModule才能实现。