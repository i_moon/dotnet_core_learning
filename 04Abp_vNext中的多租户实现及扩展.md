# 1. Abp_vNext中的多租户实现及扩展
<!-- TOC -->

- [AbpvNext中的多租户实现及扩展](#abpvnext中的多租户实现及扩展)
    - [Abp中的多租户](#abp中的多租户)
    - [多租户实现源码分析](#多租户实现源码分析)
    - [多租户存储实现](#多租户存储实现)

<!-- /TOC -->

## 1.1. Abp中的多租户

abp已实现多数据库模式的多租户，核心实现在`MultiTenancyMiddleware`这个中间件中体现，其职责是拦截每个请求，完成租户信息解析，根据租户信息获取连接字符串，以及维护整个请求期间的租户配置信息。根据租户连接建立实际的租户的DbContext的过程，则是在IDbContextProvider的实现类UnitOfWorkDbContextProvider类中完成。

## 1.2. 多租户实现源码分析

租户信息的解析的整个骨架都是在MultiTenancyMiddleware中完成，可重点看下该中间件的实现，其实也非常简洁。

``` java 

public class MultiTenancyMiddleware : IMiddleware, ITransientDependency
{
    // 租户的解析实现注入
    private readonly ITenantResolver _tenantResolver;
    // 租户存储实现注入
    private readonly ITenantStore _tenantStore;
    // 当前租户上下文
    private readonly ICurrentTenant _currentTenant;
    // 租户解析结果容器，实际解析结果会维护到HttpContext上
    private readonly ITenantResolveResultAccessor _tenantResolveResultAccessor;

    public MultiTenancyMiddleware(
        ITenantResolver tenantResolver, 
        ITenantStore tenantStore, 
        ICurrentTenant currentTenant, 
        ITenantResolveResultAccessor tenantResolveResultAccessor)
    {
        _tenantResolver = tenantResolver;
        _tenantStore = tenantStore;
        _currentTenant = currentTenant;
        _tenantResolveResultAccessor = tenantResolveResultAccessor;
    }

    public async Task InvokeAsync(HttpContext context, RequestDelegate next)
    {
        // 租户ID、名称解析，解析会从cookie,header,ICurrentUser用户上下文等几个地方去解析
        var resolveResult = _tenantResolver.ResolveTenantIdOrName();
        // 租户ID解析，以及用到的解析策略会存在HttpContext上
        _tenantResolveResultAccessor.Result = resolveResult;

        // 根据租户ID、或者租户名，从ITenantStore这个租户信息存储实现中去查询，（存在并没有实现，可以自己实现一个数据库的存储）
        TenantConfiguration tenant = null;
        if (resolveResult.TenantIdOrName != null)
        {
            tenant = await FindTenantAsync(resolveResult.TenantIdOrName);
            if (tenant == null)
            {
                //TODO: A better exception?
                throw new AbpException(
                    "There is no tenant with given tenant id or name: " + resolveResult.TenantIdOrName
                );
            }
        }
        // 将租户ID和名称信息保存到ICurrentTenant 这个租户上下文中
        using (_currentTenant.Change(tenant?.Id, tenant?.Name))
        {
            await next(context);
        }
    }

    private async Task<TenantConfiguration> FindTenantAsync(string tenantIdOrName)
    {
        if (Guid.TryParse(tenantIdOrName, out var parsedTenantId))
        {
            return await _tenantStore.FindAsync(parsedTenantId);
        }
        else
        {
            return await _tenantStore.FindAsync(tenantIdOrName);
        }
    }
}
    
```
接着我们可以重点看下ITenantResolver的实现，看看租户信息到底可以有哪些传递方式？

```java
public class TenantResolver : ITenantResolver, ITransientDependency
{
    private readonly IServiceProvider _serviceProvider;
    // 重点是这个AbpTenantResolveOptions的配置实现，记录所有租户ID解析策略，默认从用户上下文信息中解析。
    private readonly AbpTenantResolveOptions _options;

    public TenantResolver(IOptions<AbpTenantResolveOptions> options, IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
        _options = options.Value;
    }

    public TenantResolveResult ResolveTenantIdOrName()
    {
        var result = new TenantResolveResult();

        using (var serviceScope = _serviceProvider.CreateScope())
        {
            var context = new TenantResolveContext(serviceScope.ServiceProvider);

            //AbpTenantResolveOptions.TenantResolvers属性包含所有注册的解析策略。
            foreach (var tenantResolver in _options.TenantResolvers)
            {
                tenantResolver.Resolve(context);

                result.AppliedResolvers.Add(tenantResolver.Name);
                // 循环所有的解析策略，只要有一个解析出来就退出
                if (context.HasResolvedTenantOrHost())
                {
                    result.TenantIdOrName = context.TenantIdOrName;
                    break;
                }
            }
        }

        return result;
    }
}

```
AbpTenantResolveOptions这个配置类，维护着所有注册的租户ID解析策略。解析策略由ITenantResolveContributor定义。
```java

// ITenantResolveContributor接口的实现就是所有租户解析策略
public class AbpTenantResolveOptions
{
    [NotNull]
    public List<ITenantResolveContributor> TenantResolvers { get; }

    public AbpTenantResolveOptions()
    {
        TenantResolvers = new List<ITenantResolveContributor>
        {
            // 默认是采用这个从CurrentUser上下文上取传入的租户ID参数，
            new CurrentUserTenantResolveContributor()
        };
    }
}
```
ITenantResolveContributor这个租户ID解析策略接口有丰富的实现，以支持丰富的租户ID传递方式，具体有如下7种实现：
1. CurrentUserTenantResolveContributor: 通过用户上下文获取租户ID
2. ActionTenantResolveContributor:自定义解析策略，提供了一个Action代理，可以自己配置实现
3. CookieTenantResolveContributor:从Cookie获取,cookie键为__tenant
4. HeaderTenantResolveContributor: 从Header头获取，header键为__tenant
5. QueryStringTenantResolveContributor: 从查询字符串获取
6. DomainTenantResolveContributor: 从固定的Host模式中获取，比如模式为{tenantId}.xxx.com，实际host为，tenant_a.xxx.com。则可以解析出租户ID为,tenant_a
7. RouteTenantResolveContributor: 从路由规则中解析,利用httpContext.GetRouteValue解析。

前面租户信息解析好了，连接字符串也获取了，接着我们看看如何创建租户的DbContext的过程。核心就在这个IDbContextProvider接口，以及UnitOfWorkDbContextProvider这个实现类中。

```java

public class UnitOfWorkDbContextProvider<TDbContext> : IDbContextProvider<TDbContext>
    where TDbContext : IEfCoreDbContext
{
    private readonly IUnitOfWorkManager _unitOfWorkManager;
    private readonly IConnectionStringResolver _connectionStringResolver;

    public UnitOfWorkDbContextProvider(
        IUnitOfWorkManager unitOfWorkManager,
        IConnectionStringResolver connectionStringResolver)
    {
        _unitOfWorkManager = unitOfWorkManager;
        _connectionStringResolver = connectionStringResolver;
    }

    // 重点就是这个方法来构造DbContext，可以看到
    public TDbContext GetDbContext()
    {
        var unitOfWork = _unitOfWorkManager.Current;
        if (unitOfWork == null)
        {
            throw new AbpException("A DbContext can only be created inside a unit of work!");
        }
        // 之前在EF Core的总结总中，我们分析过这个连接字符串解析，当时分析了默认实现，这里我们主要看下他的多租户连接字符串解析实现。

        // 首先还是先从DbContext上解析出连接字符串名称，一般都叫Default了
        var connectionStringName = ConnectionStringNameAttribute.GetConnStringName<TDbContext>();
        // 根据租户信息解析具体的租户连接字符串信息
        var connectionString = _connectionStringResolver.Resolve(connectionStringName);

        var dbContextKey = $"{typeof(TDbContext).FullName}_{connectionString}";
        // 传入具体的连接字符串，创建DbContext
        var databaseApi = unitOfWork.GetOrAddDatabaseApi(
            dbContextKey,
            () => new EfCoreDatabaseApi<TDbContext>(
                CreateDbContext(unitOfWork, connectionStringName, connectionString)
            ));

        return ((EfCoreDatabaseApi<TDbContext>)databaseApi).DbContext;
    }

    private TDbContext CreateDbContext(IUnitOfWork unitOfWork, string connectionStringName, string connectionString)
    {
        var creationContext = new DbContextCreationContext(connectionStringName, connectionString);
        using (DbContextCreationContext.Use(creationContext))
        {
            var dbContext = CreateDbContext(unitOfWork);

            if (unitOfWork.Options.Timeout.HasValue &&
                dbContext.Database.IsRelational() &&
                !dbContext.Database.GetCommandTimeout().HasValue)
            {
                dbContext.Database.SetCommandTimeout(unitOfWork.Options.Timeout.Value.TotalSeconds.To<int>());
            }

            return dbContext;
        }
    }

    private TDbContext CreateDbContext(IUnitOfWork unitOfWork)
    {
        return unitOfWork.Options.IsTransactional
            ? CreateDbContextWithTransaction(unitOfWork)
            : unitOfWork.ServiceProvider.GetRequiredService<TDbContext>();
    }

    public TDbContext CreateDbContextWithTransaction(IUnitOfWork unitOfWork) 
    {
        var transactionApiKey = $"EntityFrameworkCore_{DbContextCreationContext.Current.ConnectionString}";
        var activeTransaction = unitOfWork.FindTransactionApi(transactionApiKey) as EfCoreTransactionApi;

        if (activeTransaction == null)
        {
            var dbContext = unitOfWork.ServiceProvider.GetRequiredService<TDbContext>();

            var dbtransaction = unitOfWork.Options.IsolationLevel.HasValue
                ? dbContext.Database.BeginTransaction(unitOfWork.Options.IsolationLevel.Value)
                : dbContext.Database.BeginTransaction();

            unitOfWork.AddTransactionApi(
                transactionApiKey,
                new EfCoreTransactionApi(
                    dbtransaction,
                    dbContext
                )
            );

            return dbContext;
        }
        else
        {
            DbContextCreationContext.Current.ExistingConnection = activeTransaction.DbContextTransaction.GetDbTransaction().Connection;

            var dbContext = unitOfWork.ServiceProvider.GetRequiredService<TDbContext>();

            if (dbContext.As<DbContext>().HasRelationalTransactionManager())
            {
                dbContext.Database.UseTransaction(activeTransaction.DbContextTransaction.GetDbTransaction());
            }
            else
            {
                dbContext.Database.BeginTransaction(); //TODO: Why not using the new created transaction?
            }

            activeTransaction.AttendedDbContexts.Add(dbContext);

            return dbContext;
        }
    }
}
```
多租户连接字符串解析实现：MultiTenantConnectionStringResolver

```java

[Dependency(ReplaceServices = true)]
public class MultiTenantConnectionStringResolver : DefaultConnectionStringResolver
{
    private readonly ICurrentTenant _currentTenant;
    private readonly IServiceProvider _serviceProvider;

    public MultiTenantConnectionStringResolver(
        IOptionsSnapshot<AbpDbConnectionOptions> options,
        ICurrentTenant currentTenant,
        IServiceProvider serviceProvider)
        : base(options)
    {
        _currentTenant = currentTenant;
        _serviceProvider = serviceProvider;
    }

    public override string Resolve(string connectionStringName = null)
    {
        //No current tenant, fallback to default logic
        // 如果当前租户上下文没有租户信息，就调用默认的连接字符串解析逻辑
        if (_currentTenant.Id == null)
        {
            return base.Resolve(connectionStringName);
        }
        
        using (var serviceScope = _serviceProvider.CreateScope())
        {
            var tenantStore = serviceScope
                .ServiceProvider
                .GetRequiredService<ITenantStore>();

            // 如果有，则从租户存储实现中取取租户信息，具体存储实现中会实现返回具体租户的连接字符串信息
            var tenant = tenantStore.Find(_currentTenant.Id.Value);
            // ConnectionStrings这个属性是字典，也就是一个租户可以包含多个数据库，都可以注册到这个属性里，比如一个租户有日志库，有报表库，不同的业务有不同的库，因此就需要有一个KV结构来存储
            if (tenant?.ConnectionStrings == null)
            {
                return base.Resolve(connectionStringName);
            }

            //Requesting default connection string
            if (connectionStringName == null)
            {
                return tenant.ConnectionStrings.Default ??
                        Options.ConnectionStrings.Default;
            }

            //Requesting specific connection string
            var connString = tenant.ConnectionStrings.GetOrDefault(connectionStringName);
            if (connString != null)
            {
                return connString;
            }

            /* Requested a specific connection string, but it's not specified for the tenant.
                * - If it's specified in options, use it.
                * - If not, use tenant's default conn string.
                */

            var connStringInOptions = Options.ConnectionStrings.GetOrDefault(connectionStringName);
            if (connStringInOptions != null)
            {
                return connStringInOptions;
            }

            return tenant.ConnectionStrings.Default ??
                    Options.ConnectionStrings.Default;
        }
    }
}

```
以上代码基本就分析完真个多租户相关的核心代码逻辑了，但是Abp并没有提供ITenantStore的存储实现。我们就需要自己去实现一个多租户元数据的存取实现。

## 1.3. 多租户存储实现

通常多租户元数据信息的存储我们可以采用一个专门的数据库，通常我们叫做多租户配置库，存储一些被所有租户所共用的信息。

首先还是需要定义一下我们的租户实体模型,通常会有2个表，一个存具体的数据库实例信息，一个存具体的租户信息，一个数据库实例下可以建多个数据库

```java 

// 数据库实例
[Table("p_instance")]
public class DbInstance
{
    [Column("instance_id")] 
    public int InstanceId { get; set; }

    [MaxLength(255)] 
    [Column("host")] 
    public string Host { get; set; }

    [Column("port")] 
    public int Port { get; set; }

    [MaxLength(255)] 
    [Column("user_name")] 
    public string UserName { get; set; }

    [MaxLength(255)] 
    [Column("password")] 
    public string Password { get; set; }

    [MaxLength(255)]
    [Column("decrypt_auth")]
    public string DecryptAuth { get; set; }

    public virtual IList<TenantInfo> TenantInfos { get; set; }
}

// 租户信息
[Table("p_tenants")]
public class TenantInfo : Entity<Guid>
{
    [Column("instance_id")] 
    public int InstanceId { get; set; }
    
    public virtual DbInstance DbInstance { get; set; }
    
    [Column("tenant_code")]
    [MaxLength(255)]
    public string TenantCode { get; set; }
    
    [Column("tenant_name")]
    [MaxLength(255)]
    public string TenantName { get; set; }
    
    [Column("db_name")]
    [MaxLength(255)]
    public string DbName { get; set; }
}


```

多租户存储实现，这里主要是根据租户ID，或者名称取出数据库信息，租户信息，最后返回一个具体租户数据库的连接信息，下面给了一个简单的实现，实际生产标准还需把这些连接信息缓存起来，防止每个请求都频繁读取配置库。
``` java

[Dependency(TryRegister = true)]
public class DbTenantStore : ITenantStore, ITransientDependency
{
    private IRepository<TenantInfo> tenantInfos;
    private string dbType;

    public DbTenantStore(IConfiguration configuration, IRepository<TenantInfo> tenantInfos)
    {
        this.tenantInfos = tenantInfos;
        dbType = configuration.GetSection("DbType")?.Value;
    }

    public Task<TenantConfiguration> FindAsync(string name)
    {
        return Task.FromResult(this.Find(name));
    }

    public Task<TenantConfiguration> FindAsync(Guid id)
    {
        return Task.FromResult(Find(id));
    }

    private const string SQLServerConnString = "Data Source={0},{1};Initial Catalog={2};Integrated Security=False;User ID={3};Password={4};Connect Timeout=15;Encrypt=False;TrustServerCertificate=False";

    public TenantConfiguration Find(string name)
    {
        throw new NotImplementedException();
    }

    public TenantConfiguration Find(Guid id)
    {
        var tenantInfo = this.tenantInfos.FirstOrDefault(item => item.Id == id);
        if (tenantInfo == null || tenantInfo.DbInstance == null)
        {
            throw new Exception("租户配置信息不存在");
        }

        var host = tenantInfo.DbInstance.Host;
        var port = tenantInfo.DbInstance.Port;
        var dbUser = tenantInfo.DbInstance.UserName;
        var dbPwd = tenantInfo.DbInstance.Password;
        var dbName = tenantInfo.DbName;

        var config = new TenantConfiguration(tenantInfo.Id, tenantInfo.TenantName);
        if (dbType == DbTypeConst.MYSQL)
        {
            // config.ConnectionStrings[ConnectionStrings.DefaultConnectionStringName] = string.Format(SQLServerConnString, host, port, dbName, dbUser, dbPwd);
        }
        else
        {
            config.ConnectionStrings[ConnectionStrings.DefaultConnectionStringName] = string.Format(SQLServerConnString, host, port, dbName, dbUser, dbPwd);
        }

        return config;
    }
}

```
