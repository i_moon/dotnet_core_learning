# ASP.NET CORE 总结
<!-- TOC -->

- [ASP.NET CORE 总结](#aspnet-core-%E6%80%BB%E7%BB%93)
    - [启动过程](#%E5%90%AF%E5%8A%A8%E8%BF%87%E7%A8%8B)
    - [依赖注入](#%E4%BE%9D%E8%B5%96%E6%B3%A8%E5%85%A5)
        - [核心概念](#%E6%A0%B8%E5%BF%83%E6%A6%82%E5%BF%B5)
        - [服务注册](#%E6%9C%8D%E5%8A%A1%E6%B3%A8%E5%86%8C)
        - [服务消费](#%E6%9C%8D%E5%8A%A1%E6%B6%88%E8%B4%B9)
        - [生命周期](#%E7%94%9F%E5%91%BD%E5%91%A8%E6%9C%9F)
        - [服务实例释放](#%E6%9C%8D%E5%8A%A1%E5%AE%9E%E4%BE%8B%E9%87%8A%E6%94%BE)
        - [实际的应用场景](#%E5%AE%9E%E9%99%85%E7%9A%84%E5%BA%94%E7%94%A8%E5%9C%BA%E6%99%AF)
            - [ASP.NET CORE中请求生命周期](#aspnet-core%E4%B8%AD%E8%AF%B7%E6%B1%82%E7%94%9F%E5%91%BD%E5%91%A8%E6%9C%9F)
            - [Abp中UnitOfWork管理数据库资源生命周期](#abp%E4%B8%ADunitofwork%E7%AE%A1%E7%90%86%E6%95%B0%E6%8D%AE%E5%BA%93%E8%B5%84%E6%BA%90%E7%94%9F%E5%91%BD%E5%91%A8%E6%9C%9F)
    - [中间件、管道](#%E4%B8%AD%E9%97%B4%E4%BB%B6%E7%AE%A1%E9%81%93)
    - [过滤器](#%E8%BF%87%E6%BB%A4%E5%99%A8)
        - [过滤器类型及执行顺序](#%E8%BF%87%E6%BB%A4%E5%99%A8%E7%B1%BB%E5%9E%8B%E5%8F%8A%E6%89%A7%E8%A1%8C%E9%A1%BA%E5%BA%8F)
        - [过滤器作用域及同类型过滤器的执行顺序](#%E8%BF%87%E6%BB%A4%E5%99%A8%E4%BD%9C%E7%94%A8%E5%9F%9F%E5%8F%8A%E5%90%8C%E7%B1%BB%E5%9E%8B%E8%BF%87%E6%BB%A4%E5%99%A8%E7%9A%84%E6%89%A7%E8%A1%8C%E9%A1%BA%E5%BA%8F)
        - [过滤器应用-Abp中提供的过滤器](#%E8%BF%87%E6%BB%A4%E5%99%A8%E5%BA%94%E7%94%A8-abp%E4%B8%AD%E6%8F%90%E4%BE%9B%E7%9A%84%E8%BF%87%E6%BB%A4%E5%99%A8)
    - [配置](#%E9%85%8D%E7%BD%AE)
        - [常见的配置项存储数据源](#%E5%B8%B8%E8%A7%81%E7%9A%84%E9%85%8D%E7%BD%AE%E9%A1%B9%E5%AD%98%E5%82%A8%E6%95%B0%E6%8D%AE%E6%BA%90)
        - [多配置信息数据源时的加载顺序](#%E5%A4%9A%E9%85%8D%E7%BD%AE%E4%BF%A1%E6%81%AF%E6%95%B0%E6%8D%AE%E6%BA%90%E6%97%B6%E7%9A%84%E5%8A%A0%E8%BD%BD%E9%A1%BA%E5%BA%8F)
        - [各种数据源常见配置场景](#%E5%90%84%E7%A7%8D%E6%95%B0%E6%8D%AE%E6%BA%90%E5%B8%B8%E8%A7%81%E9%85%8D%E7%BD%AE%E5%9C%BA%E6%99%AF)
        - [配置文件](#%E9%85%8D%E7%BD%AE%E6%96%87%E4%BB%B6)
        - [命令行参数](#%E5%91%BD%E4%BB%A4%E8%A1%8C%E5%8F%82%E6%95%B0)
        - [环境变量](#%E7%8E%AF%E5%A2%83%E5%8F%98%E9%87%8F)
        - [配置读取方式](#%E9%85%8D%E7%BD%AE%E8%AF%BB%E5%8F%96%E6%96%B9%E5%BC%8F)
            - [弱类型读取](#%E5%BC%B1%E7%B1%BB%E5%9E%8B%E8%AF%BB%E5%8F%96)
            - [通过强类型读取](#%E9%80%9A%E8%BF%87%E5%BC%BA%E7%B1%BB%E5%9E%8B%E8%AF%BB%E5%8F%96)
        - [监听配置变化](#%E7%9B%91%E5%90%AC%E9%85%8D%E7%BD%AE%E5%8F%98%E5%8C%96)
        - [自定义配置数据源](#%E8%87%AA%E5%AE%9A%E4%B9%89%E9%85%8D%E7%BD%AE%E6%95%B0%E6%8D%AE%E6%BA%90)
    - [选项模式](#%E9%80%89%E9%A1%B9%E6%A8%A1%E5%BC%8F)
        - [没有选项，注册配置类的方式加载配置](#%E6%B2%A1%E6%9C%89%E9%80%89%E9%A1%B9%E6%B3%A8%E5%86%8C%E9%85%8D%E7%BD%AE%E7%B1%BB%E7%9A%84%E6%96%B9%E5%BC%8F%E5%8A%A0%E8%BD%BD%E9%85%8D%E7%BD%AE)
        - [通过选项模式加载配置](#%E9%80%9A%E8%BF%87%E9%80%89%E9%A1%B9%E6%A8%A1%E5%BC%8F%E5%8A%A0%E8%BD%BD%E9%85%8D%E7%BD%AE)
            - [选项注册](#%E9%80%89%E9%A1%B9%E6%B3%A8%E5%86%8C)
            - [选项值获取](#%E9%80%89%E9%A1%B9%E5%80%BC%E8%8E%B7%E5%8F%96)
            - [选项作用范围](#%E9%80%89%E9%A1%B9%E4%BD%9C%E7%94%A8%E8%8C%83%E5%9B%B4)
        - [选项构造扩展点](#%E9%80%89%E9%A1%B9%E6%9E%84%E9%80%A0%E6%89%A9%E5%B1%95%E7%82%B9)
        - [选项值验证](#%E9%80%89%E9%A1%B9%E5%80%BC%E9%AA%8C%E8%AF%81)

<!-- /TOC -->

## 启动过程

asp.net core应用启动过程会几类事情：

1. 配置服务器（如监听IP端口等）
2. 对应用服务做注册（IOC）
3. 配置请求处理管道（配置中间件）

这些事情可以直接在构造Host的过程中完成，也可以部分在约定的Setup启动类中完成。整个启动过程执行顺序如下：

![Startup_seq.png](resource/core_action/Startup_seq.png)

示例代码如下：

```java

public class Program
{
    public static int Main(string[] args)
    {
        Log.Logger = new LoggerConfiguration()
#if DEBUG
            .MinimumLevel.Debug()
#else
            .MinimumLevel.Information()
#endif
            .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
            .MinimumLevel.Override("Microsoft.EntityFrameworkCore", LogEventLevel.Warning)
            .Enrich.FromLogContext()
            .WriteTo.Async(c => c.File("Logs/logs.txt"))
#if DEBUG
            .WriteTo.Async(c => c.Console())
#endif
            .CreateLogger();

        try
        {
            Log.Information("Starting web host.");
            CreateHostBuilder(args).Build().Run();
            return 0;
        }
        catch (Exception ex)
        {
            Log.Fatal(ex, "Host terminated unexpectedly!");
            return 1;
        }
        finally
        {
            Log.CloseAndFlush();
        }
    }

    internal static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
            {
                // https://github.com/dotnet/aspnetcore/blob/3e9ae8e5eee2930da0096ab4ca4976f5938df648/src/Hosting/Abstractions/src/HostingAbstractionsWebHostBuilderExtensions.cs
                Console.WriteLine("ConfigureWebHostDefaults executing");
                webBuilder.UseStartup<Startup>();
            })
            .ConfigureHostConfiguration(configBuilder =>
            {
              
                Console.WriteLine("ConfigureHostConfiguration executing");
            })
            .ConfigureAppConfiguration(configBuilder =>
            {
                Console.WriteLine("ConfigureAppConfiguration executing");
            })
            .ConfigureServices(serviceCollection =>
            {
                Console.WriteLine("ConfigureServices executing");
            })
            .ConfigureLogging(loggerBuilder =>
            {
                Console.WriteLine("ConfigureLogging executing");
            })
            .UseAutofac()
            .UseSerilog();
}


public class Startup
{
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddApplication<BookStoreWebModule>();
    }

    public void Configure(IApplicationBuilder app, ILoggerFactory loggerFactory)
    {
        app.InitializeApplication();
    }
}
```

参考：
https://github.com/dotnet/runtime
https://github.com/dotnet/aspnetcore/blob/3e9ae8e5eee2930da0096ab4ca4976f5938df648/src/Hosting/Abstractions/src/HostingAbstractionsWebHostBuilderExtensions.cs



## 依赖注入

在软件设计原则是依赖抽象而不依赖具体实现。如何管理所依赖的抽象对象的创建、和销毁过程，就是依赖注入容器所要解决的核心问题。

### 核心概念

.net core之前.net领域有很多的依赖注入项目，NInject，Castle，API设计的各式各样。从 .net core 开始，提供了一套依赖注入最基础的抽象和实现。

依赖注入的3个核心概念，服务注册，服务实例获取，服务生命周期。

1. **IServiceCollection:** 服务注册表，通过它的扩展方法```AddSingleton,AddScoped,AddTransient```注册的服务接口和服务实现都会存储在这个ServiceCollection当中。

2. **ServiceDescriptor:** 服务注册描述信息。服务注册方法最终都会构建ServiceDescriptor来描述服务基本信息

3. **IServiceProvider:** 依赖注入容器。服务实例的提供者，用于获取具体的服务实例。

4. **IServiceScope:** 服务生命周期。一个Scope代表一个生命周期范围，通过它可以实现生命周期的创建，以及生命周期结束时，对资源的释放管理


.net core中使用IOC容器的3步骤：1、注册服务 2、构造容器 3、获取服务实例


### 服务注册

注册服务，实际上就是指定服务的实例化方式，以及服务所具备的生命周期。服务注册的过程实际是往服务注册表IServiceCollection添加这一描述信息的过程。

```java

services.AddSingleton<ISingletonService，MySingletonService>();

services.AddScoped<IScopedService，MyScopedService>();

services.AddTransient<ITransientService，MyTransientService>();

services.AddTransient<ITransientService>(serviceProvider => {
    // 自己控制类型实例化过程，相当于一个工厂，比如根据某个配置来创建不同的服务实例
    return new MyTransientService();
});

// 尝试注册，已经注册，不重新注册
services.TryAddTransient<接口，实现>();

// 替换已注册服务，常用于替换已有服务的实现
services.Replace(new ServiceDescriptor(类型，实例，生命周期));

```

>  
> .net core提供的服务注册API非常的基础，注册服务过程非常繁琐，如何实现服务的自动注册过程了？



### 服务消费

1、ServiceProvider方式获取

```java

var sc = new ServiceCollection();
// 服务信息注册
sc.AddTransient<ITransientTestService, TransientTestService>();
sc.AddTransient<ITransientInnerTestService, TransientInnerTestService>();

// 服务容器（服务提供者）构造
var provider = sc.BuildServiceProvider();

// 获取服务实例
var transientSvc = provider.GetService<ITransientTestService>();
Console.WriteLine(transientSvc.Run());

```

2、构造函数注入获取

```java

public interface ITransientTestService : IDisposable
{
    string Run();
}

public class TransientTestService : ITransientTestService
{
    private ITransientInnerTestService _innerTestService;

    // 构造函数注入
    public TransientTestService(ITransientInnerTestService innerTestService)
    {
        this._innerTestService = innerTestService;
    }

    public string Run()
    {
        return "TransientTestService， 调用构造函数注入内容：" + _innerTestService.Run();
    }

}

```


### 生命周期


生命周期的意义，用于决定在某一作用域范围内，是复用之前的创建的对象实例，还是创建一个新的对象实例。

**Singleton:** 单例作用越，.net core进程内只有一个实例。

**Scoped:**  范围作用域级，作用域内只有一个实例（请求级别的生命周期，就是利用Scoped级别进行模式的，每个请求创建一个Scoped）。

**Transient:** 瞬时作用越，每次从IServiceProvider容器获取对象，容器都会返回一个新的实例。


### 服务实例释放

IServiceProvider对象除了提供服务实例，还负责服务实例的回收释放。

根容器：应用程序启动时维护的一个根容器，和应用具有相同生命周期。所有Singleton的生命周期服务实例都会被注册到根容器上，那些实现了IDisposable的，具有Singleton生命周期的服务实例，都随着根ServiceProvider对象的释放而释放。

子容器：由父容器的IServiceprovider.CreateScope()来创建一个IServiceScope，IServiceScope.ServiceProvider即为子容器，子容器保存着由它创建的Scoped和Transient生命周期的服务实例，当子容器所属的IServiceScope.Dispose()被调用时，会调用子容器IServiceProvider.Dispose()，实现了IDisposable接口Scoped生命期周期对象和Transient生命周期的服务实例也会被释放。由谁创建，由谁释放。

![service_provider.png](resource/core_action/service_provider.png)

服务注册、获取、生命周期示例：

```java
var sc = new ServiceCollection();
// 服务信息注册
sc.AddTransient<ITransientTestService, TransientTestService>();
sc.AddTransient<ITransientInnerTestService, TransientInnerTestService>();
sc.AddTransient<ITransientInnerTest2Service, TransientInnerTest2Service>();
sc.AddScoped<IScopedTestService, ScopedTestService>();
sc.AddSingleton<ISingletonTestService, SingletonTestService>();

// 服务容器（服务提供者）构造
var provider = sc.BuildServiceProvider();

// 获取服务实例
var singletonSvc = provider.GetService<ISingletonTestService>();
Console.WriteLine(singletonSvc.Run());

// 范围作用越
var scopedSvc = provider.GetService<IScopedTestService>();
Console.WriteLine(scopedSvc.Run());

// 瞬时作用域
var transientSvc = provider.GetService<ITransientTestService>();
Console.WriteLine(transientSvc.Run());

// 创建子作用域
var subScoped = provider.CreateScope();

var singleInSubScoped = subScoped.ServiceProvider.GetService<ISingletonTestService>();

Console.WriteLine("单例作用域，在任意作用域中获取的都是唯一实例：" + singletonSvc.Equals(singleInSubScoped));

var scopedSvc1 = subScoped.ServiceProvider.GetService<IScopedTestService>();
var scopedSvc2 = subScoped.ServiceProvider.GetService<IScopedTestService>();
Console.WriteLine("范围作用域，同域内相同：" + scopedSvc1.Equals(scopedSvc2));
Console.WriteLine("范围作用域，跨域不同：" + !scopedSvc1.Equals(scopedSvc));

Console.WriteLine("");

Console.WriteLine("子域subScoped释放");
subScoped.Dispose();

Console.WriteLine("");
Console.WriteLine("根域释放");
provider.Dispose();

```

### 实际的应用场景

#### ASP.NET CORE中请求生命周期

1、ASP.NET CORE启动时会创建一个根级别的ServiceScope。这个Scope代表应用程序的生命周期，从程序启动到程序关闭。

2、同时ASP.NET CORE会为每个请求创建一个IServiceScope范围生命周期，绑定HttpContext，随着HTTP请求的产生，到HTTP请求的结束，标识一个完整的请求过程，管理的请求级的资源创建和释放


![request_lifetime.png](resource/core_action/request_lifetime.png)

#### Abp中UnitOfWork管理数据库资源生命周期

Abp中每个UnitOfWork实例实际上是绑定着数据资源DbContext的生命周期，都由相同的ServiceProvider容器进行创建，因此UnitOfWork释放时，DbContext也就被释放了，也就实现了由工作单元管理DbContext的资源的目的。

大致流程：

1、using var uow = UnitOfWorkManager.Begin()，创建一个IServiceScope，由Scope的ServiceProvider创建UnitOfWork实例，同时UnitOfWork保持了一个创建它的容器ServiceProvider。

![service_life_time_uow.png](resource/core_action/dotnet_core_pic/service_life_time_uow.png)


2、DbContext的创建时由当前UnitOfWork引用的容器ServiceProvider所创建，因此UnitOfWork和DbContext是在同一个ServiceScope下的。

![service_life_time_uow1.png](resource/core_action/dotnet_core_pic/service_life_time_uow1.png)
![service_life_time_uow2.png](resource/core_action/dotnet_core_pic/service_life_time_uow2.png)

3、UnitOfWork释放时，会调用它所在Scope的Dispose()，该Scope一旦释放，就将由它创建的服务实例进行释放。因此最终效果就是当前UnitOfWork释放，则它管理的DbContext释放。
![service_life_time_uow3.png](resource/core_action/dotnet_core_pic/service_life_time_uow3.png)
![service_life_time_uow4.png](resource/core_action/dotnet_core_pic/service_life_time_uow4.png)

> **参考**
> 服务实例的生命周期：https://www.cnblogs.com/artech/p/inside-asp-net-core-03-08.html


## 中间件、管道

职责连模式实现请求处理管道
![pipline_middleware.png](resource/core_action/pipline_middleware.png)

asp.net core mvc中典型的请求处理管道注册顺序示例揭示中间件管道的作用

![mvc_middleware_sample.png](resource/core_action/mvc_middleware_sample.png)

异常处理中间件通常放在最外层，认证授权不通过就会中断后续的中间件的执行，在管道的最后叫做Endpoint的中间件

## 过滤器

中间件是HTTP请求处理级别的扩展机制，而过滤器（也叫MVC过滤器，是MVC框架下的请求处理扩展机制），在中间件管道的末端有一个Endpoint端点中间件，MVC的过滤器就是在该中间件中接着处理。

![mvc_endpoint_middleware.png](resource/core_action/mvc_endpoint_middleware.png)

### 过滤器类型及执行顺序

过滤器是MVC的的一部分，和MVC结合的更加紧密。MVC中定义了几种过滤器类型，如下图中展示的，而且这些不同的过滤器类型之间是有执行的先后顺序的。具体的不同类型的过滤器执行顺序如下图：

![mvc_filter_flow2.png](resource/core_action/mvc_filter_flow2.png)

![mvc_filter_flow1.png](resource/core_action/mvc_filter_flow1.png)


从图中可以看到，有几类过滤器类型，Authorization过滤器，Resource过滤器，Action过滤器，Exception过滤器，Result过滤器，不同的类型的过滤器将MVC请求处理划分成了若干个处理阶段。比如先处理授权，再处理静态资源，在处理模型绑定，在处理Action前，Action中，Action后，如果Action中有异常，直接走Exception异常过滤器，如果没异常走Result过滤器。

### 过滤器作用域及同类型过滤器的执行顺序

前面讲到过滤器是有类型，而且不同类型在不同的时机执行，而相同类型的过滤器也是有优先级执行顺序的。

过滤器的3中作用域：

1. Global作用域 (MvcOptions.Filters中注册)
2. Controller作用域（通过标记的方式打在控制器类上）
3. Action作用域（通过标记的方式打在方法上）

相同类型的过滤器，如果不指定过滤器的排序，则默认的执行顺序如下：

![same_type_filter_exec_order.png](resource/core_action/same_type_filter_exec_order.png)

这个顺序其其实很好理解，请求处理的顺序，一般是全局-》控制器-》方法，因此执行前方法OnActionExecuting都是全局的先执行，而执行后方法OnActionExecuted都是相反。类似堆栈的行为，先进后出，这里已同步Filter为例，异步Filter没有这些方法，但是原理都是想通的

另外相同类型的过滤器之间的执行顺序可以通过排序属性进行指定。如下：
通过指定优先级，action级别的作用域可以排在全局作用域之前。

![same_type_filter_exec_sp_order.png](resource/core_action/same_type_filter_exec_sp_order.png)

我们在扩展新增自己的中间件和过滤器时都需要注意这个顺序，特别是依赖系统提供的过滤器时，需要考虑注册顺序不要弄错。

### 过滤器应用-Abp中提供的过滤器

如下图，通过过滤器实现全局的异常处理，审计日志记录，工作单元开启
![abp_filters.png](resource/core_action/abp_filters.png)


> 参考
> https://www.cnblogs.com/tdfblog/p/filters-in-aspnet-core-mvc.html
> https://www.cnblogs.com/dotNETCoreSG/p/aspnetcore-4_4_3-filters.html

## 配置

.net core基于key-value键值对的结构抽象了配置信息。

.net core支持从不同的配置数据源读取配置信息，可以通过扩展实现IConfigurationProvider支持更多的配置数据源。

### 常见的配置项存储数据源

内置的较为常见的配置数据源：

1. 从JSON,XML,INI文件读取 （ConfigurationBuilder.AddJsonFile）
2. 从CommandLine命令行读取 （ConfigurationBuilder.AddCommandLine)
3. 从环境变量读取 （ConfigurationBuilder.AddEnvironmentVariables)
4. 从内存对象读取 （ConfigurationBuilder.AddInMemoryCollection）


### 多配置信息数据源时的加载顺序

配置信息加载的顺序是可以自由调整的，下面是一种典型的配置加载顺序：

```java
 Configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json", optional: true)
                .AddEnvironmentVariables()
                .AddCommandLine(args)
                .Build();
```

上面这段配置加载的优先级为: 命令行 》 环境变量 》 带环境的配置文件 》 不带环境的配置文件。

加载原则是最后配置的，优先级最高，会覆盖前面的同名配置项，上面的示例AddCommand在最后，因此命令行参数的优先级最高

### 各种数据源常见配置场景

各种数据源的配置通常会组合使用，就像上面的示例，各种类型数据源的配置都有各自适合的使用场景。

### 配置文件

从配置文件读取配置信息，是一种最常见的配置存取方案，一般用于作为默认的配置模板。

```json
{
  "DbType": "SQLSERVER",
  "ConnectionStrings": {
    "Default": "Data Source=localhost;Initial Catalog=bpm_engine;Integrated Security=False;User ID=sa;Password=;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False",
  },
  "Serilog": {
    "Using": [ "Serilog.Sinks.MSSqlServer" ],
    "MinimumLevel": {
      "Default": "Warning",
      "Override": {
        "Microsoft": "Information",
        "System": "Warning"
      }
    },
    "WriteTo": [
      {
        "Name": "MSSqlServer",
        "Args": {
          "connectionString": "Data Source=localhost;Initial Catalog=bpm_logs;Integrated Security=False;User ID=sa;Password=;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False",
          //"schemaName": "EventLogging",
          "tableName": "engine_logs",
          "autoCreateSqlTable": true,
          "restrictedToMinimumLevel": "Error",
          "batchPostingLimit": 1000,
          "period": "0.00:00:30",
          "columnOptionsSection": {
          }
        }
      }
    ]
  },
}
```

### 命令行参数

命令行通常是交互式，最接近用户端的，因此一般命令行参数都是最高优先级，来覆盖其他类型数据源的配置项。比如通过配置文件指定了默认的配置选项，在实际执行程序的时候通过命令行参数来改变默认的配置项。

```bash
# 通过命令行参数
sh /app/migrate-entrypoint.sh --__app_tenant_id=bpm_dev --__app_code=bpm
```

命令行传递规则：
1、默认支持集中前缀"--","/"或者无前缀
2、键值用“=”连接，或者空格连接
3、可以采用"-"对参数做别名映射

示例：
```java
var builder = new ConfigurationBuilder();
// 将-CLKey1这个配置项，映射成CommandLineKey1配置项
// -CLKey1=1 等同 CommandLineKey1=1
// 配置命令参数映射
var mapper = new Dictionary<string, string> { { "-CLKey1", "CommandLineKey1" } };
builder.AddCommandLine(args, mapper);

var configurationRoot = builder.Build();
Console.WriteLine($"CommandLineKey1:{configurationRoot["-CLKey1"]}");
Console.WriteLine($"CommandLineKey1:{configurationRoot["CommandLineKey1"]}");

Console.ReadKey();
```

### 环境变量

配置信息采用环境变量注入的方式常用于容器环境。一般容器镜像打的配置文件都只是配置模板信息，具体容器实例运行时一种常见的注入配置信息的方式就是通过环境变量来完成。

```yml
 mybpm-engine:
    image: myhub.fdccloud.com/lczx/mybpm-engine:v1.1.3.1
    environment:
      - TZ=Asia/Shanghai
      # 服务监听的IP
      - ASPNETCORE_URLS=http://0.0.0.0:80
      # 配置引擎使用的数据库为MySQL
      - DbType=MYSQL
      # Migration执行账户
      - ConnectionStrings__MigrateDefault=server=rm-bp1niy92t823g9p27yo.mysql.rds.aliyuncs.com;uid=leader;pwd=ykj@1234@180;database=bpm_yun_engine_inner;SslMode=none;Allow User Variables=True;
      # 流程引擎库连接字符串
      - ConnectionStrings__Default=server=rm-bp1niy92t823g9p27yo.mysql.rds.aliyuncs.com;uid=ykj-im-test;pwd=lTkgD91ZAz3egmwv;database=bpm_yun_engine_inner;SslMode=none;Allow User Variables=True;
      # 日志库连接字符串
      - Serilog__WriteTo__0__Args__connectionString=server=rm-bp1niy92t823g9p27yo.mysql.rds.aliyuncs.com;uid=ykj-im-test;pwd=lTkgD91ZAz3egmwv;database=bpm_yun_longger_inner;SslMode=none;
      # 应用站点API地址
      - BpmApiConf__SitePath=http://mybpm-app
      # 开启消息推送
      - notification__pc__enable=true
      - notification__pc__adapter__AdapterName=Mysoft.ProcessEngine.Message.StandardNotificationService
      - notification__mobile__enable=true
      - notification__mobile__adapter__AdapterName=Mysoft.ProcessEngine.Message.StandardNotificationService
      - JobConf__WorkerCount=1
      - JobConf__StorageType=DB
    depends_on:
      - mybpm-app
```

**环境变量的配置规则**

配置文件通常为了按功能进行区分，通常是会嵌套的，可能是嵌套的子配置对象，也可能是数组结构。

1. 嵌套子对象属性， 通过“__”进行区分

```sh
ConnectionStrings__Default=Data Source=localhost;Initial Catalog=bpm_engine;Integrated Security=False;User ID=sa;Password=;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False
```

等同于下面的配置文件结构

```json
 {
   "ConnectionStrings": {
    "Default": "Data Source=localhost;Initial Catalog=bpm_engine;Integrated Security=False;User ID=sa;Password=;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False",
  }
 }
```

2. 嵌套子数组，通过索引进行区分

```sh
 Serilog__WriteTo__0__Args__connectionString=Data Source=localhost;Initial Catalog=bpm_logs;Integrated Security=False;User ID=sa;Password=;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False
```

等同于下面的配置文件结构

```json
{
  "Serilog": {
    "WriteTo": [
      {
        "Name": "MSSqlServer",
        "Args": {
          "connectionString": "Data Source=localhost;Initial Catalog=bpm_logs;Integrated Security=False;User ID=sa;Password=;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False",
        }
      },
       {
        "Name": "Elasticsearch",
        "Args": {}
      }
    ]
  },
}
```

### 配置读取方式

#### 弱类型读取

通过`IConfiguration`进行读取。典型的键的组合方式，还是嵌套子对象，和嵌套子对象数组。通过":"分割分层嵌套的键，用索引读取数组键。

配置示例：

```json
"CustomerSection": {
    "SubKey1": "value1",
    "SubKey2": [
      {
        "ArrayKey1": "akValue1"
      },
      {
        "ArrayKey2": "akValue2"
      }
    ]
  }
```


1. 嵌套子对象读取：

``` java
// 注入IConfigration实例

Configuration.GetValue<string>("CustomerSection:SubKey1");
```

2. 嵌套子对象数组读取：

```java
// 注入IConfigration实例

Configuration.GetValue<string>("CustomerSection:SubKey2:0:ArrayKey1")
```

#### 通过强类型读取

.net core还提供了强类型的读取方式，更加方便我们读取配置信息

1. 配置类定义

```java
// 将前面的配置文件，定义成对象
public class CustomerSection
{
    public string SubKey1 { get; set; }

    public IList<ArrayItem> SubKey2 { get; set; }
}

public class ArrayItem
{
    public string ArrayKey1 { set; get; }
}
```

2. 配置读取

```java
// 注入IConfigration实例，然后通过Configuration.Bind绑定所需要读取的配置段
var confObj = new CustomerSection();
Configuration.GetSection("CustomerSection").Bind(confObj, binderOptions => { binderOptions.BindNonPublicProperties = true; });
confObj.SubKey2[0].ArrayKey1;
 
```

### 监听配置变化

订阅ChangeToken.OnChange事件，监听配置变化，可以做一些自己的调整

```java
// 先注入IConfiguration实例Configuration, 订阅ChangeToken.OnChange事件
ChangeToken.OnChange(() => Configuration.GetReloadToken(), () =>
{
    Console.WriteLine($"Key1:{Configuration["Key1"]}");
});
```

### 自定义配置数据源

1. 继承`Microsoft.Extensions.Configuration.ConfigurationProvider`实现自己的配置数据源
2. 实现`Microsoft.Extensions.Configuration.IConfigurationSource`构造自定义数据源实例
3. 注册自定配置数据源`IConfigurationBuilder.Add(IConfigurationSource source)`


## 选项模式

前面通过强类型的配置类，已经将配置的读取过程做到了简化，但配置绑定、读取的过程仍然出现在了我们的业务代码之中。
当然，我们将配置读取后绑定到强类型的配置类，然后将配置类实例直接注入到IOC容器中，在使用配置的地方通过依赖注入的方式来获取我们的配置对象。

这一过程耦合了配置读取，配置注册，以及在注册时就明确了配置的生命周期，在使用时没法改变。更常见的使用配置的场景，可能是使用配置时，明确生命周期。

选项模式的出现一方面是解耦了配置读取的这一过程，另一方面实现使用配置服务的用户来确定配置实例的作用范围，是全局保持一份，还是请求内保持一份，新请求可以获取新配置，还是请求内部不同线程都可以读取到最新配置？


### 没有选项，注册配置类的方式加载配置

通过在`.net core`的Startup的配置服务方法ConfigureServices中完成配置加载，配置类注入的过程。

```java
void ConfigureCustomerConfig(IServiceCollection services, IConfiguration configuration)
{
    // 注册为transient，需要每次读取
    // 注册为Scopeed，每次请求读取
    // 注册为Singleton，程序启动时读取
    services.AddTransient<CustomerSection>(serviceProvider =>
    {
        var confObj = new CustomerSection();
        configuration.GetSection("CustomerSection").Bind(confObj, binderOptions => { binderOptions.BindNonPublicProperties = true; });
        return confObj;
    });
}

```

使用配置

``` java
public string ReadCustomerConf([FromServices] CustomerSection confObj)
{
    return confObj.SubKey2[0].ArrayKey1;
}
```

### 通过选项模式加载配置

通过选项模式加载配置，其实不用关心前面的经配置类注册成什么作用域，什么时候需要重新加载这个动作其实包含到了选项里面去实现了。

#### 选项注册

通过```IServiceCollection.Configure```扩展方法注册选项

```java
void ConfigureOptions(IServiceCollection services, IConfiguration configuration)
{
    // 首先还需要获取响应的配置段，将配置段绑定到对应的选项类上
    services.Configure<CustomerSection>(configuration.GetSection("CustomerSection"));
}
```

#### 选项值获取

通过```IOptionsMonitor```获取到选项值，该接口实现的服务会接收来自配置系统的变更通知，重新生成Option选项对象。

```java
public string ReadCustomerConf([FromServices] IOptionsMonitor<CustomerSection> options)
{
    return options.CurrentValue.SubKey2[0].ArrayKey1;
}
```

#### 选项作用范围

前面完成选项的注册、值的获取，那选项的生命周期，或者说作用范围如何控制？

```java
// 先注入IConfiguration实例Configuration, 订阅ChangeToken.OnChange事件

var serviceCollection = new ServiceCollection();

serviceCollection.Configure<SerilogOption>(Configuration.GetSection(SerilogOption.SerilogSectionName);

serviceProvider = serviceCollection.BuildServiceProvider();

serilogOption = serviceProvider.GetService<IOptions<SerilogOption>>().Value;
Console.WriteLine($"Serilog:Using:0->{serilogOption.Using.First()}");
    
ChangeToken.OnChange(() => Configuration.GetReloadToken(), (sc) =>
{
    serilogOption = serviceProvider.GetService<IOptions<SerilogOption>>().Value;
    Console.WriteLine($"值发生变化后，通过IOptions 从IOC容器中获取：Serilog:Using:0->{serilogOption.TestAttr}"); 
    
    // 配置变更时可读取到最新值，可以用于任何生命周期的服务中，全局的不同线程间共享
    serilogOption = serviceProvider.GetService<IOptionsMonitor<SerilogOption>>().CurrentValue;
    Console.WriteLine($"值发生变化后，通过IOptionsMonitor 从IOC容器中获取：Serilog:Using:0->{serilogOption.TestAttr}"); 
    
    // 配置变更时可读取到最新值，用于Scoped生命周期，请求级别，请求类不变，请求之间会刷新
    serilogOption = serviceProvider.GetService<IOptionsSnapshot<SerilogOption>>().Value;
    Console.WriteLine($"值发生变化后，通过IOptionsSnapshot 从IOC容器中获取：Serilog:Using:0->{serilogOption.TestAttr}"); 
    
}, serviceProvider);

```



### 选项构造扩展点

通过`IServiceCollection.PostConfigure`扩展方法，可配置选项数据最终返回前的处理动作。比如某些配置是做过加密的，但是我们实际使用时需要解密后的内容，就可以利用该方法注册解密动作的逻辑。

```java
void ConfigureOptions(IServiceCollection services, IConfiguration configuration)
{
    // 首先还需要获取响应的配置段，将配置段绑定到对应的选项类上
    services.Configure<CustomerSection>(configuration.GetSection("CustomerSection"));
  
    services.PostConfigure<CustomerSection>(customerConf => {
      customerConf.SubKey1 = "将加密配置文件解密";
    });
}
```

### 选项值验证

1. 通过OptionsBuilder.Validate方法验证

```java

services.AddOptions<CustomerSection>().Configure(options =>
{
    configuration.GetSection("CustomerSection").Bind(options);
})
.Validate(o =>
{
    return o.SubKey2[0].ArrayKey1 == "hello";
}, "配置值不正确");

```

2. 通过特性配置验证

通过`DataAnnotations`特性进行验证。

```java
services.AddOptions<CustomerSection>().Configure(options =>
{
    configuration.GetSection("CustomerSection").Bind(options);
})
.ValidateDataAnnotations();
```

3. 通过接口自定义验证


```java
services.AddOptions<CustomerSection>().Configure(options => { configuration.GetSection("CustomerSection").Bind(options); })
//.Validate(o => o.SubKey2[0].ArrayKey1 == "hello", "配置值不正确")
//.ValidateDataAnnotations()
.Services.AddSingleton<IValidateOptions<CustomerSection>, OrderServiceValidateOptions>();
```

``` java
public class OrderServiceValidateOptions : IValidateOptions<CustomerSection>
{
    public ValidateOptionsResult Validate(string name, CustomerSection options)
    {
        if (options.SubKey1 != "hello")
        {
            return ValidateOptionsResult.Fail("SubKey1 值异常");
        }
        else
        {
            return ValidateOptionsResult.Success;
        }
    }
}
```
