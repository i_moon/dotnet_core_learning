# 1. Abp_vNext动态API客户端
<!-- TOC -->

- [AbpvNext动态API客户端](#abpvnext动态api客户端)
    - [动态API客户端简介、实现原理](#动态api客户端简介实现原理)
    - [Abp中动态API客户端实现分析](#abp中动态api客户端实现分析)
        - [定义、注册远端服务代理访问接口](#定义注册远端服务代理访问接口)
            - [使用Polly组件配置进行远程调用的HttpClient的容错策略](#使用polly组件配置进行远程调用的httpclient的容错策略)
            - [为代理接口注册一个动态代理实现](#为代理接口注册一个动态代理实现)
        - [通过AOP拦截器实现远端接口代理访问实现](#通过aop拦截器实现远端接口代理访问实现)
        - [如何查找远端API描述信息](#如何查找远端api描述信息)
    - [其他](#其他)
        - [动态代理访问中的接口认证实现](#动态代理访问中的接口认证实现)

<!-- /TOC -->

## 1.1. 动态API客户端简介、实现原理

.net中通常我们访问远程WEB API服务，需要通过HttpClient来完成，要完成一次远端API访问，我们不仅需要考虑对方接口描述（接口地址，访问路径，请求方法，请求头，参数，返回值，以及相参数、返回值的序列化），接口安全认证，还需要考虑调用失败的容错处理，比如重试，调用异常记录，更进一步还有熔断，降级，以及调用链路追踪等等问题。

另一方面为了方便远端服务的调用，通常我们都会在本地定义一个代理访问接口，做一个抽象层，抽象远端提供的服务，接口实现则是封装了对远端服务的代理访问过程。这样我们调用这个远端服务时，就可以像调用本地服务一样。本地使用这个服务时也不用关心这个服务的实现也本地实现，还是由远端的API服务提供。

这个封装远端服务的调用过程其实比较模式化和繁琐，前面提到的服务远程调用问题都需要考虑。所以我们可以想象一下，是否能够只定义远程访问的接口，通过动态代理工具自动生成访问远端服务的代理类？

答案当然是可以的，Abp Vnext就提供了一套WEB API客户端动态代理的实现，大致实现原理如下：

1. 定义远端服务访问的本地代理接口
2. 通过远端的`API接口描述服务`来构造具体的请求路径，参数，返回值。
3. 使用动态代理生成远端接口的实现

实现后效果就是，我要访问远端的服务，我定义一个本地代理的接口即可，并标记一下这个接口需要生成动态代理实现。调用服务只需引用这个接口，不用考虑任何前面提供的一次远程调用所要面对的任何问题。

## 1.2. Abp中动态API客户端实现分析

还是以Abp中的示例来做讲解，看看如何一步一步的实现这个动态API客户端。

### 1.2.1. 定义、注册远端服务代理访问接口

远端WEB API提供了一些服务，我们通常先定义这些远端服务在本地使用时的代理访问接口，我们用到哪些服务，不用的也不用定义。

```java
public interface IRegularTestController
{
    Task<int> IncrementValueAsync(int value);
}
```

定义完这个接口，又如何能知道要为这个接口生成动态代理实现类了？这个时候就需要先来进行注册。可以通过`AddHttpClientProxy`来注册单个接口。也可以通过`AddHttpClientProxies`来注册整个代理访问接口的程序集。

```java 
[DependsOn(
    typeof(AbpHttpClientModule),
    typeof(AbpAspNetCoreMvcTestModule)
    )]
public class AbpHttpClientTestModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        // 注册远程调用API的代理访问接口，这里的IRegularTestController就是一个代表远端服务的普通c#接口
        context.Services.AddHttpClientProxies(typeof(TestAppModule).Assembly);
        context.Services.AddHttpClientProxy<IRegularTestController>();

        // 如果本地应用需要调用多个不同的API站点，需要配置这个AbpRemoteServiceOptions
        Configure<AbpRemoteServiceOptions>(options =>
        {
            options.RemoteServices.Default = new RemoteServiceConfiguration("/");
        });
    }
}
```

看看这个`AddHttpClientProxy`扩展方法的实现，通过方法名就可以知道，它是在为我们的这个代理接口添加一个通过HttpClient来访问远端服务的代理实现，它做了2件实现：

1. 使用Polly组件配置进行远程调用的HttpClient的容错策略
2. 为代理接口注册一个动态代理实现

```java
public static IServiceCollection AddHttpClientProxy<T>(
    [NotNull] this IServiceCollection services,
    [NotNull] string remoteServiceConfigurationName = RemoteServiceConfigurationDictionary.DefaultName,
    bool asDefaultService = true,
    Action<IHttpClientBuilder> configureHttpClientBuilder = null)
{
    // 配置HttpClientFactory，以及Polly来处理请求的容错，重试策略
    AddHttpClientFactoryAndPolicy(services, remoteServiceConfigurationName, configureHttpClientBuilder);

    // 为接口生成动态代理实现，并注册到IOC容器
    return services.AddHttpClientProxy(
        typeof(T),
        remoteServiceConfigurationName,
        asDefaultService
    );
}
```

#### 1.2.1.1. 使用Polly组件配置进行远程调用的HttpClient的容错策略

如下源码展示了在调用发生HTTP异常时，进行3次倍增的时间间隔的重试策略：

```java
public static IServiceCollection AddHttpClientFactoryAndPolicy(
    [NotNull] this IServiceCollection services,
    [NotNull] string remoteServiceConfigurationName = RemoteServiceConfigurationDictionary.DefaultName,
    Action<IHttpClientBuilder> configureHttpClientBuilder = null)
{
    var httpClientBuilder = services.AddHttpClient(remoteServiceConfigurationName);
    if (configureHttpClientBuilder == null)
    {
        // 调用Polly组件对Http Error进行重试，重试3次，重试时间2倍递增
        httpClientBuilder.AddTransientHttpErrorPolicy(builder =>
            builder.WaitAndRetryAsync(3, i => TimeSpan.FromSeconds(Math.Pow(2, i))));
    }
    else
    {
        configureHttpClientBuilder.Invoke(httpClientBuilder);
    }

    return services;
}

```
这里并没有3次重试后失败以后的日志记录操作，也是一个可以完善的地方。

#### 1.2.1.2. 为代理接口注册一个动态代理实现

前面定义了代理访问接口，那这个接口的实现怎么自动生成了？这里就用到Castle DynamicProxy这个AOP组件，其中`ProxyGenerator.CreateInterfaceProxyWithoutTarget`方法，就可以为一个空接口做一个动态代理实现类。这里代理类的功能，就是封装了对远程WEB API服务的访问。下面就是注册这个动态代理实现类的过程。

```java
public static IServiceCollection AddHttpClientProxy(
    [NotNull] this IServiceCollection services,
    [NotNull] Type type,
    [NotNull] string remoteServiceConfigurationName = RemoteServiceConfigurationDictionary.DefaultName,
    bool asDefaultService = true)
{
    Check.NotNull(services, nameof(services));
    Check.NotNull(type, nameof(type));
    Check.NotNullOrWhiteSpace(remoteServiceConfigurationName, nameof(remoteServiceConfigurationName));

    // 为每个远端服务接口配置采用的服务配置KEY（不同的服务接口可能在不同的接口站点下）
    services.Configure<AbpHttpClientOptions>(options =>
    {
        options.HttpClientProxies[type] = new DynamicHttpClientProxyConfig(type, remoteServiceConfigurationName);
    });
    // 为传入的接口类型type, 生成一个泛型的Abp的拦截器泛型类型，比如上面的示例就是DynamicHttpProxyInterceptor<IRegularTestController>类型
    var interceptorType = typeof(DynamicHttpProxyInterceptor<>).MakeGenericType(type);
    // 注册拦截器类型
    services.AddTransient(interceptorType);

    // 将ABP的拦截器适配成Castle的拦截器类型，AbpAsyncDeterminationInterceptor<DynamicHttpProxyInterceptor<IRegularTestController>>
    var interceptorAdapterType = typeof(AbpAsyncDeterminationInterceptor<>).MakeGenericType(interceptorType);
    // 将Abp的服务验证拦截器适配成Castle的拦截器类型
    var validationInterceptorAdapterType =
        typeof(AbpAsyncDeterminationInterceptor<>).MakeGenericType(typeof(ValidationInterceptor));

    if (asDefaultService)
    {
        // 使用Castle DynamicProxy的CreateInterfaceProxyWithoutTarget方法为这里的远程服务接口IRegularTestController动态生成实现类，并运用两个拦截器，并且将这个动态代理实现注册成IRegularTestController接口的实现类
        services.AddTransient(
            type,
            serviceProvider => ProxyGeneratorInstance
                .CreateInterfaceProxyWithoutTarget(
                    type,
                    (IInterceptor)serviceProvider.GetRequiredService(validationInterceptorAdapterType),
                    (IInterceptor)serviceProvider.GetRequiredService(interceptorAdapterType)
                )
        );
    }
    // 同时为范围类型实例IHttpClientProxy<IRegularTestController>，动态生成一套实现，并运用两个拦截器
    services.AddTransient(
        typeof(IHttpClientProxy<>).MakeGenericType(type),
        serviceProvider =>
        {
            var service = ProxyGeneratorInstance
                .CreateInterfaceProxyWithoutTarget(
                    type,
                    (IInterceptor)serviceProvider.GetRequiredService(validationInterceptorAdapterType),
                    (IInterceptor)serviceProvider.GetRequiredService(interceptorAdapterType)
                );

            return Activator.CreateInstance(
                typeof(HttpClientProxy<>).MakeGenericType(type),
                service
            );
        });

    return services;
}

```
通过上面的过程可以看到整个远端接口的动态代理实现注册过程。其中访问远端服务的具体实现都在Abp的拦截器中实现，那具体调拦截器又是如何实现的了？

### 1.2.2. 通过AOP拦截器实现远端接口代理访问实现

访问远端接口当然还是通过HttpClient直接访问了，大致访问过程如下：

1. 获取入口处配置的，服务接口类型和服务地址标记KEY
2. 根据服务地址标记KEY获取服务地址信息，主要是HOST地址，API版本
3. 获取远端服务的接口描述
4. 根据接口描述，构造请求路径，序列化参数、添加认证信息，发送请求
5. 根据接口描述，获取请求结果，进行反序列化

整个过程的实现如下：

```java
public class DynamicHttpProxyInterceptor<TService> : AbpInterceptor, ITransientDependency
{
    // ReSharper disable once StaticMemberInGenericType
    protected static MethodInfo GenericInterceptAsyncMethod { get; }

    protected ICancellationTokenProvider CancellationTokenProvider { get; }
    protected ICorrelationIdProvider CorrelationIdProvider { get; }
    protected ICurrentTenant CurrentTenant { get; }
    protected AbpCorrelationIdOptions AbpCorrelationIdOptions { get; }
    protected IDynamicProxyHttpClientFactory HttpClientFactory { get; }
    protected IApiDescriptionFinder ApiDescriptionFinder { get; }
    protected AbpRemoteServiceOptions AbpRemoteServiceOptions { get; }
    protected AbpHttpClientOptions ClientOptions { get; }
    protected IJsonSerializer JsonSerializer { get; }
    protected IRemoteServiceHttpClientAuthenticator ClientAuthenticator { get; }

    public ILogger<DynamicHttpProxyInterceptor<TService>> Logger { get; set; }

    static DynamicHttpProxyInterceptor()
    {
        GenericInterceptAsyncMethod = typeof(DynamicHttpProxyInterceptor<TService>)
            .GetMethods(BindingFlags.NonPublic | BindingFlags.Instance)
            .First(m => m.Name == nameof(MakeRequestAndGetResultAsync) && m.IsGenericMethodDefinition);
    }

    public DynamicHttpProxyInterceptor(
        IDynamicProxyHttpClientFactory httpClientFactory,
        IOptions<AbpHttpClientOptions> clientOptions,
        IOptionsSnapshot<AbpRemoteServiceOptions> remoteServiceOptions,
        IApiDescriptionFinder apiDescriptionFinder,
        IJsonSerializer jsonSerializer,
        IRemoteServiceHttpClientAuthenticator clientAuthenticator,
        ICancellationTokenProvider cancellationTokenProvider,
        ICorrelationIdProvider correlationIdProvider, 
        IOptions<AbpCorrelationIdOptions> correlationIdOptions,
        ICurrentTenant currentTenant)
    {
        CancellationTokenProvider = cancellationTokenProvider;
        CorrelationIdProvider = correlationIdProvider;
        CurrentTenant = currentTenant;
        AbpCorrelationIdOptions = correlationIdOptions.Value;
        HttpClientFactory = httpClientFactory;
        ApiDescriptionFinder = apiDescriptionFinder;
        JsonSerializer = jsonSerializer;
        ClientAuthenticator = clientAuthenticator;
        ClientOptions = clientOptions.Value;
        AbpRemoteServiceOptions = remoteServiceOptions.Value;

        Logger = NullLogger<DynamicHttpProxyInterceptor<TService>>.Instance;
    }

    public override async Task InterceptAsync(IAbpMethodInvocation invocation)
    {
         // 这里是重点，MakeRequestAsync方法是重点！！！
        if (invocation.Method.ReturnType.GenericTypeArguments.IsNullOrEmpty())
        {
           ！
            // 检查返回值是否为泛型类型，不是泛型类型，直接调用当前的MakeRequestAsync
            await MakeRequestAsync(invocation).ConfigureAwait(false);
        }
        else
        {
            // 如果返回值是泛型类型，则反射调用当前类的MakeRequestAndGetResultAsync泛型方法
            var result = (Task)GenericInterceptAsyncMethod
                .MakeGenericMethod(invocation.Method.ReturnType.GenericTypeArguments[0])
                .Invoke(this, new object[] { invocation });
            // 获取异步调用的结果
            invocation.ReturnValue = await GetResultAsync(
                result,
                invocation.Method.ReturnType.GetGenericArguments()[0]
            ).ConfigureAwait(false);
        }

    }

    private async Task<object> GetResultAsync(Task task, Type resultType)
    {
        await task.ConfigureAwait(false);
        return typeof(Task<>)
            .MakeGenericType(resultType)
            .GetProperty(nameof(Task<object>.Result), BindingFlags.Instance | BindingFlags.Public)
            .GetValue(task);
    }

    private async Task<T> MakeRequestAndGetResultAsync<T>(IAbpMethodInvocation invocation)
    {
        var responseAsString = await MakeRequestAsync(invocation).ConfigureAwait(false);

        //TODO: Think on that
        // 私有类型直接强制转换
        if (TypeHelper.IsPrimitiveExtended(typeof(T), true))
        {
            return (T)Convert.ChangeType(responseAsString, typeof(T));
        }
        // 复杂类型JSON反序列化
        return JsonSerializer.Deserialize<T>(responseAsString);
    }

    private async Task<string> MakeRequestAsync(IAbpMethodInvocation invocation)
    {
        // 获取入口处配置的，服务接口类型和服务地址标记KEY
        var clientConfig = ClientOptions.HttpClientProxies.GetOrDefault(typeof(TService)) ?? throw new AbpException($"Could not get DynamicHttpClientProxyConfig for {typeof(TService).FullName}.");
        // 根据服务地址标记KEY获取服务地址信息，主要是HOST地址，API版本
        var remoteServiceConfig = AbpRemoteServiceOptions.RemoteServices.GetConfigurationOrDefault(clientConfig.RemoteServiceName);

        // 创建一个HttpClient
        var client = HttpClientFactory.Create(clientConfig.RemoteServiceName);

        // 根据接口描述获取实际的接口路径
        var action = await ApiDescriptionFinder.FindActionAsync(remoteServiceConfig.BaseUrl, typeof(TService), invocation.Method).ConfigureAwait(false);
        
        // 获取API版本信息
        var apiVersion = GetApiVersionInfo(action);

        // 构造最终请求地址，携带上GET参数
        var url = remoteServiceConfig.BaseUrl.EnsureEndsWith('/') + UrlBuilder.GenerateUrlWithParameters(action, invocation.ArgumentsDictionary, apiVersion);

        // 构造请求体
        var requestMessage = new HttpRequestMessage(action.GetHttpMethod(), url)
        {
            Content = RequestPayloadBuilder.BuildContent(action, invocation.ArgumentsDictionary, JsonSerializer, apiVersion)
        };

        // 添加请求头
        AddHeaders(invocation, action, requestMessage, apiVersion);

        // 处理接口认证信息
        await ClientAuthenticator.Authenticate(
            new RemoteServiceHttpClientAuthenticateContext(
                client,
                requestMessage,
                remoteServiceConfig,
                clientConfig.RemoteServiceName
            )
        ).ConfigureAwait(false);

        // 发送HTTP请求
        var response = await client.SendAsync(requestMessage, GetCancellationToken()).ConfigureAwait(false);

        if (!response.IsSuccessStatusCode)
        {
            await ThrowExceptionForResponseAsync(response).ConfigureAwait(false);
        }

        // 读取响应结果
        return await response.Content.ReadAsStringAsync().ConfigureAwait(false);
    } 
    
    private ApiVersionInfo GetApiVersionInfo(ActionApiDescriptionModel action)
    {
        var apiVersion = FindBestApiVersion(action);

        //TODO: Make names configurable?
        var versionParam = action.Parameters.FirstOrDefault(p => p.Name == "apiVersion" && p.BindingSourceId == ParameterBindingSources.Path) ??
                            action.Parameters.FirstOrDefault(p => p.Name == "api-version" && p.BindingSourceId == ParameterBindingSources.Query);

        return new ApiVersionInfo(versionParam?.BindingSourceId, apiVersion);
    }

    private string FindBestApiVersion(ActionApiDescriptionModel action)
    {
        var configuredVersion = GetConfiguredApiVersion();
        // API版本配置为空取1.0
        if (action.SupportedVersions.IsNullOrEmpty())
        {
            return configuredVersion ?? "1.0";
        }
        // 配置存在取对应版本
        if (action.SupportedVersions.Contains(configuredVersion))
        {
            return configuredVersion;
        }
        // 否则取最新版本
        return action.SupportedVersions.Last(); //TODO: Ensure to get the latest version!
    }

    protected virtual void AddHeaders(IAbpMethodInvocation invocation, ActionApiDescriptionModel action, HttpRequestMessage requestMessage, ApiVersionInfo apiVersion)
    {
        //API Version
        if (!apiVersion.Version.IsNullOrEmpty())
        {
            //TODO: What about other media types?
            requestMessage.Headers.Add("accept", $"{MimeTypes.Text.Plain}; v={apiVersion.Version}");
            requestMessage.Headers.Add("accept", $"{MimeTypes.Application.Json}; v={apiVersion.Version}");
            requestMessage.Headers.Add("api-version", apiVersion.Version);
        }

        //Header parameters
        var headers = action.Parameters.Where(p => p.BindingSourceId == ParameterBindingSources.Header).ToArray();
        foreach (var headerParameter in headers)
        {
            var value = HttpActionParameterHelper.FindParameterValue(invocation.ArgumentsDictionary, headerParameter);
            if (value != null)
            {
                requestMessage.Headers.Add(headerParameter.Name, value.ToString());
            }
        }

        //CorrelationId
        requestMessage.Headers.Add(AbpCorrelationIdOptions.HttpHeaderName, CorrelationIdProvider.Get());

        //TenantId
        if (CurrentTenant.Id.HasValue)
        {
            //TODO: Use AbpAspNetCoreMultiTenancyOptions to get the key
            requestMessage.Headers.Add(TenantResolverConsts.DefaultTenantKey, CurrentTenant.Id.Value.ToString());
        }

        //Culture
        //TODO: Is that the way we want? Couldn't send the culture (not ui culture)
        var currentCulture = CultureInfo.CurrentUICulture.Name ?? CultureInfo.CurrentCulture.Name;
        if (!currentCulture.IsNullOrEmpty())
        {
            requestMessage.Headers.AcceptLanguage.Add(new StringWithQualityHeaderValue(currentCulture));
        }

        //X-Requested-With
        requestMessage.Headers.Add("X-Requested-With", "XMLHttpRequest");
    }

    private string GetConfiguredApiVersion()
    {
        var clientConfig = ClientOptions.HttpClientProxies.GetOrDefault(typeof(TService))
                            ?? throw new AbpException($"Could not get DynamicHttpClientProxyConfig for {typeof(TService).FullName}.");

        return AbpRemoteServiceOptions.RemoteServices.GetOrDefault(clientConfig.RemoteServiceName)?.Version
                ?? AbpRemoteServiceOptions.RemoteServices.Default?.Version;
    }

   // 省去不相关代码...
}
```

这里一个重点就是如何能知道对方的服务接口描述信息？回顾前面我们只是简单定义了一个C#接口:

```java
public interface IRegularTestController
{
    Task<int> IncrementValueAsync(int value);
}
```

这个接口怎么就能翻译成`http://远程IP:端口/服务地址?查询参数`这样的一个真实服务？这里一定有一个根据这个接口找到具体的远程API服务描述的功能。


### 1.2.3. 如何查找远端API描述信息

前面的调用过程中`ApiDescriptionFinder.FindActionAsync`正是查找API描述的信息。
可以看到其内部通过调用对方服务站点下的`api/abp/api-definition`接口来获取的接口描述，也就是说远程接口提供方还必须提供一个`api/abp/api-definition`接口，根据我们本地的接口定义信息能够找到远程接口描述。具体这个API元数据接口代码如下：

```java
// 查找API描述信息
ApiDescriptionFinder.FindActionAsync(remoteServiceConfig.BaseUrl, typeof(TService), invocation.Method)

// 内部又会调用如下接口，调用远程服务站点下的接口
protected virtual async Task<ApplicationApiDescriptionModel> GetApiDescriptionFromServerAsync(string baseUrl)
{
    using (var client = HttpClientFactory.Create())
    {
        // 可以看到这个默认的实现，就是调用一个固定的地址，如果对方不是采用Abp框架来提供接口，就只能模拟适配这个接口的返回了
        var response = await client.GetAsync(
            baseUrl.EnsureEndsWith('/') + "api/abp/api-definition",
            CancellationTokenProvider.Token
        ).ConfigureAwait(false);

       // 省略不相关...
    }
}

```

如果这个接口服务是有Abp框架开发的，它就自带了这个接口，下面看看这个接口的实现：

```java
[Route("api/abp/api-definition")]
public class AbpApiDefinitionController : AbpController, IRemoteService
{
    private readonly IApiDescriptionModelProvider _modelProvider;

    public AbpApiDefinitionController(IApiDescriptionModelProvider modelProvider)
    {
        _modelProvider = modelProvider;
    }

    [HttpGet]
    public ApplicationApiDescriptionModel Get()
    {
        return _modelProvider.CreateApiModel();
    }
}

public class AspNetCoreApiDescriptionModelProvider : IApiDescriptionModelProvider, ITransientDependency
{
    public ILogger<AspNetCoreApiDescriptionModelProvider> Logger { get; set; }

    private readonly IApiDescriptionGroupCollectionProvider _descriptionProvider;
    private readonly AbpAspNetCoreMvcOptions _options;
    private readonly AbpApiDescriptionModelOptions _modelOptions;

    public AspNetCoreApiDescriptionModelProvider(
        IApiDescriptionGroupCollectionProvider descriptionProvider,
        IOptions<AbpAspNetCoreMvcOptions> options,
        IOptions<AbpApiDescriptionModelOptions> modelOptions)
    {
        _descriptionProvider = descriptionProvider;
        _options = options.Value;
        _modelOptions = modelOptions.Value;

        Logger = NullLogger<AspNetCoreApiDescriptionModelProvider>.Instance;
    }

    public ApplicationApiDescriptionModel CreateApiModel()
    {

        var model = ApplicationApiDescriptionModel.Create();
        // 从 Microsoft.AspNetCore.Mvc.ApiExplorer.IApiDescriptionGroupCollectionProvider中获取描述信息
        foreach (var descriptionGroupItem in _descriptionProvider.ApiDescriptionGroups.Items)
        {
            foreach (var apiDescription in descriptionGroupItem.Items)
            {
                if (!apiDescription.ActionDescriptor.IsControllerAction())
                {
                    continue;
                }

                AddApiDescriptionToModel(apiDescription, model);
            }
        }

        return model;
    }
    
    // 省略不相关代码...
}
```

从上面的源码可以看到它的实现逻辑，就是从`ASP.NET CORE`内部获取控制器Action的元数据信息，它会把站点下所有的API接口描述都返回。 对于.net core作为API框架的接口服务可以快速提供这个接口。但是如果对方的接口服务是采用PHP或者JAVA等其他语言的开发框架提供的服务了。要模拟一个`ApplicationApiDescriptionModel`的返回结构，虽然不麻烦，但是也增加了不必要的工作量。那还有没有其他的实现方式了，其他的平台又是如何实现的了？

看看Java领域火热的微服务框架中实现方案Srping Cloud Feign，同样是一个动态API客户端。它直接在本地服务接口上，通过打标记来描述，也就是说再定义本地代理访问服务的接口时就描述了对方提供服务的方式，示例如下：

```java
@FeignClient( value = "xxx-api-servie" )
public interface IRegularTestController {

    @GetMapping(value="/RegularTest/IncrementValue", method = RequestMethod.POST)
    int IncrementValueAsync(@RequestParam("value") int value);
}
```

这种通过特性标记来进行接口API自描述的方法，就不需要对方提供API描述，本身定义代理接口就给出了接口描述信息。对WEB API服务提供方没有任何约束条件，其通用性更强。


## 1.3. 其他

### 1.3.1. 动态代理访问中的接口认证实现

最后来看看访问远程接口时的认证部分的处理，在前面拦截器中在发HTTP请求前，先做了认证逻辑。
1、如果配置了取当前请求上下文中的`access_token`则取当前（如果同一公司提供的服务，可能是同一套token规则，因此可不用重新生成）
2. 通过Abp的认证模块提供OIDC的两种认证模式`客户端认证`和`密码认证`

调用部分如下：

```java
// 调用接口认证方法
ClientAuthenticator.Authenticate(
    new RemoteServiceHttpClientAuthenticateContext(
        client,
        requestMessage,
        remoteServiceConfig,
        clientConfig.RemoteServiceName
    )
)
```

默认的添加认证的实现：

```java
[Dependency(ReplaceServices = true)]
public class IdentityModelRemoteServiceHttpClientAuthenticator : IRemoteServiceHttpClientAuthenticator, ITransientDependency
{
    public IHttpContextAccessor HttpContextAccessor { get; set; }

    protected IIdentityModelAuthenticationService IdentityModelAuthenticationService { get; }

    public IdentityModelRemoteServiceHttpClientAuthenticator(
        IIdentityModelAuthenticationService identityModelAuthenticationService)
    {
        IdentityModelAuthenticationService = identityModelAuthenticationService;
    }

    public async Task Authenticate(RemoteServiceHttpClientAuthenticateContext context)
    {
        if (context.RemoteService.GetUseCurrentAccessToken() != false)
        {
            // 如果设置了从当前httpContext取上下文的选项，则从当前请求的上下文取access_token
            var accessToken = await GetAccessTokenFromHttpContextOrNullAsync().ConfigureAwait(false);
            if (accessToken != null)
            {
                context.Request.SetBearerToken(accessToken);
                return;
            }
        }
        // 否则，调用Abp认证模块提供服务获取Token
        await IdentityModelAuthenticationService.TryAuthenticateAsync(
            context.Client,
            context.RemoteService.GetIdentityClient()
        ).ConfigureAwait(false);
    }

    protected virtual async Task<string> GetAccessTokenFromHttpContextOrNullAsync()
    {
        var httpContext = HttpContextAccessor?.HttpContext;
        if (httpContext == null)
        {
            return null;
        }

        return await httpContext.GetTokenAsync("access_token").ConfigureAwait(false);
    }
}
```

可以看到最后的认证服务是由Abp的认证模块`Volo.Abp.IdentityModel`提供的，是通过`IdentityModelAuthenticationService`，其内部适配OIDC的客户端认证和密码两种模来提供接口认证，如果对方的接口认证不是OIDC实现的两种规范，则需要去重写这个认证服务。

Abp的认证模块后续专门的章节进行分析。


> **参考**
> 1. 动态API客户端文档： https://docs.abp.io/zh-Hans/abp/latest/AspNetCore/Dynamic-CSharp-API-Clients